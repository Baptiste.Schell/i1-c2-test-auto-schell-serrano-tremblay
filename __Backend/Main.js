const express = require("express");
const fs = require('fs')
const Path = require('path')
const conx = require("./db.config")
const axios = require("axios")
const app = express();
var bodyParser = require('body-parser');
const port = 5000;
const Validate_mail = require("./Mail/ConfirmSignupMail")
const multer = require('multer')
const upload = multer({ dest: './UploadedPhoto' });
const FTP = require("./FTP_TRANSFERT")
const FTP_TREE = require("./FTP_MKDIR")
const jwt = require('jsonwebtoken');
const dotenv = require('dotenv');
const FireApp =require("firebase/app");
const FireAuth=require("firebase/auth");
const FireSignIn=require("firebase/auth")
const FireCreateUser=require("firebase/auth")

const firebaseConfig = {
    apiKey: "AIzaSyD2bPVkqSFev2VpPXr2cEEmJgTzW7CYICA",
    authDomain: "keepmyplant-e364c.firebaseapp.com",
    projectId: "keepmyplant-e364c",
    storageBucket: "keepmyplant-e364c.appspot.com",
    messagingSenderId: "511872530771",
    appId: "1:511872530771:web:09fa03a4d29e267014c9a6"
  };

const Fireapp = FireApp.initializeApp(firebaseConfig);
const auth = FireAuth.getAuth(Fireapp);

const TOKEN = "004daff42150bce6e774eeca91b6b490471fd9b681d8bc3949c8a4a2f442eb515768166b6174585e4c0f75c3c553a219c345978e47eab2020ee705c475756d83";

const cors = require('cors');
const corsOptions = {
    origin: '*',
    credentials: true,            //access-control-allow-credentials:true
    optionSuccessStatus: 200
}

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

app.use(cors(corsOptions));
app.use(express.json());

function VerifyToken(authHeader) {
    if (authHeader) // si y'a un headers
    {
        const token = authHeader.split(" ")[1];

        try {
            return { user: jwt.verify(token, TOKEN), "Verif": true }; //on verifie la signature du token si c'est bon on laisse passer
        } catch (error) {
            return { error: "API KEY NOT FOUND ! Not Authorized" };
        }
    }
    else {
        return { error: "API KEY NOT FOUND ! Not Authorized" };
    }
}

//-----------------------------------------------------------------//
//---------------------------- API --------------------------------//
//-----------------------------------------------------------------//

//---------- Verifier connexion serveur ----------//

app.get("/ccs", function (req, res) {

    const TokenCheck = VerifyToken(req.headers.authorization);
    if (TokenCheck.Verif) {
        res.send("OK")
    }
    else {
        res.send(TokenCheck)
    }
})

app.post("/GenerateToken/:mail" , function (req,res)  {
    res.send({token : jwt.sign({user : req.params.mail},TOKEN, {expiresIn: '1d'})})
})

//-------------------------------//

//----------------------------------//
//---------- Page profil -----------//
//----------------------------------//

app.get("/GetUser", function (req, res) {

    const TokenCheck = VerifyToken(req.headers.authorization);
    if (TokenCheck.Verif) {
        conx.query
            ("SELECT * FROM Users", (err, result) => {
                if (err) throw err;
                res.send(result);
            })
    }
    else {
        res.send(TokenCheck)
    }



})

app.get("/GetUserByMail/:mail", function (req, res) {
    const TokenCheck = VerifyToken(req.headers.authorization);
    if (TokenCheck.Verif) {
        conx.query
            ("SELECT * FROM Users WHERE mail=?",
                req.params.mail, (err, result) => {
                    if (err) throw err;
                    res.send(result);
                })
    }
    else {
        res.send(TokenCheck)
    }

})


app.get("/GetUserByID/:id", function (req, res) {
    const TokenCheck = VerifyToken(req.headers.authorization);
    if (TokenCheck.Verif) {
        conx.query
            ("SELECT * FROM Users WHERE idUser=?",
                req.params.id, (err, result) => {
                    if (err) throw err;
                    res.send(result);
                })
    }
    else {
        res.send(TokenCheck)
    }
})

app.get("/GetRole/:mail", function (req, res) {
    const TokenCheck = VerifyToken(req.headers.authorization);
    if (TokenCheck.Verif) {
        conx.query
            ("SELECT Roles.* FROM Users, Roles WHERE Roles.idRole = Users.idRole AND mail = ?",
                req.params.mail, (err, result) => {
                    if (err) throw err;
                    res.send(result);

                });
    }
    else {
        res.send(TokenCheck)
    }
});


app.get("/GetBotaniste/:mail", function (req, res) {
    const TokenCheck = VerifyToken(req.headers.authorization);
    if (TokenCheck.Verif) {
        conx.query
            ("SELECT Users.* FROM Users WHERE Users.idRole = '2' AND mail=?",
                req.params.mail, (err, result) => {
                    if (err) throw err;
                    res.send(result);
                })
    }
    else {
        res.send(TokenCheck)
    }
})

app.get("/GetAdresse/:mail", function (req, res) {
    const TokenCheck = VerifyToken(req.headers.authorization);
    if (TokenCheck.Verif) {
        conx.query
            ("SELECT Adresses.*,Users.idUser FROM Adresses, Users WHERE Users.idAdresse = Adresses.idAdresse AND mail=?",
                req.params.mail, (err, result) => {
                    if (err) throw err;
                    res.send(result);
                })
    }
    else {
        res.send(TokenCheck)
    }
})

app.get("/LoadPostAnnonces/:mail", function (req, res) {
    const TokenCheck = VerifyToken(req.headers.authorization);
    if (TokenCheck.Verif) {
        conx.query(
            "SELECT Annonces.*, Users.idUser, " +
            "(SELECT pseudo FROM Users WHERE idUser IN (SELECT idUser FROM Reservations WHERE Reservations.idAnnonce = Annonces.idAnnonce AND Reservations.validation = 1)) AS pseudoTest, " +
            "(SELECT mail FROM Users WHERE idUser IN (SELECT idUser FROM Reservations WHERE Reservations.idAnnonce = Annonces.idAnnonce AND Reservations.validation = 1)) AS mailTest " +
            "FROM Annonces, Users WHERE Annonces.idUser = Users.idUser AND mail = ?",
            req.params.mail, (err, result) => {
                if (err) throw err;
                res.send(result);
            });
    }
    else {
        res.send(TokenCheck)
    }
});


app.get("/LoadKeepAnnonces/:mail", function (req, res) {
    const TokenCheck = VerifyToken(req.headers.authorization);
    if (TokenCheck.Verif) {
        conx.query("SELECT Annonces.*, Reservations.*, Users.* FROM Annonces INNER JOIN Reservations ON Annonces.idAnnonce = Reservations.idAnnonce INNER JOIN Users ON Users.idUser = Reservations.idUser WHERE Users.mail = ?",
            req.params.mail, (err, result) => {
                if (err) throw err;
                res.send(result);
            });
    }
    else {
        res.send(TokenCheck)
    }
});

app.post("/UpdateAdresse/:voie/:rue/:ville/:cp/:idAdresse", function (req, res) {
    const TokenCheck = VerifyToken(req.headers.authorization);
    if (TokenCheck.Verif) {
        conx.query("UPDATE Adresses set voie=? , rue=? , ville=? , cp=? WHERE idAdresse=?", [req.params.voie, req.params.rue, req.params.ville, req.params.cp, req.params.idAdresse], (err, result) => {
            if (err) throw err;
            res.send("OK")
            res.send("OK")
        })
    }
    else {
        res.send(TokenCheck)
    }
})

app.post("/UpdateInfoProfil/:nom/:prenom/:telephone/:pseudo/:iduser", function (req, res) {
    const TokenCheck = VerifyToken(req.headers.authorization);
    if (TokenCheck.Verif) {
        conx.query("UPDATE Users set nom=? , prenom=? , telephone=?, pseudo=? WHERE idUser=?", [req.params.nom, req.params.prenom, req.params.telephone, req.params.pseudo, req.params.iduser], (err, result) => {
            if (err) throw err;
            res.send("OK")
        })
    }
    else {
        res.send(TokenCheck)
    }
})


app.post("/ResetPassword/:password/:idUser", function (req, res) {
    const TokenCheck = VerifyToken(req.headers.authorization);
    if (TokenCheck.Verif) {
        conx.query("UPDATE Users SET mdp=? WHERE idUser=?", [req.params.password, req.params.idUser], (err, result) => {
            if (err) throw err;
            res.send("OK")
        })
    }
    else {
        res.send(TokenCheck)
    }
})

//----------------------------------//
//------ Page visite de profil -----//
//----------------------------------//
app.get("/GetVisitUserByPseudo/:pseudo", function (req, res) {
    const TokenCheck = VerifyToken(req.headers.authorization);
    if (TokenCheck.Verif) {
        conx.query
            ("SELECT * FROM Users WHERE pseudo=?",
                req.params.pseudo, (err, result) => {
                    if (err) throw err;
                    res.send(result);
                })
    }
    else {
        res.send(TokenCheck)
    }
})

//----------------------------------//
//------ Page visite de profil Botaniste -----//
//----------------------------------//
app.get("/GetConseilsByBotaniste/:mail", function (req, res) {
    const TokenCheck = VerifyToken(req.headers.authorization);
    if (TokenCheck.Verif) {
        conx.query
            ("SELECT Conseils.*,TypesPlante.idTypePlante,TypesPlante.libelle,TypesPlante.urlPhoto FROM Users,Conseils,TypesPlante WHERE Users.idUser = Conseils.idUser AND Conseils.idTypePlante = TypesPlante.idTypePlante AND Users.mail = ?",
                req.params.mail, (err, result) => {
                    if (err) throw err;
                    res.send(result);
                })
    }
    else {
        res.send(TokenCheck)
    }
})

//----------------------------------//
//------- Page AllConseilsForTypePlante -------//
//----------------------------------//

app.get("/GetConseilAndUserByTypePlant/:idTypePlante", function (req, res) {
    const TokenCheck = VerifyToken(req.headers.authorization);
    if (TokenCheck.Verif) {
        conx.query
            ('SELECT Conseils.*,Users.pseudo,Users.mail FROM Conseils,Users WHERE Conseils.idUser = Users.idUser AND Conseils.idTypePlante = ?',
                req.params.idTypePlante, (err, result) => {
                    if (err) throw err;
                    res.send(result);
                })
    }
    else {
        res.send(TokenCheck)
    }
})


//----------------------------------//
//------- Page recherche/map -------//
//----------------------------------//



//----------------------------------//
//---- Page recherche botaniste ----//
//----------------------------------//
app.get("/GetTypePlants", function (req, res) {
    const TokenCheck = VerifyToken(req.headers.authorization);
    if (TokenCheck.Verif) {
        conx.query
            ("SELECT * FROM TypesPlante", (err, result) => {
                if (err) throw err;
                res.send(result);
            })
    }
    else {
        res.send(TokenCheck)
    }
})

app.get("/GetTypePlantById/:id", function (req, res,) {
    const TokenCheck = VerifyToken(req.headers.authorization);
    if (TokenCheck.Verif) {
        conx.query
            ("SELECT * FROM TypesPlante WHERE idTypePlante = ?", req.params.id, (err, result) => {
                if (err) throw err;
                res.send(result);
            })
    }
    else {
        res.send(TokenCheck)
    }
})

app.get("/GetTypesPlantsByName/:name", function (req, res) {
    const TokenCheck = VerifyToken(req.headers.authorization);
    if (TokenCheck.Verif) {
        conx.query
            ('SELECT * FROM TypesPlante WHERE libelle= ?', req.params.name, (err, result) => {
                if (err) throw err;
                res.send(result);
            })
    }
    else {
        res.send(TokenCheck)
    }
})

app.get("/GetConseilByTypePlant/:idTypePlant", function (req, res) {
    const TokenCheck = VerifyToken(req.headers.authorization);
    if (TokenCheck.Verif) {
        conx.query
            ('SELECT * FROM Conseils WHERE idTypePlante = ?',
                req.params.idTypePlant, (err, result) => {
                    if (err) throw err;
                    res.send(result);
                })
    }
    else {
        res.send(TokenCheck)
    }
})

app.post("/CreateConseil/:mail/:idTypePlant", function (req, res) {
    const TokenCheck = VerifyToken(req.headers.authorization);
    if (TokenCheck.Verif) {
        conx.query
            ("SELECT idUser FROM Users WHERE mail = ?",
                req.params.mail, (err, result) => {
                    if (err) {
                        throw err;
                    }
                    else {
                        const userId = result[0].idUser;
                        conx.query
                            ("INSERT INTO Conseils (titre, description, idTypePlante, idUser) VALUES (?, ?, ?, ?)",
                                [req.body.titre, req.body.description, req.params.idTypePlant, userId],
                                (err, result) => {
                                    if (err) {
                                        throw err;
                                    } else {
                                        res.send("OK");
                                    }
                                }
                            );
                    }
                }
            );
    }
    else {
        res.send(TokenCheck)
    }
});

app.post("/UpdateConseil/:titre/:description/:idTypePlante/:idConseil", function (req, res) {
    const TokenCheck = VerifyToken(req.headers.authorization);
    if (TokenCheck.Verif) {
        conx.query
            ("UPDATE Conseils SET titre = ?, description = ?, idTypePlante = ? WHERE idConseil = ?",
                [req.params.titre, req.params.description, req.params.idTypePlante, req.params.idConseil], (err, result) => {
                    if (err) throw err;
                    res.send("OK");
                }
            );
    }
    else {
        res.send(TokenCheck)
    }
})

app.delete("/DeleteConseilById/:idConseil", function (req, res) {
    const TokenCheck = VerifyToken(req.headers.authorization);
    if (TokenCheck.Verif) {
        conx.query
            ("DELETE FROM Conseils WHERE idConseil = ?",
                [req.params.idConseil],
                (err, result) => {
                    if (err) throw err;
                    res.send(result);
                })
    }
    else {
        res.send(TokenCheck)
    }
})

//----------------------------------//
//-------- Page mes plantes --------//
//----------------------------------//
app.get("/TypePlantes", function (req, res) {
    const TokenCheck = VerifyToken(req.headers.authorization);
    if (TokenCheck.Verif) {
        conx.query
            ("SELECT idTypePlante, libelle FROM TypesPlante", (err, result) => {
                if (err) throw err;
                res.send(result);
            })
    }
    else {
        res.send(TokenCheck)
    }
})

app.get("/GetPlantByUser/:mail", function (req, res) {
    const TokenCheck = VerifyToken(req.headers.authorization);
    if (TokenCheck.Verif) {
        conx.query
            ("SELECT P.idPlante, P.nom, P.description, P.urlPhoto, U.mail, P.idTypePlante, Tp.libelle FROM Users U, Plantes P, TypesPlante Tp WHERE U.idUser = P.idUser AND P.idTypePlante = Tp.idTypePlante AND U.mail=?",
                req.params.mail, (err, result) => {
                    if (err) throw err;
                    res.send(result);
                })
    }
    else {
        res.send(TokenCheck)
    }
})

app.get("/GetPlantByUserId/:idUser", function (req, res) {
    const TokenCheck = VerifyToken(req.headers.authorization);
    if (TokenCheck.Verif) {
        conx.query
            ("SELECT P.idPlante, P.nom, P.description, P.urlPhoto, U.mail, P.idTypePlante, Tp.libelle FROM Users U, Plantes P, TypesPlante Tp WHERE U.idUser = P.idUser AND P.idTypePlante = Tp.idTypePlante AND U.idUser = ?",
                req.params.idUser, (err, result) => {
                    if (err) throw err;
                    res.send(result);
                })
    }
    else {
        res.send(TokenCheck)
    }
})

app.get("/GetPlanteById/:idplante", function (req, res) {
    const TokenCheck = VerifyToken(req.headers.authorization);
    if (TokenCheck.Verif) {
        conx.query
            ("SELECT * FROM Plantes WHERE idPlante=?",
                req.params.idplante, (err, result) => {
                    if (err) throw err;
                    res.send(result);
                })
    }
    else {
        res.send(TokenCheck)
    }
})

app.get("/GetTypePlantesByName/:idplante", function (req, res) {
    const TokenCheck = VerifyToken(req.headers.authorization);
    if (TokenCheck.Verif) {
        conx.query
            ("SELECT * FROM Plantes WHERE idPlante=?",
                req.params.idplante, (err, result) => {
                    if (err) throw err;
                    res.send(result);
                })
    }
    else {
        res.send(TokenCheck)
    }
})

app.post("/UploadPhotoToFolder/:extension/:mail", upload.single('image'), (req, res) => {
    const TokenCheck = VerifyToken(req.headers.authorization);
    if (TokenCheck.Verif) {

        const imageName = req.file.filename
        const old_path = "./UploadedPhoto/" + imageName
        const new_path = "./UploadedPhoto/" + imageName + "." + req.params.extension
        fs.rename(old_path, new_path, function (err) {
            if (err) console.log('ERROR: ' + err);
        })
        const RemotePath = "/home/sastret/www/drive_mspr/" + req.params.mail + "/" + imageName + "." + req.params.extension
        FTP.UploadToFTP(new_path, RemotePath)

        res.send("OK")
    }
    else {
        res.send(TokenCheck)
    }

})



app.post("/CreatePlanteByUser/:mail/:nom/:description/:type_plante/:extension", upload.single('image'), function (req, res) {
    const TokenCheck = VerifyToken(req.headers.authorization);
    if (TokenCheck.Verif) {

        //TRANSFER FTP DE L'IMAGE
        const imageName = req.file.filename // on recupere le nom généré 
        const old_path = "./UploadedPhoto/" + imageName //old path et new_path pour renommer le fichier 
        const new_path = "./UploadedPhoto/" + imageName + "." + req.params.extension
        fs.rename(old_path, new_path, function (err) {
            if (err) console.log('ERROR: ' + err);
        })

        const RemotePath = "/home/sastret/www/drive_mspr/" + req.params.mail + "/" + imageName + "." + req.params.extension

        FTP.UploadToFTP(new_path, RemotePath) //TRANSFER VERS FTP HUGO



        // Récupérer l'id de l'utilisateur à partir de son email
        conx.query
            ("SELECT idUser FROM Users WHERE mail=?", req.params.mail, (err, result) => {
                if (err) {
                    throw err;
                } else {
                    const userId = result[0].idUser;
                    const _urlPhoto = "https://sastre-hugo.com/drive_mspr/" + req.params.mail + "/" + imageName + "." + req.params.extension

                    // Insérer la plante dans la table Plantes en utilisant l'id de l'utilisateur récupéré
                    conx.query("INSERT INTO Plantes (description, urlPhoto, nom, idUser, idTypePlante) VALUES (?, ?, ?, ?, ?)",
                        [req.params.description, _urlPhoto, req.params.nom, userId, req.params.type_plante],
                        (err, result) => {
                            if (err) {
                                throw err;
                            } else {
                                res.send("OK");
                            }
                        }
                    );
                }
            });
    }
    else {
        res.send(TokenCheck)
    }
});



app.post("/UpdatePlante/:idPlante/:nom/:description/:idTypePlante", function (req, res) {
    const TokenCheck = VerifyToken(req.headers.authorization);
    if (TokenCheck.Verif) {
        conx.query
            ("UPDATE Plantes SET nom=?, urlPhoto=?, description=?, idTypePlante=? WHERE idPlante = ?",
                [req.params.nom, req.body.url, req.params.description, req.params.idTypePlante, req.params.idPlante], (err, result) => {
                    if (err) throw err;
                    res.send("OK");
                })
    }
    else {
        res.send(TokenCheck)
    }
})

app.delete("/DeletePlanteById/:id_plante", function (req, res) {
    const TokenCheck = VerifyToken(req.headers.authorization);
    if (TokenCheck.Verif) {
        conx.query
            ("DELETE FROM Plantes WHERE idPlante = ?",
                [req.params.id_plante],
                (err, result) => {
                    if (err) throw err;
                    res.send(result);
                })
    }
    else {
        res.send(TokenCheck)
    }
})



//----------------------------------//
//-------- Page mes réservations ---//
//----------------------------------//

app.get("/GetReservation/:mail", function (req, res) {
    const TokenCheck = VerifyToken(req.headers.authorization);
    if (TokenCheck.Verif) {
        conx.query("SELECT R.idReservation, R.validation, A.dateDebut, A.dateFin, A.description , A.reference, U.mail FROM Users U,Reservations R, Annonces A WHERE R.idAnnonce = A.idAnnonce AND U.idUser = R.idUser AND U.mail = ?", req.params.mail, (err, result) => {
            if (err) throw err;
            res.send(result);
        })
    }
    else {
        res.send(TokenCheck)
    }
})

app.delete("/DeleteReservationById/:id_reservation", function (req, res) {
    const TokenCheck = VerifyToken(req.headers.authorization);
    if (TokenCheck.Verif) {
        conx.query("DELETE FROM Reservations WHERE idReservation = ?", [req.params.id_reservation],
            (err, result) => {
                if (err) throw err;
                res.send(result);
            })
    }
    else {
        res.send(TokenCheck)
    }
})

//----------------------------------------------------------//
//---------- Page Annonce + Recheche de plantes----------//
//----------------------------------------------------------//
app.post("/SendRequestReservation/:idUser/:idAnnonce", function (req, res) {
    const TokenCheck = VerifyToken(req.headers.authorization);
    if (TokenCheck.Verif) {
        conx.query("INSERT INTO Reservations (idUser, idAnnonce) VALUES (?, ?)",
            [req.params.idUser, req.params.idAnnonce],
            (err, result) => {
                if (err) {
                    throw err;
                } else {
                    res.send("OK");
                }
            }
        );
    }
    else {
        res.send(TokenCheck)
    }
})

app.post("/SendAbonnementByBotaniste/:idUser/:idAnnonce", function (req, res) {
    const TokenCheck = VerifyToken(req.headers.authorization);
    if (TokenCheck.Verif) {
        conx.query(
            "UPDATE Annonces SET idUser_1 = ? WHERE idAnnonce = ?",
            [req.params.idUser, req.params.idAnnonce],
            (err, result) => {
                if (err) {
                    throw err;
                } else {
                    res.send("OK");
                }
            }
        );
    }
    else {
        res.send(TokenCheck)
    }
});

app.get("/GetPDPByPseudo/:Pseudo",function (req,res) {
    const TokenCheck = VerifyToken(req.headers.authorization);
    if (TokenCheck.Verif) {
       conx.query("SELECT photodeprofil FROM Users WHERE pseudo=?",[req.params.Pseudo],(err,result) => {
        if(err) throw err ; 
        res.send(result)
       })
    }
    else {
        res.send(TokenCheck)
    }
})


app.post("/CreateAnnonceByUser/:mail/:dateDebut/:dateFin/:description/:idNiveauExpertiseRequis/:idCycleCompteRendu", function (req, res) {
    const TokenCheck = VerifyToken(req.headers.authorization);
    if (TokenCheck.Verif) {
        // Récupérer l'id de l'utilisateur à partir de son email
        conx.query("SELECT idUser FROM Users WHERE mail=?", req.params.mail, (err, result) => {
            if (err) {
                throw err;
            } else {
                const userId = result[0].idUser;
                const plantIds = []; // stocke les IDs des plantes associées à l'utilisateur

                // Récupérer les IDs des plantes associées à l'utilisateur
                conx.query("SELECT idPlante FROM Plantes WHERE idUser=?", userId, (err, result) => {
                    if (err) {
                        throw err;
                    } else {
                        // Ajouter les IDs des plantes dans le tableau plantIds
                        result.forEach((row) => {
                            plantIds.push(row.idPlante);
                        });

                        // Insérer l'annonce dans la table Annonces
                        conx.query("INSERT INTO Annonces (dateDebut, dateFin, description, idUser, idNiveauExpertiseRequis, idCycleCompteRendu, active) VALUES (?, ?, ?, ?, ?, ?, ?)",
                            [req.params.dateDebut, req.params.dateFin, req.params.description, userId, req.params.idNiveauExpertiseRequis, req.params.idCycleCompteRendu, null],
                            (err, result) => {
                                if (err) {
                                    throw err;
                                } else {
                                    const annonceId = result.insertId; // récupérer l'ID de l'annonce insérée
                                    const values = []; // stocke les valeurs à insérer dans la table contient

                                    // Ajouter les valeurs à insérer dans le tableau values
                                    plantIds.forEach((id) => {
                                        values.push([annonceId, id]);
                                    });

                                    // Insérer les plantes associées à l'utilisateur dans la table contient
                                    conx.query("INSERT INTO contient (idAnnonce, idPlante) VALUES ?", [values], (err, result) => {
                                        if (err) {
                                            throw err;
                                        } else {
                                            res.send("OK");
                                        }
                                    });
                                }
                            }
                        );
                    }
                });
            }
        });
    }
    else {
        res.send(TokenCheck)
    }
});

app.get("/NiveauExpertiseRequis", function (req, res) {
    const TokenCheck = VerifyToken(req.headers.authorization);
    if (TokenCheck.Verif) {
        conx.query
            ("SELECT idNiveauExpertiseRequis, libelle FROM NiveauExpertiseRequis", (err, result) => {
                if (err) throw err;
                res.send(result);
            })
    }
    else {
        res.send(TokenCheck)
    }
})

app.get("/CycleCompteRendu", function (req, res) {
    const TokenCheck = VerifyToken(req.headers.authorization);
    if (TokenCheck.Verif) {
        conx.query
            ("SELECT idCycleCompteRendu, nombre FROM CycleCompteRendu", (err, result) => {
                if (err) throw err;
                res.send(result);
            })
    }
    else {
        res.send(TokenCheck)
    }
})

app.get("/Search_plante_by_location_and_date/:ville/:niveauExpertise/:DateDebut/:DateFin/:mail", function (req, res) { // POUR RECUPERER LES ANNONCES POSTÉ DANS UNE VILLE ET DATE DONNÉE 
    const TokenCheck = VerifyToken(req.headers.authorization);
    if (TokenCheck.Verif) {
        if (req.params.DateDebut === "null" && req.params.DateFin === "null") {
            conx.query(
                "SELECT Annonces.*, Adresses.*,Users.photodeprofil,Users.mail,Users.nom,Users.prenom,Users.pseudo " +
                "FROM Annonces " +
                "INNER JOIN Users ON Annonces.idUser = Users.idUser " +
                "INNER JOIN Adresses ON Users.idAdresse=Adresses.idAdresse " +
                "WHERE Adresses.Ville LIKE ? " +
                "AND Annonces.idNiveauExpertiseRequis=? " +
                "AND Annonces.dateFin > NOW() " +
                "AND Users.idUser != ( SELECT idUser FROM Users WHERE mail =?) " +
                "AND NOT EXISTS ( " +
                "SELECT * " +
                "FROM Reservations " +
                "WHERE Reservations.idAnnonce = Annonces.idAnnonce " +
                "AND Reservations.validation = 1 )",
                ['%' + req.params.ville + '%', req.params.niveauExpertise, req.params.mail], (err, result) => {
                    if (err) throw err;
                    if (Object.keys(result).length === 0) {
                        res.send("AUCUNE ANNONCE")
                    }
                    else {
                        res.send(result);
                    }
                })
        }
        else {
            conx.query(
                "SELECT Annonces.*, Adresses.*,Users.photodeprofil,Users.mail,Users.nom,Users.prenom " +
                "FROM Annonces " +
                "INNER JOIN Users ON Annonces.idUser = Users.idUser " +
                "INNER JOIN Adresses ON Users.idAdresse=Adresses.idAdresse " +
                "WHERE Adresses.Ville LIKE ? " +
                "AND Annonces.idNiveauExpertiseRequis=? " +
                "AND Annonces.dateDebut >= ? " +
                "AND Annonces.dateFin <= ? " +
                "AND Annonces.dateFin > NOW() " +
                "AND Users.idUser != ( SELECT idUser FROM Users WHERE mail =?) " +
                "AND NOT EXISTS ( " +
                "SELECT * " +
                "FROM Reservations " +
                "WHERE Reservations.idAnnonce = Annonces.idAnnonce " +
                "AND Reservations.validation = 1)",
                ['%' + req.params.ville + '%', req.params.niveauExpertise, req.params.DateDebut, req.params.DateFin, req.params.mail], (err, result) => {
                    if (err) throw err;
                    if (Object.keys(result).length === 0) {
                        res.send("AUCUNE ANNONCE")
                    }
                    else {
                        res.send(result);
                    }
                })
        }
    }
    else {
        res.send(TokenCheck)
    }
})

app.get("/GetCoordCenter/:ville", function (req, res) {
    const TokenCheck = VerifyToken(req.headers.authorization);
    if (TokenCheck.Verif) {
        axios({
            method: "post",
            url: "https://maps.googleapis.com/maps/api/geocode/json?address=" + req.params.ville + "&key=AIzaSyAgTqC_T9-c6tNtNEY7CDUk3tKIp8eN0ys"
        }).then((resp) => {
            const lat = resp.data.results[0].geometry.location.lat
            const lng = resp.data.results[0].geometry.location.lng
            res.send([lat, lng])
        })
    }
    else {
        res.send(TokenCheck)
    }
})


//----------------------------------//
//--------- Page mon annonce -------//
//----------------------------------//
app.get("/GetAnnoncesActive/:mail", function (req, res) {
    const TokenCheck = VerifyToken(req.headers.authorization);
    if (TokenCheck.Verif) {
        conx.query(
            "SELECT Annonces.* " +
            "FROM Annonces, Users " +
            "WHERE Annonces.idUser = Users.idUser " +
            "AND Annonces.active = 1 " +
            "AND Users.mail = ? ",
            req.params.mail,
            (err, result) => {
                if (err) throw err;
                res.send(result);
            }
        );
    }
    else {
        res.send(TokenCheck)
    }
});

app.get("/GetKeeperByAnnonce/:idAnnonce", function (req, res) {
    const TokenCheck = VerifyToken(req.headers.authorization);
    if (TokenCheck.Verif) {
        conx.query(
            "SELECT Users.* " +
            "FROM Annonces, Users, Reservations " +
            "WHERE Annonces.idAnnonce = Reservations.idAnnonce " +
            "AND Reservations.idUser = Users.idUser " +
            "AND Annonces.active = 1 " +
            "AND Reservations.validation = 1 " +
            "AND Annonces.idAnnonce = ? ",
            req.params.idAnnonce,
            (err, result) => {
                if (err) throw err;
                res.send(result);
            }
        );
    }
    else {
        res.send(TokenCheck)
    }
});



app.get("/GetAnnonces/:mail", function (req, res) {
    const TokenCheck = VerifyToken(req.headers.authorization);
    if (TokenCheck.Verif) {
        conx.query(
            "SELECT Annonces.* " +
            "FROM Annonces, Users " +
            "WHERE Annonces.idUser = Users.idUser " +
            "AND Users.mail = ? ",
            req.params.mail,
            (err, result) => {
                if (err) throw err;
                res.send(result);
            }
        );
    }
    else {
        res.send(TokenCheck)
    }
});

app.get("/GetPhotosByAnnoncesWithComments/:idAnnonce", function (req, res) {
    const TokenCheck = VerifyToken(req.headers.authorization);
    if (TokenCheck.Verif) {
        conx.query(
            "SELECT Photos.*, Commentaires.* " +
            "FROM Annonces " +
            "INNER JOIN Photos ON Annonces.idAnnonce = Photos.idAnnonce " +
            "LEFT JOIN Commentaires ON Photos.idCommentaire = Commentaires.idCommentaire " +
            "WHERE Annonces.idAnnonce = ?",
            req.params.idAnnonce,
            (err, result) => {
                if (err) throw err;
                res.send(result);
            }
        );
    }
    else {
        res.send(TokenCheck)
    }
});


app.get("/GetKeeperByPhotos/:idAnnonce", function (req, res) {
    const TokenCheck = VerifyToken(req.headers.authorization);
    if (TokenCheck.Verif) {
        conx.query(
            "SELECT Users.* " +
            "FROM Users, Photos, Annonces " +
            "WHERE Users.idUser = Photos.idUser " +
            "AND Annonces.idAnnonce=? ",
            req.params.idAnnonce,
            (err, result) => {
                if (err) throw err;
                res.send(result);
            }
        );
    }
    else {
        res.send(TokenCheck)
    }
});

//----------------------------------//
//----- Page annonces suivies ------//
//----------------------------------//

app.get("/GetAnnoncesReserved/:mail", function (req, res) {
    const TokenCheck = VerifyToken(req.headers.authorization);
    if (TokenCheck.Verif) {
        conx.query(
            "SELECT Annonces.*,Reservations.numero " +
            "FROM Annonces, Users, Reservations " +
            "WHERE Reservations.idAnnonce = Annonces.idAnnonce " +
            "AND Reservations.idUser = Users.idUser " +
            "AND Annonces.active = 1 " +
            "AND Reservations.validation = 1 " +
            "AND Users.mail = ? ",
            req.params.mail,
            (err, result) => {
                if (err) throw err;
                res.send(result);
            }
        );
    }
    else {
        res.send(TokenCheck)
    }
});

app.get("/GetProprioByAnnonce/:idAnnonce", function (req, res) {
    const TokenCheck = VerifyToken(req.headers.authorization);
    if (TokenCheck.Verif) {
        conx.query(
            "SELECT Users.* " +
            "FROM Users, Annonces " +
            "WHERE Users.idUser = Annonces.idUser " +
            "AND Annonces.idAnnonce=? ",
            req.params.idAnnonce,
            (err, result) => {
                if (err) throw err;
                res.send(result);
            }
        );
    }
    else {
        res.send(TokenCheck)
    }
});

app.post("/AddPhotoForFollow/:mail/:idAnnonce/:idPhoto", function (req, res) {
    const TokenCheck = VerifyToken(req.headers.authorization);
    if (TokenCheck.Verif) {
        conx.query
            ("SELECT idUser FROM Users WHERE mail=?", req.params.mail, (err, result) => {
                if (err) {
                    throw err;
                } else {
                    conx.query("INSERT INTO Photos (urlPhoto) VALUES (?)",
                        [req.body.urlphoto],
                        (err, result) => {
                            if (err) {
                                throw err;
                            } else {
                                res.send("OK");
                            }
                        }
                    );
                }
            });
    }
    else {
        res.send(TokenCheck)
    }
});

app.post("/UpdatePhotoForFollow/:idPhoto/:extension/:mail_proprio", upload.single('image'), function (req, res) {
    const TokenCheck = VerifyToken(req.headers.authorization);
    if (TokenCheck.Verif) {

        const imageName = req.file.filename
        const old_path = "./UploadedPhoto/" + imageName
        const new_path = "./UploadedPhoto/" + imageName + "." + req.params.extension

        fs.rename(old_path, new_path, function (err) {
            if (err) console.log('ERROR: ' + err);
        })

        const RemotePathFTP = "/home/sastret/www/drive_mspr/" + req.params.mail_proprio + "/" + imageName + "." + req.params.extension
        FTP.UploadToFTP(new_path, RemotePathFTP)

        const pdp = "https://sastre-hugo.com/drive_mspr/" + req.params.mail_proprio + "/" + imageName + "." + req.params.extension;

        conx.query(
            "UPDATE Photos SET urlPhoto=? WHERE idPhoto=?",
            [pdp, req.params.idPhoto],
            (err, result) => {
                if (err) {
                    throw err;
                } else {
                    res.send("OK");
                }
            }
        );
    }
    else {
        res.send(TokenCheck)
    }
});


//----------------------------------//
//----- Page annonces suivies Botaniste ------//
//----------------------------------//
app.get("/GetFollowAnnonce/:mail", function (req, res) {
    const TokenCheck = VerifyToken(req.headers.authorization);
    if (TokenCheck.Verif) {
        conx.query(
            "SELECT Annonces.* " +
            "FROM Annonces, Users " +
            "WHERE Users.idUser = Annonces.idUser_1 " +
            "AND Annonces.active = 1 " +
            "AND Users.mail = ? ",
            req.params.mail,
            (err, result) => {
                if (err) throw err;
                res.send(result);
            }
        );
    }
    else {
        res.send(TokenCheck)
    }
});

app.post("/AddCommentsForFollow/:mail/:idAnnonce/:idPhoto", function (req, res) {
    const TokenCheck = VerifyToken(req.headers.authorization);
    if (TokenCheck.Verif) {
        conx.query
            ("SELECT idUser FROM Users WHERE mail=?", req.params.mail, (err, result) => {
                if (err) {
                    throw err;
                } else {
                    const userId = result[0].idUser;

                    conx.query("INSERT INTO Commentaires (description, idUser) VALUES (?, ?)",
                        [req.body.description, userId],
                        (err, result) => {
                            if (err) {
                                throw err;
                            } else {
                                const idCommentaire = result.insertId;
                                const idPhoto = req.params.idPhoto;
                                conx.query("UPDATE Photos SET idCommentaire=? WHERE idPhoto=?",
                                    [idCommentaire, idPhoto],
                                    (err, result) => {
                                        if (err) {
                                            throw err;
                                        } else {
                                            res.send("OK");
                                        }
                                    }
                                );
                            }
                        }
                    );
                }
            });
    }
    else {
        res.send(TokenCheck)
    }
});

//-----------------------------------------------------------------//
//---------------------GESTION DES DEMANDES------------------------//
//-----------------------------------------------------------------//

app.get("/GetReservationAnnonce/:mail", function (req, res) {
    const TokenCheck = VerifyToken(req.headers.authorization);
    if (TokenCheck.Verif) {
        conx.query("SELECT Users.nom, Users.prenom, Users.telephone, Users.photodeprofil, Reservations.* FROM Reservations, Users, Annonces WHERE Annonces.idAnnonce = Reservations.idAnnonce AND Reservations.idUser = Users.idUser AND Annonces.idUser = (SELECT idUser FROM Users WHERE mail = ?)", req.params.mail, (err, result) => {
            if (err) throw err;
            res.send(result);
        })
    }
    else {
        res.send(TokenCheck)
    }
})

app.post("/UpdateReservationAccepter/:idReservation", function (req, res) {
    const TokenCheck = VerifyToken(req.headers.authorization);
    if (TokenCheck.Verif) {
        conx.query("UPDATE Reservations SET validation = 1 WHERE validation IS NULL AND idReservation = ?", req.params.idReservation, (err, result) => {
            if (err) throw err;
            res.send("OK")
        })
    }
    else {
        res.send(TokenCheck)
    }
})

app.post("/UpdateReservationRefuser/:idReservation", function (req, res) {
    const TokenCheck = VerifyToken(req.headers.authorization);
    if (TokenCheck.Verif) {
        conx.query("UPDATE Reservations SET validation = 0 WHERE validation IS NULL AND idReservation = ?", req.params.idReservation, (err, result) => {
            if (err) throw err;
            res.send("OK")
        })
    }
    else {
        res.send(TokenCheck)
    }
})


//----------------------------------//
//-------- Authentification --------//
//----------------------------------//

app.post("/Authentification/:Login/:Password", function (req, res) {
    /*conx.query
    ("SELECT mail,estVerifie FROM Users WHERE mail=?",
        [req.params.Login], (err, result) => {
        if (err) throw err;
        if (Object.keys(result).length === 0) {
            res.send({ Auth: "Failed" }) // Pas de compte trouvé
        }
        else if(result[0].estVerifie===0 || result[0].estVerifie===null){
            res.send({Auth: "Undone" , token : jwt.sign({user : req.params.Login},TOKEN,{expiresIn: '1d'})}) // Compte trouvé avec le bon mot de passe mais compte non validé par mail (VOIR NADJIB)
        }
        else
        {*/

            FireSignIn.signInWithEmailAndPassword(auth, req.params.Login, req.params.Password)
            .then((userCredential) => {
                res.send({Auth: "Done" , token : jwt.sign({user : req.params.Login},TOKEN,{expiresIn: '1d'})} )
            })
            .catch((error) => {
                res.send({ Auth: "zebi" })
            });

            
        //}
    //})
})

app.post("/ConfirmAccount/:mail", function (req, res) { //
    const TokenCheck = VerifyToken(req.headers.authorization);
    if (TokenCheck.Verif) {
        conx.query("UPDATE Users SET estVerifie=1 WHERE Mail=?",req.params.mail, (err, result) => {
            if (err) throw err; 
            res.send({Status :"OK",token : jwt.sign({user : req.params.mail},TOKEN, {expiresIn: '1d'})})
        })
    
    }
    else 
    {
        res.send(TokenCheck)
    }
})

app.post("/SendMailConfirmationCode/:mail/", function (req, res) {  // Pour envoyer le code pour confirmer l'adresse mail lors de l'inscription 
    const TokenCheck = VerifyToken(req.headers.authorization);
    if (TokenCheck.Verif) {
        const code = Validate_mail.Send_Code_Confirmation_Mail(req.params.mail);

        res.send({ Code: code })
    }
    else {
        res.send(TokenCheck)
    }
})

app.post("/ValidateConfirmationCodeMail/:mail/:code", function (req, res) { // Pour confirmer le code envoyé par mail 
    const TokenCheck = VerifyToken(req.headers.authorization);
    if (TokenCheck.Verif) {
        conx.query
            ("SELECT * FROM CodeConfirmation WHERE Mail=? and Code=?",
                [req.params.mail, req.params.code], (err, result) => {
                    if (err) throw err;
                    if (Object.keys(result).length > 0) {
                        res.send("OK")
                    }
                    else {
                        res.send("FAIL")
                    }
                })
    }
    else {
        res.send(TokenCheck)
    }
})

app.post("/VerifyExistingMail/:mail", function (req, res) {

        conx.query
            ("SELECT mail FROM Users WHERE mail=?",
                req.params.mail, (err, result) => {
                    if (err) throw err;
                    if (Object.keys(result).length === 0) {
                        res.send({ ExistingMail: "NO" })
                    }
                    else {
                        res.send({ ExistingMail: "YES" })
                    }
                })

})

app.post("/registernewaccount/:Pseudo/:Nom/:Prenom/:Mail/:password/:Telephone/:NumeroRue/:Rue/:CodePostal/:Ville/:extension", upload.single('image'), function (req, res) {
   

        const RemotePath = "/home/sastret/www/drive_mspr/" + req.params.Mail
        FTP_TREE.CreateTree(RemotePath)

        const imageName = req.file.filename
        const old_path = "./UploadedPhoto/" + imageName
        const new_path = "./UploadedPhoto/" + imageName + "." + req.params.extension

        fs.rename(old_path, new_path, function (err) {
            if (err) console.log('ERROR: ' + err);
        })

        setTimeout(() => {
            const RemotePathFTP = "/home/sastret/www/drive_mspr/" + req.params.Mail + "/" + imageName + "." + req.params.extension
            FTP.UploadToFTP(new_path, RemotePathFTP)
        }, 2000)



        const idRole = 1
        const pdp = "https://sastre-hugo.com/drive_mspr/" + req.params.Mail + "/" + imageName + "." + req.params.extension;
        const argu = [req.params.Pseudo, req.params.Nom, req.params.Prenom, req.params.Mail, req.params.Telephone, pdp, idRole]
        conx.query
            ("INSERT INTO Users (Pseudo,Nom,Prenom,Mail,Telephone,photodeprofil,idRole) VALUES (?,?,?,?,?,?,?)", argu, (err, result) => {
                if (err) throw err;
                const adresse = req.params.NumeroRue + " " + req.params.Rue + " " + req.params.CodePostal + " " + req.params.Ville
                axios({
                    method: "POST",
                    url: "https://maps.googleapis.com/maps/api/geocode/json?address=" + adresse + "&key=AIzaSyAgTqC_T9-c6tNtNEY7CDUk3tKIp8eN0ys"
                }).then((resp) => {
                    conx.query
                        ("INSERT INTO Adresses (Cp,Ville,Voie,Rue,lat,lgt) VALUES (?,?,?,?,?,?)",
                            [req.params.CodePostal, req.params.Ville, req.params.NumeroRue, req.params.Rue, resp.data.results[0].geometry.location.lat, resp.data.results[0].geometry.location.lng], (err, result) => {
                                if (err) throw err;

                                const idAdresse = result.insertId;

                                conx.query("UPDATE Users SET idAdresse = ? WHERE Mail = ?", [idAdresse, req.params.Mail], (err, result) => {
                                    if (err) throw err;
                                
                                });
                            })
                })
            })
   

            FireCreateUser.createUserWithEmailAndPassword(auth,req.params.Mail,req.params.password)
            .then((userCredential) => {
                res.send({Status: "ok" , token : jwt.sign({user : req.params.mail},TOKEN, {expiresIn: '1d'})})
            })
            .catch((error) => {
                const errorCode = error.code;
                const errorMessage = error.message;
                console.log(errorCode, errorMessage)
            });

})


//-----------------------------------------------------------------//
//--------------------------FRONT BATOU----------------------------//
//-----------------------------------------------------------------//


app.get("/GetProfilePhotoByPseudo/:Pseudo", function (req, res) {
    const TokenCheck = VerifyToken(req.headers.authorization);
    if (TokenCheck.Verif) {
        conx.query("SELECT photodeprofil FROM Users WHERE pseudo = ?", req.params.Pseudo, (err, result) => {
            if (err) throw err;
            res.send(result);
        })
    }
    else {
        res.send(TokenCheck)
    }
}
)


//-----------------------------------------------------------------//
//--------------------------Spécial API----------------------------//
//-----------------------------------------------------------------//
app.post("/Bypassauth/:mail",function (req,res) {
    conx.query("SELECT * from Users WHERE mail=?",req.params.mail,(err,result) => {
        if(Object.keys(result)===0)
        {
            res.send("FAIL")
        }
        else
        {
            res.send({Auth: "Done" , token : jwt.sign({user : req.params.mail},TOKEN,{expiresIn: '1d'}),Pseudo : result[0].pseudo} )

        }
    })
    
})
//-----------------------------------------------------------------//
//--------------------------MESSAGERIE----------------------------//
//-----------------------------------------------------------------//
app.post("/SendMessage/:FromPseudo/:ToPseudo/:Message" ,function (req,res)  {
    const TokenCheck = VerifyToken(req.headers.authorization);
    if (TokenCheck.Verif) {
        conx.query("INSERT INTO Messagerie (FromPseudo,ToPseudo,Message) VALUES (?,?,?)",[req.params.FromPseudo,req.params.ToPseudo,req.params.Message], (err,result) => {
            if (err) throw err ; 
            res.send("OK")
        })
    }
    else {
        res.send(TokenCheck)
    }
})

app.get("/GetConversation/:Pseudo",function (req,res) {
    const TokenCheck = VerifyToken(req.headers.authorization);
    if (TokenCheck.Verif) {
       conx.query(`SELECT cm.*
       FROM Messagerie cm
       INNER JOIN (
           SELECT CONCAT(LEAST(FromPseudo, ToPseudo), '-', GREATEST(FromPseudo, ToPseudo)) AS conversation, MAX(horodatage) AS max_horodatage
           FROM Messagerie
           GROUP BY conversation
       ) AS sub ON CONCAT(LEAST(cm.FromPseudo, cm.ToPseudo), '-', GREATEST(cm.FromPseudo, cm.ToPseudo)) = sub.conversation AND cm.horodatage = sub.max_horodatage
       WHERE cm.FromPseudo = ? OR cm.ToPseudo = ? ORDER BY horodatage DESC`,[req.params.Pseudo,req.params.Pseudo],(err,result) => {
        if(err) throw err ; 
        res.send(result)
       })
    }
    else {
        res.send(TokenCheck)
    }
})

app.get("/LoadMessageByConversation/:PseudoFrom/:PseudoTo",function (req,res) {
    const TokenCheck = VerifyToken(req.headers.authorization);
    if (TokenCheck.Verif) {
       conx.query("SELECT * FROM Messagerie WHERE (ToPseudo=? AND FromPseudo=?) OR (ToPseudo=? AND FromPseudo=?)",[req.params.PseudoFrom,req.params.PseudoTo,req.params.PseudoTo,req.params.PseudoFrom],(err,result) => {
        if(err) throw err ; 
        res.send(result)
       })
    }
    else {
        res.send(TokenCheck)
    }
})


app.get("/GetLastConversation/:Pseudo",function (req,res) {
    const TokenCheck = VerifyToken(req.headers.authorization);
    if (TokenCheck.Verif) {
       
    }
    else {
        res.send(TokenCheck)
    }
})








app.use(cors())

app.listen(port, console.log("====> Server Listening on " + port))

module.exports = app;