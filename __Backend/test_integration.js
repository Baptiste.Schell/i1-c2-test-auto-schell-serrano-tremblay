const request = require("supertest");
const app = require("./Main");

describe('Tests de la requête /css', () => {
    test('La requête doit permettre de ce connecter', () => {
        return request(app)
            .get("/ccs")
            .then((response) => {
                expect(response.statusCode).toBe(200);
                expect(response.body).toEqual({ Connection: "ok" });
            });
    });
});

describe('Test de la requête /GetUser', () => {
    test('La requête devrait récupérer les utilisateurs', () => {
        return request(app)
            .get('/GetUser')
            .expect(200)
            .then((response) => {
                expect(response.body).toBeDefined();
                expect(response.body).toBeInstanceOf(Array);
                expect(response.body.length).toBeGreaterThan(0);

                // Test sur les propriétés d'un utilisateur
                const user = response.body[0];
                expect(user).toHaveProperty('idUser');
                expect(user).toHaveProperty('nom');
                expect(user).toHaveProperty('mail');

                // Test sur le champ nom
                expect(user.nom).toBeDefined();
                expect(typeof user.nom).toBe('string');
                expect(user.nom.length).toBeGreaterThan(0);
            });
    });
});

describe('Test de la requête /GetUserByMail/:mail', () => {
    test('La requête devrait récupérer l\'utilisateur avec l\'adresse e-mail spécifiée', () => {
        const mail = 'testParticulier@gmail.com';

        return request(app)
            .get(`/GetUserByMail/${mail}`)
            .expect(200)
            .then((response) => {
                expect(response.body).toBeInstanceOf(Array);
                expect(response.body.length).toBeGreaterThan(0);

                const user = response.body[0];
                expect(user).toHaveProperty('idUser');
                expect(user).toHaveProperty('nom');
                expect(user).toHaveProperty('prenom');
                expect(user).toHaveProperty('mail');
                expect(user).toHaveProperty('mdp');
                expect(user).toHaveProperty('photodeprofil');
                expect(user).toHaveProperty('idRole');
            });
    });
});

describe('Test de la requête /GetUserByID/:id', () => {
    test('La requête devrait récupérer l\'utilisateur avec l\'ID spécifié', () => {
        const id = 121;

        return request(app)
            .get(`/GetUserByID/${id}`)
            .expect(200)
            .then((response) => {
                expect(response.body).toBeDefined();
                expect(response.body).toBeInstanceOf(Array);
                expect(response.body.length).toBe(1);

                const user = response.body[0];
                expect(user).toHaveProperty('idUser');
                expect(user).toHaveProperty('nom');
                expect(user).toHaveProperty('mail');
                expect(user.idUser).toBe(id);
            });
    });
});

describe('Test de la requête /GetRole/:mail', () => {
    test('La requête devrait récupérer le rôle avec l\'adresse e-mail spécifiée', () => {
        const mail = 'testParticulier@gmail.com';

        return request(app)
            .get(`/GetRole/${mail}`)
            .expect(200)
            .then((response) => {
                expect(response.body).toBeDefined();
                expect(response.body).toBeInstanceOf(Array);
                expect(response.body.length).toBeGreaterThan(0);

                const role = response.body[0];
                expect(role).toHaveProperty('idRole');
                expect(role).toHaveProperty('label');
                expect(role.label).toBe('Particulier');
            });
    });
});

describe('Test de la requête /GetBotaniste/:mail', () => {
    test('La requête devrait récupérer le botaniste avec l\'adresse e-mail spécifiée', () => {
        const mail = 'testBotaniste@gmail.com';

        return request(app)
            .get(`/GetBotaniste/${mail}`)
            .expect(200)
            .then((response) => {
                expect(response.body).toBeDefined();
                expect(response.body).toBeInstanceOf(Array);
                expect(response.body.length).toBeGreaterThan(0);

                const botaniste = response.body[0];
                expect(botaniste).toHaveProperty('idUser');
                expect(botaniste).toHaveProperty('nom');
                expect(botaniste.nom).toBe('TESTNOMB');
            });
    });
});

describe('Test de la requête /GetAdresse/:mail', () => {
    test('La requête devrait récupérer l\'adresse de l\'utilisateur spécifié', () => {
        const mail = 'testParticulier@gmail.com';

        return request(app)
            .get(`/GetAdresse/${mail}`)
            .expect(200)
            .then((response) => {
                expect(response.body).toBeDefined();
                expect(response.body).toBeInstanceOf(Array);
                expect(response.body.length).toBeGreaterThan(0);

                const adresse = response.body[0];
                expect(adresse).toHaveProperty('idAdresse');
                expect(adresse).toHaveProperty('CP');
                expect(adresse).toHaveProperty('ville');
                expect(adresse).toHaveProperty('voie');
                expect(adresse).toHaveProperty('rue');
                expect(adresse).toHaveProperty('complementAdresse');
                expect(adresse).toHaveProperty('lat');
                expect(adresse).toHaveProperty('lgt');
                expect(adresse.ville).toBe('Montpellier');
            });
    });
});

describe('Test de la requête /LoadPostAnnonces/:mail', () => {
    test('La requête devrait charger les annonces postées par l\'utilisateur spécifié', () => {
        const mail = 'testParticulier@gmail.com';

        return request(app)
            .get(`/LoadPostAnnonces/${mail}`)
            .expect(200)
            .then((response) => {
                expect(response.body).toBeDefined();
                expect(response.body).toBeInstanceOf(Array);
                expect(response.body.length).toBeGreaterThan(0);

                const annonce = response.body[0];
                expect(annonce).toHaveProperty('idAnnonce');
                expect(annonce).toHaveProperty('dateDebut');
                expect(annonce).toHaveProperty('dateFin');
                expect(annonce).toHaveProperty('description');
                expect(annonce).toHaveProperty('reference');
                expect(annonce).toHaveProperty('nbEtape');
                expect(annonce).toHaveProperty('active');
                expect(annonce).toHaveProperty('idUser');
                expect(annonce).toHaveProperty('idNiveauExpertiseRequis');
                expect(annonce).toHaveProperty('idCycleCompteRendu');
            });
    });
});

describe('Test de la requête /LoadKeepAnnonces/:mail', () => {
    test('La requête devrait charger les annonces pour l\'utilisateur spécifié', () => {
        const mail = 'testParticulier@gmail.com';

        return request(app)
            .get(`/LoadKeepAnnonces/${mail}`)
            .expect(200)
            .then((response) => {
                expect(response.body).toBeDefined();
                expect(response.body).toBeInstanceOf(Array);
                expect(response.body.length).toBeGreaterThan(0);

                const annonce = response.body[0];
                expect(annonce).toHaveProperty('idAnnonce');
                expect(annonce).toHaveProperty('dateDebut');
                expect(annonce).toHaveProperty('dateFin');
                expect(annonce).toHaveProperty('description');
                expect(annonce).toHaveProperty('reference');
                expect(annonce).toHaveProperty('nbEtape');
                expect(annonce).toHaveProperty('active');
                expect(annonce).toHaveProperty('idUser');
                expect(annonce).toHaveProperty('idNiveauExpertiseRequis');
                expect(annonce).toHaveProperty('idCycleCompteRendu');
            });
    });
});

describe('Test de la requête /UpdateAdresse/:voie/:rue/:ville/:cp/:idAdresse', () => {
    test('La requête devrait mettre à jour l\'adresse spécifiée', () => {
        const voie = 'Nouvelle voie';
        const rue = 'Nouvelle rue';
        const ville = 'Nouvelle ville';
        const cp = '12345';
        const idAdresse = 1;

        return request(app)
            .post(`/UpdateAdresse/${voie}/${rue}/${ville}/${cp}/${idAdresse}`)
            .expect(200)
            .then((response) => {
                expect(response.text).toBe('OK');
            });
    });
});

describe('Test de la requête /UpdateInfoProfil/:nom/:prenom/:telephone/:pseudo/:iduser', () => {
    test('La requête devrait mettre à jour les informations de profil d\'un utilisateur', () => {
        const nom = 'Nouveau nom';
        const prenom = 'Nouveau prénom';
        const telephone = '1234567890';
        const pseudo = "Nouveau pseudo"
        const iduser = 1;

        return request(app)
            .post(`/UpdateInfoProfil/${nom}/${prenom}/${telephone}/${pseudo}/${iduser}`)
            .expect(200)
            .then((response) => {
                expect(response.text).toBe('OK');
            });
    });
});