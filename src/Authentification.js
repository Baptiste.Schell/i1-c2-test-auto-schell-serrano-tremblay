import React, { useState } from "react";
import "./Assets/CSS/Authentification.css"
import Logo from "./Assets/image/Logo.png"
import Image1 from "./Assets/image/keepyourplant_login.png"
import Image2 from "./Assets/image/Rhipsalis_Paradoxa.png"
import axios from "axios";
import { useNavigate } from "react-router-dom";
import ErrorMsg from "./Component/ErrorMsg";
import Temp_img from "./Assets/image/no-image.jpg"





function Authentification() {
    const [Login, setLogin] = useState("")
    const [Password, setPassword] = useState("")
    const [IsSignUp, setIsSignup] = useState(false)
    const [IsSignIn,setIsSignIn]=useState(true)
    const [FormAdresse,setFormAdresse]=useState(false) 
    const [ErrorPassword,setErrorPassword]=useState(false)

    const [Nom,setNom]=useState("")
    const [Prenom,setPrenom]=useState("")
    const [Pseudo,setPseudo]=useState("")
    const [Mail,setMail]=useState("")
    const [Telephone,setTelephone]=useState("")
    const [ConfirmMail,setConfirmMail]=useState("") 
    const [PasswordSignup,setPasswordSignup]=useState("") ;  
    const [ConfirmPasswordSignup,setConfirmPasswordSignup]=useState("") ;  
    const [NumRue,setNumRue]=useState("")
    const [Rue,setRue]=useState("")
    const [CodePostal,setCodePostal]=useState("")
    const [Ville,setVille]=useState("")
    const [Image,setImage]=useState()


    const navigate=useNavigate()

   function CheckLogin() 
    {
        axios({
            headers :{"authorization" : 'Bearer '+localStorage["Token"] },
            method : "POST" , 
            url : process.env.REACT_APP_server + "Authentification/"+Login+"/"+Password
        }).then((resp) => {
            if(resp.data.Auth==="Done")
            {   
                localStorage["UserMail"]=Login ;
                localStorage["Token"]=resp.data.token
                navigate("/Dashboard")
            }
            else if(resp.data.Auth==="Undone")
            {
                axios({
                    headers :{"authorization" : 'Bearer '+resp.data.token },
                    method: 'POST',
                    url: process.env.REACT_APP_server + "SendMailConfirmationCode/" + Login
                }).then((response) => {
                    console.log(resp.data.token)
                    navigate("/Auth/ConfirmMail", {state:{Confirmation_code: response.data.Code, Mail : Login, Token : resp.data.token}}) ;
                })
            }
            else
            {
                setErrorPassword(true)
            }
        })


    }

    function Bypassauth()
    {
        axios({
            method :'POST',
            url : process.env.REACT_APP_server + "Bypassauth/"+Login
        }).then((resp) => {
            if(resp.data.Auth==="Done")
            {   
                localStorage["UserMail"]=Login ;
                localStorage["Token"]=resp.data.token
                localStorage["Pseudo"]=resp.data.Pseudo
                navigate("/Dashboard")
            }
            
        })
    }

    function ConfirmMailByCode() 
    {

        const checkEmpty=true ; // On part du principe que tous les champs sont remplies
        const input=[document.getElementById("N°"),document.getElementById("Rue"),document.getElementById("CodePostal"),
        document.getElementById("Ville")]
        
        input.map((item) => { // On check si les champs sont vide ou non 
            if(item.value==="")
            {
              item.style.backgroundColor="#eb5968" ; 
              checkEmpty=false ;
            }
         })

         if(checkEmpty===false)
         {
            alert("Veuillez remplir tous les champs.") // Si y'a un champ de vide on envoie une notif
         }
         else 
         {
            const formData = new FormData()
            formData.append("image", Image)


            // Sinon on envoie les nom,prenom,adresse, etc etc en base de donnees et on confirme son adresse mail par un code
            axios.post(process.env.REACT_APP_server + "RegisterNewAccount/"+ Pseudo +"/" + Nom +"/"+ Prenom +"/"+Mail +"/"+PasswordSignup+"/"+Telephone+"/"+NumRue+"/"+Rue+"/"+CodePostal+"/"+Ville+"/"+Image.type.split("/")[1], formData, { headers: {'Content-Type': 'multipart/form-data'}})
            .then((response) => {
                if(response.data.Status==="ok")
                {
                   
                    axios({
                        headers : {"authorization" : 'Bearer '+ response.data.token},
                        method: 'POST',
                        url: process.env.REACT_APP_server + "SendMailConfirmationCode/" + Mail
                    }).then((resp) => {
                        navigate("/Auth/ConfirmMail", {state:{Confirmation_code: resp.data.Code, Mail : Mail, SignupMDP : PasswordSignup, Token : response.data.token}}) ;
                    })
                }
            })
        
        }
        


        
    }

    function Signup() 
    {
        setIsSignup(true)
        setIsSignIn(false)
    }

    function HandleImage(e) 
    {
        setImage(e.target.files[0])
    }

    function GoToAdresse() 
    {

        const checkEmpty=true ; // True = On part du principe que tous les champs sont bien remplie

        // On recupere tous les champs pour checker si y'en a un de vide
        const input=[document.getElementById("Nom"),document.getElementById("Prenom"),document.getElementById("Mail"),
        document.getElementById("Confirm-mail"),document.getElementById("Telephone-input"),document.getElementById("PasswordSignup"),
        document.getElementById("ConfirmSingupPassword")]

        input.map((item) => {
            if(item.value==="")
            {
              item.style.backgroundColor="#eb5968" ; 
              checkEmpty=false ;
            }
         })
 
         

         if(checkEmpty===true)
         {
            if(Mail!=ConfirmMail)
            {
                alert("Les deux adresses mails doivent être identiques.")
            }
            else if(PasswordSignup.length<7)
            {
                alert("Le mot de passe doit contenir au moins 6 caracteres.")
            }
            else if (PasswordSignup!=ConfirmPasswordSignup)
            {
                alert("Les deux mot de passes doivent être identiques.")
            }
            else
            {
                axios({
       
                    method : "post",
                    url : process.env.REACT_APP_server+"VerifyExistingMail/"+Mail
                }).then((response) => {
                    if (response.data.ExistingMail==="YES")
                    {
                        alert("Votre adresse mail est déja utilisée par un compte tentez de vous connecter avec.")
                    }
                    else
                    {
                        setIsSignup(false)
                        setIsSignIn(false)
                        setFormAdresse(true)
                    }
                })
        
            }
         }
        
    }

    return (
        <div style={{ width: '100%', height: '100%' ,display : 'flex' , flexDirection : "row", }}>
            <div id="illustration">
                {<img src={Image1}/> || <img src={Image2}/>}
                <div id="hgg2233">
                    <p> Gardez vos plantes en toute sécurité avec nos 1500 botanistes chez A'Rosa-je</p>
                </div>
            </div>

            {IsSignUp &&
                <div id="Form-sign">
                    <h3> Bienvenue sur A'Rosa-je ! </h3>
                    <div id="Nom-prenom">
                        <input value={Nom} required id="Nom" placeholder="Votre Nom" onChange={(e,id) => {setNom(e.target.value)}}/> 
                        <input value={Prenom} id="Prenom" placeholder="Votre Prénom" onChange={(e) => {setPrenom(e.target.value)}}/>
                        <input value={Pseudo} id="pseudo" placeholder="Votre Pseudo" onChange={(e) => {setPseudo(e.target.value)}}/>
                    </div>

                    <div id="Mail-Confirm"> 
                        <input value={Mail} id="Mail" placeholder="Votre Adresse mail" onChange={(e) => {setMail(e.target.value)}}/>
                        <input value={ConfirmMail} id="Confirm-mail" placeholder="Confirmez votre Adresse mail" onChange={(e) => {setConfirmMail(e.target.value)}}/>
                    </div>

                    <div id="Telephone">
                        <input value={Telephone} id="Telephone-input"  onChange={(e) => {setTelephone(e.target.value)}} placeholder="Votre Numéro de téléphone"/>
                    </div>

                    <div id="password-Confirm"> 
                        <input value={PasswordSignup} id="PasswordSignup" placeholder="Mot de passe" onChange={(e) => {setPasswordSignup(e.target.value)}} type="password"/>
                        <input value={ConfirmPasswordSignup} id="ConfirmSingupPassword" placeholder="Confirmez votre mot de passe" type="password" onChange={(e) => {setConfirmPasswordSignup(e.target.value)}}/>
                    </div>

                    <div id="AddPDP">
                        {Image ? (
                        <img src={URL.createObjectURL(Image)} ></img>
                    ) : (
                        <img src={Temp_img}></img>
                    )}

                    <input placeholder="Url de la photo" type="file" onChange={(e) => {HandleImage(e)}}/>
                    </div>
                    
                    

                    <button onClick={() => {GoToAdresse()}}> Suivant </button>
                </div> }



            {IsSignIn && 
                <div id="Form">
                    <img src={Logo}/>
                    <div id="Login-input">
                        <input value={Login} placeholder="Votre mail" onChange={(e) => {setLogin(e.target.value)}}/> 
                        <input value={Password} type="password" placeholder="Votre Mot de passe" onChange={(e) => {setPassword(e.target.value)}}/> 
                        {ErrorPassword && <ErrorMsg/>}
                    </div>

                    <div id="tffc-2222">
                        <button onClick={() => {Bypassauth()}}> Connexion </button>
                        <a href="#"> Mot de passe oublié ?</a>
                    </div>

                    <div id="Inscrip">
                        <button onClick={() => {Signup()}}> Pas de compte ? <span> Créer un compte </span> </button>
                    </div>
                </div>}


                {
                    FormAdresse && 
                    <div id="Form-adresse">
                        <h3> On y est presque . . .  </h3>
                        <div id="Nom-prenom">
                            <input value={NumRue} id="N°" placeholder="N° " onChange={(e) => {setNumRue(e.target.value)}} style={{width :"15%"}}/> 
                            <input value={Rue} id="Rue" placeholder="Le nom de la rue" onChange={(e) => {setRue(e.target.value)}}/> 
                            <input value={CodePostal} id="CodePostal" placeholder="Code Postal" onChange={(e) => {setCodePostal(e.target.value)}} style={{width :"35%"}}/> 
                        </div>

                        <div id="Mail-Confirm"> 
                            <input value={Ville} id="Ville" placeholder="Ville" onChange={(e) => {setVille(e.target.value)}}/>
                        </div>

                        <button onClick={() => ConfirmMailByCode()}> Suivant </button>
                    </div>
                }

        </div>
    )
}


export default Authentification; 