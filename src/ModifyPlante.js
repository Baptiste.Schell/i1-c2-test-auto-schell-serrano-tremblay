import React, { useState, useEffect } from "react";
import { useNavigate,useLocation } from "react-router";
import axios from "axios";
import NavbarHaut from "./Component/NavbarHaut";
import NavbarLateral from "./Component/NavbarLateral";
import "./Assets/CSS/ModifyPlante.css"
import Temp_img from "./Assets/image/no-image.jpg"

function ModifyPlante()
{
    const location=useLocation() ;
    const navigate=useNavigate() ; 
    const [nom,setNom]=useState(location.state.nom)
    const [description,setDescription]=useState(location.state.description)
    const [_url,setUrl]=useState(location.state.url)
    const [TypePlante,setTypePlante]=useState(location.state._libelle)
    const [idPlante,setIdPlante]=useState(location.state.idPlante)
    const [ListTypePlant,setListTypePlant]=useState([])
    const [SelectedTypePlant,setSelectedTypePlant]=useState(TypePlante)

    useEffect(() => {
        axios({
            headers : {"authorization" : 'Bearer '+localStorage["Token"]},
            method : 'get',
            url :process.env.REACT_APP_server+"TypePlantes/"
        }).then((resp) => {
            setListTypePlant(resp.data)
        })
    },[])

    function UpdatePlante() 
    {
        axios({
            headers : {"authorization" : 'Bearer '+localStorage["Token"]},
            method :'get',
            url :process.env.REACT_APP_server+"GetTypesPlantsByName/"+SelectedTypePlant
        }).then((resp) => {
            axios({
                headers : {"authorization" : 'Bearer '+localStorage["Token"]},
                method : 'POST',
                url :process.env.REACT_APP_server+"UpdatePlante/"+idPlante+"/"+nom+"/"+description+"/"+resp.data[0].idTypePlante,
                data: {
                    url: _url,
                  }
            }).then((resp) => {
                if(resp.data==='OK')
                {
                    navigate('/Mesplantes')
                }
            })
                })

    }
    
    return(
        <div style={{ width: "100%", height: "100%" }}>
        <div id="Mesplantes">
            <NavbarLateral />
        </div>
        <NavbarHaut title="Modifier ma plante" />
    
        <div id="Main_Panel_plante">
            <div id="xcd587415">
                {_url ? (
                    <img src={_url} ></img>
                ) : (
                    <img src={Temp_img}></img>
                )}
                <fieldset>
                    <input value={nom} onChange={(e) => {setNom(e.target.value)}}/>
                </fieldset>
              
                <fieldset>
                    <input value={description} onChange={(e) => {setDescription(e.target.value)}}/>
                </fieldset>

                <fieldset>
                    <select id="SelectPlant"  onChange={(e) => {setSelectedTypePlant(e.target.value)}}>
                        <option> {TypePlante} </option>
                        {ListTypePlant.map((item) => {
                                return( <option> {item.libelle} </option>)
                        })}
                    </select>
                </fieldset>

                <fieldset>
                    <input value={_url} onChange={(e) => {setUrl(e.target.value)}}/>
                </fieldset>

                <div id="button-plante">
                        <button class="boutton-ajouter-plante" onClick={() => {UpdatePlante()}}> Enregistrer </button>
                        <button class="boutton-annuler-add-plante" onClick={() => {navigate("/Mesplantes")}}> Annuler </button>
                </div>
            </div>
        </div>
        </div>
    )
}

export default ModifyPlante ;