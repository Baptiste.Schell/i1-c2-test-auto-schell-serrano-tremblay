import React, { useEffect, useState } from "react";
import NavbarHaut from "./Component/NavbarHaut";
import NavbarLateral from "./Component/NavbarLateral"
import "./Assets/CSS/FindPlant.css"
import Map from "./Component/Map";
import Card from "./Component/Card_Map"
import axios from "axios"
import { confirmAlert } from "react-confirm-alert";
import "react-confirm-alert/src/react-confirm-alert.css";


function FindPlant () 
{
    const [User,setUser]= useState("")
    const [Ville,setVille]=useState("") 
    const [Plante,setPlante]=useState("")
    const [DateDebut,setDateDebut]=useState("null")
    const [DateFin,setDateFin]=useState("null")
    const [Annonce,setAnnonce]=useState([])
    const [niveauExpertise,setNiveauExpertise]=useState("null")
    const [List_Adresse,setListe_Adresse]=useState([])
    const [AucuneAnnonce,setAucuneAnnonce]=useState(false)

    useEffect(() => {

        axios({
            headers : {"authorization" : 'Bearer '+localStorage["Token"]},
          method : 'GET',
          url : process.env.REACT_APP_server + "GetUserByMail/" + localStorage["UserMail"]
        }).then((resp) => {
          setUser(resp.data[0])
        });
    },[])



    function SearchPlante() 
    {
       if(document.getElementById("InputVille").value==="") 
       {
            document.getElementById("InputVille").style.backgroundColor="#e34267"
       }
       else if(document.getElementById("InputDateDebut").value!="" && document.getElementById("InputDateFin").value==="")
       {

            document.getElementById("InputDateFin").style.backgroundColor="#e34267"            
       }
       else if(document.getElementById("InputDateDebut").value==="" && document.getElementById("InputDateFin").value!="")
       {

            document.getElementById("InputDateDebut").style.backgroundColor="#e34267"            
       }
       else if(niveauExpertise==="null")
       {

            document.getElementById("SelectInput").style.backgroundColor="#e34267"            
       }
       
       else 
       {
        var temp_niveau="null" ; 

        if(niveauExpertise==="Débutant")
        {
            temp_niveau=1
        }
        else if(niveauExpertise==="Connaisseur")
        {
            temp_niveau=2
        }
        else if (niveauExpertise==="Professionnel")
        {
            temp_niveau=3 
        }

        
     axios({  // POUR TROUVER UNE PLANTE SELON LA VILLE ET LES DATES
        headers : {"authorization" : 'Bearer '+localStorage["Token"]},
        method : "GET",
        url : process.env.REACT_APP_server+"Search_plante_by_location_and_date/"+Ville+"/"+temp_niveau+"/"+DateDebut+"/"+DateFin+"/"+localStorage["UserMail"]
      }).then((response) => {
        if(response.data==='AUCUNE ANNONCE')
        {
            setAucuneAnnonce(true)
            setAnnonce([])
        }
        else
        {
            setAucuneAnnonce(false)
            setAnnonce(response.data)

        }
      })
       }
    }
    
    const handleChangeDateDebut = (e) => {
        setDateDebut(e.target.value);
          };

        const handleChangeDateFin = (e) => {
            setDateFin(e.target.value);
              };
    
    return(
        <div style={{width : '100%' , height:'100%'}}>
            <div id="Navbar"> 
                <NavbarLateral/>
            </div>
            <NavbarHaut title="Carte des plantes à garder"/>

            <div id="Main_Panel">
                    <div id="Map_and_card">
                        <div id="Map">
                            <Map _Annonce={Annonce} ville="Montpellier"/>
                        </div>

                        <div id="Search-Find">
                            {Annonce.length!=0 &&   
                            <div id="Search-Find-Nbr-Annonce">
                                <p> {Annonce.length} Annonces trouvés </p>
                            </div>}
                           
                            
                            <div id="Search-Find-Search-bar"> 
                                <div id="Search-Find-Search-bar-1">
                                    <input placeholder="Ville/Quartier" value={Ville} onChange={(e) => {setVille(e.target.value)}} id="InputVille"/>
                                    <select onChange={(e) => {setNiveauExpertise(e.target.value)}} id="SelectInput">
                                        <option> Niveau d'expertise</option>
                                        <option> Débutant </option>
                                        <option> Connaisseur </option>
                                        <option> Professionnel </option>
                                    </select>
                                </div>

                                <div id="Search-Find-Search-bar-2">
                                    <div className="Search-Find-Search-bar-2-class"> 
                                        <label> Date de début prise en charge </label>
                                        <input type="date" placeholder="Date de début prise en charge" value={DateDebut} onChange={handleChangeDateDebut} id="InputDateDebut"/>
                                    </div>

                                    <div className="Search-Find-Search-bar-2-class">
                                        <label> Date de fin de prise en charge </label>
                                        <input type="date" placeholder="Date de fin prise en charge"  value={DateFin}  onChange={handleChangeDateFin} id="InputDateFin"/>
                                    </div>
                                </div>

                                <button id="Search-Find-Search-bar-button" onClick={() => {SearchPlante()}}> Rechercher </button>

                            </div>

                            <div id="Search-Find-Result">
                                
                           
                            {AucuneAnnonce===false ? Annonce.map((item) => {
                              return (<Card Date_Fin={item.dateFin} Date_Debut={item.dateDebut} ville={item.ville}
                                            url_pdp={item.photodeprofil} Description={item.description}
                                            nom={item.nom} prenom={item.prenom} mail={item.mail} reference={item.reference} 
                                            expertise={item.idNiveauExpertiseRequis} idAnnonce={item.idAnnonce} idUser={User.idUser} Pseudo={item.pseudo}/> )
                            }) : <div id='AucuneAnnonceTrouve'><p> Aucune Annonce trouvé ! </p></div> }

                            </div>
                        </div>
                  </div>
                    
            </div> 
        </div>
    )

}


export default FindPlant ; 