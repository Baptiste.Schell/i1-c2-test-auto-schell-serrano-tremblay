import React, { useState, useEffect } from "react";
import { useNavigate,useLocation } from "react-router";
import axios from "axios";
import NavbarHaut from "./Component/NavbarHaut";
import NavbarLateral from "./Component/NavbarLateral";
import "./Assets/CSS/ModifieConseil.css"

function PosterConseil()
{

    const navigate=useNavigate() ; 
    const [titre,setTitre]=useState("")
    const [description,setDescription]=useState("")
    const [ListTypePlant,setListTypePlant]=useState([])
    const [SelectedTypePlant,setSelectedTypePlant]=useState("")

    useEffect(() => {
        axios({
            headers : {"authorization" : 'Bearer '+localStorage["Token"]},
            method : 'get',
            url :process.env.REACT_APP_server+"GetTypePlants/"
        }).then((resp) => {
            setListTypePlant(resp.data)
        })


    },[])

    function AddConseil()
    {
        axios({
            headers : {"authorization" : 'Bearer '+localStorage["Token"]},
            method :'get',
            url :process.env.REACT_APP_server+"GetTypesPlantsByName/"+SelectedTypePlant
        }).then((resp) => {
            axios({
                headers : {"authorization" : 'Bearer '+localStorage["Token"]},
                method : "POST",
                url : process.env.REACT_APP_server+"CreateConseil/"+localStorage["UserMail"]+"/"+resp.data[0].idTypePlante,
                data: {
                    titre :titre,
                    description :description
                  }
            }).then((resp) => {
                if(resp.data==='OK')
                {
                    navigate('/MesConseils')
                }
            })
                })    
    }

    
    return(
        <div style={{ width: "100%", height: "100%" }}>
        <div id="MesConseils">
            <NavbarLateral />
        </div>
        <NavbarHaut title="Poster un conseil" />
    
        <div id="Main_Panel_conseil">
            <div id="xcd587415">
                <fieldset>
                    <input placeholder="Titre" value={titre} onChange={(e) => {setTitre(e.target.value)}}/>
                </fieldset>
                <fieldset class="input-description">
                    <input placeholder="Description" value={description} onChange={(e) => {setDescription(e.target.value)}}/>
                </fieldset>
                <fieldset>
                    <select id="SelectPlant"  onChange={(e) => {setSelectedTypePlant(e.target.value)}}>
                        <option> Type de plante</option>
                        {ListTypePlant.map((item) => {
                                return( <option> {item.libelle} </option>)
                                
                        })}
                    </select>
                </fieldset>      
                <div id="button-conseil">
                        <button class="boutton-ajouter-conseil" onClick={() => {AddConseil()}}> Ajouter </button>
                        <button class="boutton-annuler-add-conseil" onClick={() => {navigate("/MesConseils")}}> Annuler </button>
                </div>
            </div>
        </div>
        </div>
    )
}

export default PosterConseil ;