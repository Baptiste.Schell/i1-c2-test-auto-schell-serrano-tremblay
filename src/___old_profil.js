import NavbarHaut from "./Component/NavbarHaut";
import NavbarLateral from "./Component/NavbarLateral"
import { useState,useEffect } from "react";
import axios from "axios";
import { useNavigate } from "react-router-dom";
import "./Assets/CSS/Profil.css"
import React from "react";

function Profil(){

// state (état, données)
const [User, setUser]=useState("")
const [Role, setRole]=useState("")
const [Plante, setPlante]=useState([])
const [Adresse, setAdresse]=useState("")
const [postAnnonces, setPostAnnonces] = useState([]);
const [keepAnnonces, setKeepAnnonces]=useState([]);
const [switchFilters, setSwitchFilters]=useState(true)
const [keepAnnoncesCount, setKeepAnnoncesCount] = useState("")
const [postAnnoncesCount, setPostAnnoncesCount] = useState("")
const [planteCount, setPlanteCount]= useState("")
const navigate=useNavigate()
//const AnnonceFiltered = useMemo(() => postAnnonces.filter(item => ) )

// comportement
useEffect(() => {

  axios({
    method : 'GET',
    url : process.env.REACT_APP_server + "GetUserByMail/" + localStorage["UserMail"]
  }).then((resp) => {
    setUser(resp.data[0])
  });

  axios({
    method : 'GET',
    url : process.env.REACT_APP_server + "GetRole/" + localStorage["UserMail"]
  }).then((resp) => {
    setRole(resp.data[0])
  });

  axios({
    method : 'GET',
    url : process.env.REACT_APP_server + "GetAdresse/" + localStorage["UserMail"]
  }).then((resp) => {
    setAdresse(resp.data[0])
  });

  axios({
    method : 'GET',
    url : process.env.REACT_APP_server + "GetPlantByUser/" + localStorage["UserMail"]
  }).then((resp) => {
    setPlante(resp.data)
    if (resp.data !== undefined) { // vérification que resp.data est défini
      setPlanteCount(resp.data.length); // ajout de cette ligne pour compter le nombre de plantes de l'utilisateur 
    } else {
      setPlanteCount(0); // si resp.data est indéfini, on initialise la valeur de count à 0
    }
  });

  axios({
    method : 'GET',
    url : process.env.REACT_APP_server + "LoadPostAnnonces/" + localStorage["UserMail"]
  }).then((resp) => {
    setPostAnnonces(resp.data)
    if (resp.data !== undefined) { // vérification que resp.data est défini
      setPostAnnoncesCount(resp.data.length); // ajout de cette ligne pour compter le nombre d'annonces postée
    } else {
      setPostAnnoncesCount(0); // si resp.data est indéfini, on initialise la valeur de count à 0
    }
  });

  axios({
    method : 'GET',
    url : process.env.REACT_APP_server + "LoadKeepAnnonces/" + localStorage["UserMail"]
  }).then((resp) => {
    setKeepAnnonces(resp.data)
    if (resp.data !== undefined) { // vérification que resp.data est défini
      setKeepAnnoncesCount(resp.data.length); // ajout de cette ligne pour compter le nombre d'annonces postée
    } else {
      setKeepAnnoncesCount(0); // si resp.data est indéfini, on initialise la valeur de count à 0
    }
  });
  
},[])

// affichage

    return(
      <div style={{ width: '100%', height: '100%' }}>
      <div id="Profil">
        <NavbarLateral />
      </div>
      <NavbarHaut title="Profil" />
    
      <div id="Main_Panel">
        <div class="container margin border">
          <div class="border">
            <h2 class="h2_perso">Informations personnelles :
              <button type="button" class="btn btn-light btn_edit" data-mdb-ripple-color="dark" onClick={() => { navigate("/EditProfil") }}>
                Modifier
              </button>
            </h2>
          </div>
          <div class="container">
          <div class="row margin border">
            <div class="col">
              <img src={User.photodeprofil} alt="..." class="border-dark img_reduit img-fluid img-thumbnail" />
            </div>
            <div class="col-6">
              <p>Pseudo</p>
              <p class="text-muted">{User.pseudo}</p>
              <p>Nom</p>
              <p class="text-muted">{User.nom}</p>
              <p>Mail</p>
              <p class="text-muted">{User.mail}</p>
              <p>Téléphone</p>
              <p class="text-muted">{User.telephone}</p>
            </div>
            <div class="col">
              <p>Status</p>
              <p class="text-muted">{Role.label}</p>
              <p>Prénom</p>
              <p class="text-muted">{User.prenom}</p>
            </div>
          </div>
          </div>
          <hr />
          </div>
          <div class="container">
          <h2 class="h2_perso">Adresse :</h2>
          </div>
          <div class="container">
            <div class="col-sm">
              <p>Rue :</p>
              <p class="text-muted">{Adresse.rue}</p>
              <p>Numéro de voie :</p>
              <p class="text-muted">{Adresse.voie}</p>
              <p>Complément d'adresse :</p>
              <p class="text-muted">{Adresse.complementAdresse}</p>
            </div>
            <div class="col-sm">
              <p>Ville :</p>
              <p class="text-muted">{Adresse.ville}</p>
            </div>
            <div class="col-sm">
              <p>Code postal :</p>
              <p class="text-muted">{Adresse.CP}</p>
            </div>
          </div>
        <div class="container">
          <h2 class="h2_perso">Mes Annonces :</h2>
        </div>
        <div class="container">
          <div class="px-3">
            <p class="mb-1 h5">{planteCount}</p>
            <p class="small text-muted mb-0">Plantes</p>
          </div>
          <div class="px-3">
            <p class="mb-1 h5">{keepAnnoncesCount}</p>
            <p class="small text-muted mb-0">Gardée</p>
          </div>
          <div>
            <p class="mb-1 h5">{postAnnoncesCount}</p>
            <p class="small text-muted mb-0">Postée</p>
          </div>
          <button onClick={() => setSwitchFilters(false)}>Garder</button>
          <button onClick={() => setSwitchFilters(true)}>Poster</button>
        </div><br></br>
        <div className="col-sm">
          {switchFilters ? (
            postAnnonces.map((item) => (
              <div className="col-sm" key={item.idAnnonce}>
                <p>Date debut: {item.dateDebut}</p>
                <p>Date fin: {item.dateFin}</p>
                <p>Description: {item.description}</p>
                <p>NbEtape: {item.nbEtape}</p>
                <p>Reference: {item.reference}</p>
                <hr></hr>
              </div>
            ))
          ) : (
            keepAnnonces.map((item) => (
              <div className="col-sm" key={item.idAnnonce}>
                <p>Date debut: {item.dateDebut}</p>
                <p>Date fin: {item.dateFin}</p>
                <p>Description: {item.description}</p>
                <p>NbEtape: {item.nbEtape}</p>
                <p>Reference: {item.reference}</p>
                <p>Numéro de réservation: {item.numero}</p>
              </div>
            ))
          )}
        </div>
          </div>
        </div>
    )
}
export default Profil;
