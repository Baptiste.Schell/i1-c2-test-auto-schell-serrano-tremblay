import React from "react";
import NavbarHaut from "./Component/NavbarHaut";
import NavbarLateral from "./Component/NavbarLateral"
import { useState,useEffect} from "react";
import axios from "axios";
import Temp_img from "./Assets/image/no-image.jpg";
import {useNavigate} from "react-router-dom";


function MonAnnonce () {
    const [Annonce, setAnnonce] = useState("")
    const [Photos,setPhotos] = useState([])
    const [Keeper, setKeeper] = useState("")
    const navigate=useNavigate() ;

    // comportement
    useEffect(() => {
    axios({
        headers : {"authorization" : 'Bearer '+localStorage["Token"]},
        method : 'GET',
        url : process.env.REACT_APP_server + "GetAnnoncesActive/" + localStorage['UserMail']
      }).then((resp) => {
        setAnnonce(resp.data[0])
      });
    },[])

    useEffect(() => {
        if (Annonce.idAnnonce) {
            axios({
                headers : {"authorization" : 'Bearer '+localStorage["Token"]},
                method: 'GET',
                url: process.env.REACT_APP_server + "GetPhotosByAnnoncesWithComments/" + Annonce.idAnnonce
            }).then((resp) => {
                setPhotos(resp.data);
            });
        }
    }, [Annonce.idAnnonce])

    useEffect(() => {
        if (Annonce.idAnnonce) {
          axios({
            headers : {"authorization" : 'Bearer '+localStorage["Token"]},
            method: 'GET',
            url: process.env.REACT_APP_server + "GetKeeperByAnnonce/" + Annonce.idAnnonce
          }).then((resp) => {
            if (resp.data === undefined) {
              setKeeper("Aucun gardien pour le moment");
            } else {
              setKeeper(resp.data[0]);
            }
          });
        }
      }, [Annonce.idAnnonce]);

    function parseDate(dateString) {
        const parts = dateString.split('-');
        return new Date(parts[0], parts[1] - 1, parts[2]);
      }
  

    return (
    <div style={{ width: '100%', height: '100%' }}>
      <div id="VisitProfil">
        <NavbarLateral />
      </div>
      <NavbarHaut title="Mon annonce" />

        <div id="Main_Panel">
            <div className="page-suivis-annonce">
                <div className="card-principale-suivis-annonce">
                    <div className="title-suivis-annonce">
                        <div className="title-left-suivis-annonce">
                            <p className="title-left-gras-suivis-annonce">Annonce
                                du {Annonce.dateDebut} au {Annonce.dateFin}</p>
                            <p className="title-left-ligth-suivis-annonce">Réf : {Annonce.numero}</p>
                        </div>
                        <div className="title-right-suivis-annonce">
                            <div className="title-right-left-suivis-annonce">
                                <p className="title-right-left-nomprenom-suivis-annonce">Gardé par :{Keeper ? Keeper.pseudo : ' Aucun gardien pour le moment'}</p>
                            </div>
                                {Keeper ? (
                                    <img src={Keeper.photodeprofil} className="title-right-center-suivis-annonce" onClick={() => {
                                        navigate("/VisitProfil", { state: { mail: Keeper.mail } });
                                    }}/>
                                ) : (
                                    <img src={Temp_img} className="title-right-center-suivis-annonce"/>
                                )}
                        </div>
                    </div>
                    <div className="cycle-nbetape-section-suivis-annonce">
                        <div className="cycle-section-suivis-annonce">
                            <p className="label-cycle-nbetape-suivis-annonce">Cycle :</p>
                            <p className="light-label-cycle-nbetape-suivis-annonce">Tous
                                les {Annonce.idCycleCompteRendu} jours</p>
                        </div>
                        <div className="cycle-section-suivis-annonce">
                            <p className="label-cycle-nbetape-suivis-annonce">Nombre d'étapes :</p>
                            <p className="light-label-cycle-nbetape-suivis-annonce">{Annonce.nbEtape}</p>
                        </div>
                        <div className="cycle-section-suivis-annonce">
                            <p className="label-cycle-nbetape-suivis-annonce">Expertise :</p>
                            <p className="light-label-cycle-nbetape-suivis-annonce">{
                                Annonce.idNiveauExpertiseRequis === 1 ? "Débutant" :
                                    Annonce.idNiveauExpertiseRequis === 2 ? "Intermédiaire" :
                                        Annonce.idNiveauExpertiseRequis === 3 ? "Expert" : ""}</p>
                        </div>
                    </div>
                    <div className="section-description-suivis-annonce">
                        <p className="label-cycle-nbetape-suivis-annonce">Description :</p>
                        <p className="light-label-cycle-nbetape-suivis-annonce">{Annonce.description}</p>
                    </div>
                    {Photos.map((item) => (
                        <>
                            <div className="divider"></div>
                            <div className="section-photo-suivis-conseil">
                                {item.urlPhoto ? (
                                    <img src={item.urlPhoto} className="photo-suivis-annonce"/>
                                ) : (
                                    <img src={Temp_img} className="photo-suivis-annonce"/>
                                )}
                                <div className="right-photo-suivis-annonce">
                                    <p className="date-de-poste-label">Date de poste</p>
                                    <p className="date-de-poste">{item.datePoste}</p>

                                    {item.description && item.urlPhoto ? (
                                        <>
                                            <p className="date-de-poste-label">Commentaire :</p>
                                            <p className="date-de-poste">{item.description}</p>
                                        </>
                                    ) : (
                                        <p className="date-de-poste">Aucun commentaire pour cette photo</p>
                                    )}
                                </div>
                            </div>
                        </>
                    ))}
                    <div className="boutton-de-merde">
                        <p className="contacter-proprio">Contacter le propriétaire</p>
                        <p className="numero-telephone-suivis-annonce">
                        {Keeper ? Keeper.telephone : "Pas de gardien"}
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>

)}
export default MonAnnonce;
