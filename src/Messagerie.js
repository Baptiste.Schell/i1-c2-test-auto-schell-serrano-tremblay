import NavbarLateral from "./Component/NavbarLateral";
import NavbarHaut from "./Component/NavbarHaut";
import axios from "axios"
import { useState,useEffect } from "react";
import React from "react";
import "./Assets/CSS/Messagerie.css"
import "./Component/Conversation"
import Conversation from "./Component/Conversation";
import MessageEnvoye from "./Component/MessageEnvoye";
import MessageRecu from "./Component/MessageReçu";
import { useLocation } from "react-router-dom";

function Messagerie ()
{
    const location=useLocation() ;
    const [ConversationList,setConversationList]=useState([])
    const [MessageFromConversation,setMessageFromConversation]=useState([])
    const [ConvHeadTitle,setConvHeadTitle]=useState("")
    const [NewMessage,setNewMessage]=useState("")
    const [PDP_USER,setPDP_USER]=useState("")
    const [PDP_Destinataire,setPDP_Destinataire]=useState("")
    const [ConvPDP,setConvPDP]=useState()
    const [NewConv,setNewConv]=useState(location.state.CreateNewConv ? location.state.CreateNewConv : "")
    

    function SendMessage()
    {

        axios({
            headers :{"authorization" : 'Bearer '+localStorage["Token"] },
            method :'POST',
            url : process.env.REACT_APP_server+"SendMessage/"+localStorage["Pseudo"]+"/"+ConvHeadTitle+"/"+NewMessage
        }).then((resp) => {
            if(resp.data==="OK")
            {
                ReloadConversation(ConvHeadTitle)
                axios({
                    headers :{"authorization" : 'Bearer '+localStorage["Token"] },
                    method :'get',
                    url : process.env.REACT_APP_server+"GetConversation/"+localStorage["Pseudo"]
                }).then((resp) => {
                    if(Array.isArray(resp.data))
                    {
                        setConversationList(resp.data)
                        
                        
            
                    }
                    else
                    {
                        console.log(resp.data)
                    }
                })
                setNewMessage("")
            }
        })
    }


    function LoadMessageFromConversation(_PseudoFrom,_PseudoTo) 
    {
        
        if(_PseudoFrom===localStorage["Pseudo"])
        {
            setConvHeadTitle(_PseudoTo)
            axios({
                headers :{"authorization" : 'Bearer '+localStorage["Token"] },
                method :'get',
                url : process.env.REACT_APP_server+"GetPDPByPseudo/"+_PseudoTo
            }).then((resp) => {
                setPDP_Destinataire(resp.data[0].photodeprofil)
                setConvPDP(resp.data[0].photodeprofil)
                
            })
        }
        else if(_PseudoTo===localStorage["Pseudo"])
        {
            setConvHeadTitle(_PseudoFrom)
            axios({
                headers :{"authorization" : 'Bearer '+localStorage["Token"] },
                method :'get',
                url : process.env.REACT_APP_server+"GetPDPByPseudo/"+_PseudoFrom
            }).then((resp) => {
                setPDP_Destinataire(resp.data[0].photodeprofil)
                setConvPDP(resp.data[0].photodeprofil)
                
            })
        }

        

        axios({
            headers :{"authorization" : 'Bearer '+localStorage["Token"] },
            method :'get',
            url : process.env.REACT_APP_server+"LoadMessageByConversation/"+_PseudoTo+"/"+_PseudoFrom
        }).then((resp) => {
            if(Array.isArray(resp.data))
            {
                setMessageFromConversation(resp.data)
                console.log(resp.data)
                
                
    
            }
            else
            {
                console.log(resp.data)
            }
        })
    }

    function ReloadConversation(_Pseudo)
    {
        
        axios({
            headers :{"authorization" : 'Bearer '+localStorage["Token"] },
            method :'get',
            url : process.env.REACT_APP_server+"LoadMessageByConversation/"+localStorage["Pseudo"]+"/"+_Pseudo
        }).then((resp) => {
            if(Array.isArray(resp.data))
            {
                setMessageFromConversation(resp.data)
                
                
    
            }
            else
            {
                console.log(resp.data)
            }
        })
    }
    
    useEffect(() => {
        axios({
            headers :{"authorization" : 'Bearer '+localStorage["Token"] },
            method :'get',
            url : process.env.REACT_APP_server+"GetConversation/"+localStorage["Pseudo"]
        }).then((resp) => {
            if(Array.isArray(resp.data))
            {
                setConversationList(resp.data)
                
                
    
            }
            else
            {
                console.log(resp.data)
            }
        })
       
        /*if(typeof location.state.CreateNewConv!=="undefined") // Pour Créeer une nouvelle conversation 
        {
            setConvHeadTitle(location.state.Pseudo)
            setConvPDP(location.state.PDP)
            
        }*/
        alert(NewConv)

        axios({
            headers :{"authorization" : 'Bearer '+localStorage["Token"] },
            method :'get',
            url : process.env.REACT_APP_server+"GetPDPByPseudo/"+localStorage["Pseudo"]
        }).then((resp) => {
            setPDP_USER(resp.data[0].photodeprofil)
            
        })
        
    },[])
    


    return(
        <div style={{ width: '100%', height: '100%' }}>
        <div id="Navbar">
            <NavbarLateral />
        </div>
        <NavbarHaut title="Messagerie" />
        
        <div id="Main_Panel">
            
            <div style={{width :'30%',height :'100%',borderRight :'2px solid grey'}}>
                <div id="Search-Bar-div">
                    <i class="fa-solid fa-magnifying-glass"></i>
                    <input id="SearchInput" type="search" placeholder="Rechercher une message, une conversation..." autofocus required />
                </div>

                <div id="Conversation">
                    {
                        ConversationList.length===0 ? <p> Aucun message</p> : 
                        ConversationList.map((item) => {
                                return(
                                <div style={{width : '100%'}} onClick={() => {LoadMessageFromConversation(item.FromPseudo,item.ToPseudo)}}>
                                    <Conversation  From={item.FromPseudo} To={item.ToPseudo} Message={item.Message} horodatage={item.horodatage}/>
                                    <hr style={{color : 'grey',width :'70%',fontSize :"1em"}} ></hr>
                                </div>)
                        })
                        
                    }

                </div>
            </div>

            <div id="Message">
                    
                <div id="ConvHead">
                    <img src={ConvPDP}/>
                    <p>{ConvHeadTitle}</p>
                    <i class="fa-solid fa-rotate-right" onClick={() => {ReloadConversation(ConvHeadTitle)}} ></i>
                </div>
                <div id="AllMessage">
                    {   
                        MessageFromConversation.map((item) => {
                            if(item.FromPseudo===localStorage["Pseudo"])
                            {
                                return(<MessageEnvoye Content={item.Message} PDP={PDP_USER}/>)
                            }else
                            {
                                return(<MessageRecu Content={item.Message} PDP={PDP_Destinataire}/>)
                            }
                        })
                    }
                </div>
                <div id="x556322">
                    <input id="Input-Message" onChange={(e) => {setNewMessage(e.target.value)}} value={NewMessage}/>
                    <i class="fa-solid fa-paper-plane" onClick={() => {SendMessage()}}></i>
                </div>

            </div>
            
                
        </div>
    </div>
    )
}


export default Messagerie ; 