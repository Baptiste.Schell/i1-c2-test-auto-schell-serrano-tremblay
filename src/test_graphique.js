import React from 'react';
import { render, screen, fireEvent } from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect';
import Authentification from './Authentification';
import { BrowserRouter as Router } from 'react-router-dom';

describe('Authentification Component', () => {
    test('renders sign-in form initially', () => {
        render(
            <Router>
                <Authentification />
            </Router>
        );

        // Vérifie si les éléments de connexion sont présents
        expect(screen.getByPlaceholderText('Votre mail')).toBeInTheDocument();
        expect(screen.getByPlaceholderText('Votre Mot de passe')).toBeInTheDocument();
        expect(screen.getByText('Connexion')).toBeInTheDocument();
        expect(screen.getByText('Pas de compte ? Créer un compte')).toBeInTheDocument();
        expect(screen.getByText('Mot de passe oublié ?')).toBeInTheDocument();
    });

    test('renders sign-up form on "Créer un compte" click', () => {
        render(
            <Router>
                <Authentification />
            </Router>
        );

        // Clique sur le bouton "Créer un compte"
        fireEvent.click(screen.getByText(/Créer un compte/i));

        // Vérifie si les éléments du formulaire d'inscription sont présents
        expect(screen.getByPlaceholderText('Votre Nom')).toBeInTheDocument();
        expect(screen.getByPlaceholderText('Votre Prénom')).toBeInTheDocument();
        expect(screen.getByPlaceholderText('Votre Pseudo')).toBeInTheDocument();
        expect(screen.getByPlaceholderText('Votre Adresse mail')).toBeInTheDocument();
        expect(screen.getByPlaceholderText('Confirmez votre Adresse mail')).toBeInTheDocument();
        expect(screen.getByPlaceholderText('Votre Numéro de téléphone')).toBeInTheDocument();
        expect(screen.getByPlaceholderText('Mot de passe')).toBeInTheDocument();
        expect(screen.getByPlaceholderText('Confirmez votre mot de passe')).toBeInTheDocument();
    });

    test('renders address form on "Suivant" click in sign-up form', () => {
        render(
            <Router>
                <Authentification />
            </Router>
        );

        // Clique sur le bouton "Créer un compte"
        fireEvent.click(screen.getByText(/Créer un compte/i));

        // Remplit les champs requis du formulaire d'inscription
        fireEvent.change(screen.getByPlaceholderText('Votre Nom'), { target: { value: 'TestNom' } });
        fireEvent.change(screen.getByPlaceholderText('Votre Prénom'), { target: { value: 'TestPrenom' } });
        fireEvent.change(screen.getByPlaceholderText('Votre Pseudo'), { target: { value: 'TestPseudo' } });
        fireEvent.change(screen.getByPlaceholderText('Votre Adresse mail'), { target: { value: 'test@example.com' } });
        fireEvent.change(screen.getByPlaceholderText('Confirmez votre Adresse mail'), { target: { value: 'test@example.com' } });
        fireEvent.change(screen.getByPlaceholderText('Votre Numéro de téléphone'), { target: { value: '123456789' } });
        fireEvent.change(screen.getByPlaceholderText('Mot de passe'), { target: { value: 'password123' } });
        fireEvent.change(screen.getByPlaceholderText('Confirmez votre mot de passe'), { target: { value: 'password123' } });

        // Clique sur le bouton "Suivant"
        fireEvent.click(screen.getByText('Suivant'));

        // Vérifie si les éléments du formulaire d'adresse sont présents
        expect(screen.getByPlaceholderText('N°')).toBeInTheDocument();
        expect(screen.getByPlaceholderText('Le nom de la rue')).toBeInTheDocument();
        expect(screen.getByPlaceholderText('Code Postal')).toBeInTheDocument();
        expect(screen.getByPlaceholderText('Ville')).toBeInTheDocument();
    });
});
