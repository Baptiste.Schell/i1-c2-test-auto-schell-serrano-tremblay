import React from "react";
import NavbarHaut from "./Component/NavbarHaut";
import NavbarLateral from "./Component/NavbarLateral"
import { useState,useEffect} from "react";
import axios from "axios";
import "./Assets/CSS/BotanisteCSS/VisitProfilBotaniste.css"
import { useLocation } from "react-router-dom";
import {useNavigate} from "react-router-dom";


function VisitProfilBotaniste () {

    const navigate = useNavigate();
    const location=useLocation()
    const [Mail,setMail]=useState(location.state.mail)
    const [User, setUser]=useState("")
    const [Role, setRole]=useState("")
    const [conseils, setConseils] = useState([])
    const [conseilsCount, setConseilsCount]= useState("")
    const [Switcher,setSwitcher]=useState(false)
    // comportement
    useEffect(() => {

    axios({
        headers : {"authorization" : 'Bearer '+localStorage["Token"]},
      method : 'GET',
      url : process.env.REACT_APP_server + "GetUserByMail/" + Mail
    }).then((resp) => {
      setUser(resp.data[0])
    });
  
    axios({
        headers : {"authorization" : 'Bearer '+localStorage["Token"]},
      method : 'GET',
      url : process.env.REACT_APP_server + "GetRole/" + Mail
    }).then((resp) => {
      setRole(resp.data[0])
    });
    axios({
        headers : {"authorization" : 'Bearer '+localStorage["Token"]},
        method : 'GET',
        url : process.env.REACT_APP_server + "GetConseilsByBotaniste/" + Mail
      }).then((resp) => {
        setConseils(resp.data)
        setConseilsCount(resp.data.length)
      });
},[])

    return (
    <div style={{ width: '100%', height: '100%' }}>
      <div id="VisitProfil">
        <NavbarLateral />
      </div>
      <NavbarHaut title={"Profil de " + User.pseudo}/>

        <div id="Main_Panel">
            <div className="card-entiere-profil">
                <div id="Profil-card">
                    <div className="information-perso-titre">
                        <p className="titre-gauche-info-perso">Informations personnelles</p>
                    </div>
                    <div className="information-perso-section">
                        <img src={User.photodeprofil} className="information-perso-pdp"></img>
                        <div className="information-perso-rigth">

                            <div className="information-perso-nom-prenom">
                                <div className="information-perso-cadre">
                                    <p className="information-perso-label-title"> Pseudo </p>
                                    <p className="information-perso-label"> {User.pseudo} </p>
                                </div>
                                <div className="information-perso-cadre">
                                    <p className="information-perso-label-title"> Status </p>
                                    <p className="information-perso-label"> {Role.label} </p>
                                </div>
                            </div>
                            <div className="information-perso-nom-prenom">
                                <div className="information-perso-cadre">
                                    <p className="information-perso-label-title"> Nom </p>
                                    <p className="information-perso-label"> {User.nom} </p>
                                </div>
                                <div className="information-perso-cadre">
                                    <p className="information-perso-label-title"> Prénom </p>
                                    <p className="information-perso-label"> {User.prenom} </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="section-menu-choix-profil">
                    <button onClick={() => {
                        setSwitcher(false)
                    }} className="bouton-choix-annonces-profil">
                        Mes conseils
                    </button>
                </div>

                <div className="section-card-mes-annonces">
                    {conseils.map((item) => {
                        return (
                            <>
                                <div className="card-mon-conseil">
                                    <div className="section-title-top-conseil">
                                        <p className="text-title-top-conseil">{item.titre}</p>
                                    </div>
                                    <div className="div-de-separation-mon-conseil"></div>
                                    <div className="section-bottom-conseil">
                                        <img className="img-conseil-card" src={item.urlPhoto}/>
                                        <div className="right-img-conseil-card">
                                            <div className="button-type-plant-conseil">
                                                <p className="text-type-plant-conseil" onClick={() => {navigate('/AllConseilsForTypePlante',{state:{idTypePlante : item.idTypePlante}})}}>{item.libelle}</p>
                                            </div>
                                            <div className="description-card-conseil">
                                                <p className="title-desc-conseil">Conseil</p>
                                                <p className="paragraph-desc-conseil">{item.description}</p>
                                            </div>

                                        </div>
                                    </div>
                                </div>

                            </>
                        )
                    })
                    }
                </div>
            </div>
        </div>
    </div>
    )
}
export default VisitProfilBotaniste ; 
