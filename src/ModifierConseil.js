import React, { useState, useEffect } from "react";
import { useNavigate,useLocation } from "react-router";
import axios from "axios";
import NavbarHaut from "./Component/NavbarHaut";
import NavbarLateral from "./Component/NavbarLateral";
import "./Assets/CSS/ModifieConseil.css"

function ModifierConseil()
{

    const location=useLocation() 
    const navigate=useNavigate() 
    const [titre,setTitre]=useState(location.state.titre)
    const [description,setDescription]=useState(location.state.description)
    const [TypePlante,setTypePlante]=useState(location.state.libelle)
    const [idConseil,setIdConseil]=useState(location.state.idConseil)
    const [urlPhoto,setUrlPhoto]=useState(location.state.urlPhoto)
    const [ListTypePlant,setListTypePlant]=useState([])
    const [SelectedTypePlant,setSelectedTypePlant]=useState(TypePlante)

    useEffect(() => {
        axios({
            headers : {"authorization" : 'Bearer '+localStorage["Token"]},
            method : 'get',
            url :process.env.REACT_APP_server+"GetTypePlants/"
        }).then((resp) => {
            setListTypePlant(resp.data)
        })
    },[])

    function UpdateConseil() 
    {
        axios({
            headers : {"authorization" : 'Bearer '+localStorage["Token"]},
            method :'get',
            url :process.env.REACT_APP_server+"GetTypesPlantsByName/"+SelectedTypePlant
        }).then((resp) => {
            axios({
                headers : {"authorization" : 'Bearer '+localStorage["Token"]},
                method : 'POST',
                url :process.env.REACT_APP_server+"UpdateConseil/"+titre+"/"+description+"/"+resp.data[0].idTypePlante+"/"+idConseil
            }).then((resp) => {
                if(resp.data==='OK')
                {
                    navigate('/MesConseils')
                }
            })
                })

    }
    
    return(
        <div style={{ width: "100%", height: "100%" }}>
        <div id="MesConseils">
            <NavbarLateral />
        </div>
        <NavbarHaut title="Modifier mon conseil" />

        <div id="Main_Panel_conseil">
            <div id="xcd587415">
                <img src={urlPhoto}></img>
                <fieldset>
                    <input value={titre} onChange={(e) => {setTitre(e.target.value)}}/>
                </fieldset>
              
                <fieldset>
                    <input value={description} onChange={(e) => {setDescription(e.target.value)}}/>
                </fieldset>

                <fieldset>
                    <select id="SelectPlant"  onChange={(e) => {setSelectedTypePlant(e.target.value)}}>
                        <option> {TypePlante} </option>
                        {ListTypePlant.map((item) => {
                                return( <option> {item.libelle} </option>)
                        })}
                    </select>
                </fieldset>
                <div id="button-conseil">
                        <button class="boutton-ajouter-conseil" onClick={() => {UpdateConseil()}}> Enregistrer </button>
                        <button class="boutton-annuler-add-conseil" onClick={() => {navigate("/MesConseils")}}> Annuler </button>
                </div>
            </div>
        </div>
        </div>
    )
}

export default ModifierConseil ;