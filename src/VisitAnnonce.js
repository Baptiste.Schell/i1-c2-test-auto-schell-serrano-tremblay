import React, { useState, useEffect } from "react";
import axios from "axios";
import NavbarHaut from "./Component/NavbarHaut";
import NavbarLateral from "./Component/NavbarLateral";
import "./Assets/CSS/Mesplantes.css";
import VisitPlante from "./Component/VisitPlante"
import { useNavigate,useLocation } from "react-router";



function VisitAnnonce() {
    const location=useLocation() 
    const navigate=useNavigate()
    const [PlantesList, setPlantes] = useState([]);
    const [UserId,setUserId] = useState(location.state.idUser)
    const [User,setUser] = useState("")

    useEffect(() => {
        axios({
            headers : {"authorization" : 'Bearer '+localStorage["Token"]},
            method: "GET",
            url: process.env.REACT_APP_server + "GetPlantByUserId/" + UserId
        })
        .then((resp) => {
            setPlantes(resp.data);
        })

        axios({
            headers : {"authorization" : 'Bearer '+localStorage["Token"]},
            method: "GET",
            url: process.env.REACT_APP_server + "GetUserByID/" + UserId
        })
        .then((resp) => {
            setUser(resp.data[0]);
        })
    }, []);



    return (
        <div style={{ width: "100%", height: "100%" }}>
            <div id="Mesplantes">
                <NavbarLateral />
            </div>
            <NavbarHaut title={"Les plantes de " + User.pseudo} />

            <div id="Main_Panel_plante">
                <div id="Panel_of_card">
                  {PlantesList.map((item) => {
                    return(<VisitPlante idPlante={item.idPlante} url={item.urlPhoto} nom={item.nom} description={item.description} idtype={item.idTypePlante} />)
                  })}
                </div>
                <div id="button-plante">
                        <button class="boutton-annuler-add-plante" onClick={() => {window.history.back()}}> Retour</button>
                </div>

            </div>
        </div>
    );
}

export default VisitAnnonce;
