import React, { useState, useEffect } from "react";
import axios from "axios";
import NavbarHaut from "./Component/NavbarHaut";
import NavbarLateral from "./Component/NavbarLateral";
import { useNavigate } from "react-router-dom";
import { useLocation } from "react-router";
import "./Assets/CSS/GestionDemandes.css";

function GestionDemandes() {

    const navigate=useNavigate() ; 
    const [ReservationList, setReservation] = useState([]);
    const [_idReservation, setIdReservation] = useState();

    useEffect(() => {
        axios({
            headers : {"authorization" : 'Bearer '+localStorage["Token"]},
            method: "GET",
            url: process.env.REACT_APP_server + "GetReservationAnnonce/"+localStorage["UserMail"],
        }).then((resp) => {
            setReservation(resp.data);
        });
    }, []);

    const UpdateReservationAccepter = (_idReservation) => {
        axios({
            headers : {"authorization" : 'Bearer '+localStorage["Token"]},
            method: "POST",
            url: process.env.REACT_APP_server + "UpdateReservationAccepter/" + _idReservation,
        }).then((resp) => {
            if(resp.data==='OK')
            {
                navigate('/MonAnnonce');
            }
        });
    };

    const UpdateReservationRefuser = (_idReservation) => {
        axios({
            headers : {"authorization" : 'Bearer '+localStorage["Token"]},
            method: "POST",
            url: process.env.REACT_APP_server + "UpdateReservationRefuser/" + _idReservation,
        }).then((resp) => {
            if(resp.data==='OK')
            {
                window.location.reload();
            }
        });
    };

    const getValidationStatus = (validation) => {
        switch (validation) {
            case 0:
                return "Refusée";
            case 1:
                return "Acceptée";
            default:
                return "En attente";
        }
    };

    return (
        <div style={{ width: "100%", height: "100%" }}>
            <div id="GestionDemandes">
                <NavbarLateral />
            </div>
            <NavbarHaut title="Gestion des demandes" />

            <div id="Main_Panel">
                {ReservationList.map((item) => (
                    <li className="card-mes-reservations" key={item.idReservation}>
                        <div className="card-column-left-gestion-demande">
                            <div className="title-card-column-left">
                                <img src={item.photodeprofil}></img>
                                <div class="card-column-left-gestion-demande-labels">
                                    <p className="card-column-left-gestion-demande-paragraphgrand">{item.nom} {item.prenom} </p>
                                    <p className="card-column-left-gestion-demande-paragraphpetit">Num. de la réservation : {item.numero}</p>
                                </div>

                            </div>
                        </div>
                        <div className="card-column-right-gestion-demande">
                            {item.validation === null && (
                                <>
                                    <button class="bouton-vert-gestion-reserve" onClick={() => UpdateReservationAccepter(item.idReservation)}>
                                        <i className="fa-solid fa-check"></i>
                                    </button>
                                    <button class="bouton-rouge-gestion-reserve" onClick={() => UpdateReservationRefuser(item.idReservation)}>
                                        <i className="fa-solid fa-x"></i>
                                    </button>
                                </>
                            )}
                            {item.validation === 0 && (
                                <>
                                    <p class="texte-gestion-reserve-refuse">Vous avez refusé cette réservation</p>
                                </>
                            )}
                            {item.validation === 1 && (
                                <>
                                    <p class="texte-gestion-reserve-accepte">Vous avez accepté cette réservation</p>
                                </>
                            )}
                        </div>
                    </li>
                ))}
            </div>
        </div>
    );
}

export default GestionDemandes;