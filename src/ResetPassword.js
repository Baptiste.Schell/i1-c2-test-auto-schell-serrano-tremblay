import React from "react";
import NavbarHaut from "./Component/NavbarHaut";
import NavbarLateral from "./Component/NavbarLateral"
import { useState,useEffect } from "react";
import axios from "axios";
import "./Assets/CSS/ResetPassword.css"
import { useNavigate } from "react-router-dom";
import { useLocation } from "react-router-dom";


function ResetPassword() {
    const location=useLocation() ; 
    const navigate=useNavigate() ; 
    const [Password,setPassword]=useState("")
    const [ConfirmPassword,setConfirmPassword]=useState("")
    const [idUser,setIduser]=useState(location.state._iduser) 

    function UpdatePassword()
    {
        if(Password===ConfirmPassword)
        {
            axios({
                headers : {"authorization" : 'Bearer '+localStorage["Token"]},
                method :"POST",
                url : process.env.REACT_APP_server+"ResetPassword/"+Password+"/"+idUser
            }).then(() => {
                navigate("/Profil")
            })
        }
        else
        {
            alert("Les deux mot de passes doivent être identiques !")
        }
        
    }


    return(

        <div style={{width : '100%' , height:'100%'}}>
            <div id="Navbar"> 
                <NavbarLateral/>
            </div>
            <NavbarHaut title="Modifier mon mot de passe"/>
            <div class="Main_Panel_Profil">
                <div className="card-entiere-profil">
            <div class="card-reset-password">
                <div class="section-title-reset-password">
                    <p class="texte-title-reset-password">Réinitialisation du mot de passe</p>
                </div>
                <div class="middle-section-reset-password">
                    <div class="input-label-reset-password">
                        <p class="label-reset-password">Nouveau mot de passe</p>
                        <input type="password" value={Password} onChange={(e) => {setPassword(e.target.value)}} class="input-reset-password"/>
                    </div>
                    <div class="input-label-reset-password">
                        <p class="label-reset-password">Confirmation du nouveau mot de passe</p>
                        <input type="password" value={ConfirmPassword} onChange={(e) => {setConfirmPassword(e.target.value)}} class="input-reset-password"/>
                    </div>
                </div>
                <div class="bottom-section-reset-password">
                    <button className="boutton-annuler-profil" onClick={() => {
                        navigate("/Profil")
                    }}>
                        <p className="bouton-texte-enregistrer-profil">Retour au profil</p>
                    </button>
                    <button class="boutton-enregistrer-profil" onClick={() => {UpdatePassword()}}>
                        <p class="bouton-texte-enregistrer-profil">Enregistrer</p>
                    </button>
                </div>
            </div>
        </div></div></div>
    )
}

export default ResetPassword