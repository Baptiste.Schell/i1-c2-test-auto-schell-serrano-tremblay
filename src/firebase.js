// Import the functions you need from the SDKs you need
import { initializeApp } from "firebase/app";
import { getAuth } from "firebase/auth";
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
const firebaseConfig = {
  apiKey: "AIzaSyD580n6077mlkKAFPsp37g0lm-5ouuVEF4",
  authDomain: "keepmyplant.firebaseapp.com",
  projectId: "keepmyplant",
  storageBucket: "keepmyplant.appspot.com",
  messagingSenderId: "40140418932",
  appId: "1:40140418932:web:f4cbf1a62f7cce55c70621"
};

const app = initializeApp(firebaseConfig);

// Initialize Firebase
export const auth = getAuth(app);
export default app;