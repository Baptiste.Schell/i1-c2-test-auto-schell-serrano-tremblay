import React, { useState, useEffect } from "react";
import axios from "axios";
import NavbarHaut from "./Component/NavbarHaut";
import NavbarLateral from "./Component/NavbarLateral";
import { useNavigate } from "react-router-dom";
import "./Assets/CSS/MesReservations.css";
import Temp_img from "./Assets/image/no-image.jpg";


function Mesreservations() {
    const [ReservationList, setReservation] = useState([]);
    const navigate=useNavigate() ;

    useEffect(() => {
        axios({
            headers : {"authorization" : 'Bearer '+localStorage["Token"]},
            method: "GET",
            url: process.env.REACT_APP_server + "GetReservation/" + localStorage["UserMail"],
        })
            .then((resp) => {
                setReservation(resp.data);
            })
    }, []);

    const DeleteReservation = (idReser) => {
        axios({
            headers : {"authorization" : 'Bearer '+localStorage["Token"]},
            method: "DELETE",
            url: process.env.REACT_APP_server + "DeleteReservationById/" + idReser,
        })
            .then((resp) => {
                // Mettre à jour la liste des réservations après la suppression
                setReservation(ReservationList.filter((item) => item.idReservation !== idReser));
            })
            .catch((error) => {
                console.log(error);
            });
    }
    const getValidationStatus = (validation) => {
        switch (validation) {
            case 0:
                return "Refusée";
            case 1:
                return "Acceptée";
            default:
                return "En attente";
        }
    }

    return (
        <div style={{ width: "100%", height: "100%" }}>
            <div id="MesReservations">
                <NavbarLateral />
            </div>
            <NavbarHaut title="Mes demandes de gardiennage" />

            <div id="Main_Panel">

                <ul>
                    {ReservationList.map((item) => (
                        <li className="card-mes-reservations" key={item.reference}>
                            <div className="card-column-left">
                                <div class="title-card-column-left">
                                    {item.validation == null &&(
                                        <p class="stand-by-state-reservation">{getValidationStatus(item.validation)}</p>
                                    )}
                                    {item.validation == 1 &&(
                                        <p className="accept-state-reservation">{getValidationStatus(item.validation)}</p>
                                    )}
                                    {item.validation == 0 &&(
                                        <p className="refuse-state-reservation">{getValidationStatus(item.validation)}</p>

                                    )}
                                    <p class="reference-reservation">- Ref : {item.reference}</p>
                                </div>
                                <p class="description-reservation-title">Description :</p>
                                <p class="description-reservation">{item.description}</p>
                            </div>
                            <div className="card-column-right">
                                <p class="date-reservation">Annonce du {item.dateDebut} au {item.dateFin}</p>
                                {item.validation == null &&(
                                    <>
                                        <button className="boutton-supprimer-reservation"
                                                onClick={() => DeleteReservation(item.idReservation)}>Supprimer</button>
                                    </>
                                )}
                                {item.validation == 1 &&(
                                    <p className="no-button-delete-reservation">Vous ne pouvez plus supprimer la réservation</p>
                                )}
                                {item.validation == 0 &&(
                                    <p className="no-button-delete-reservation">Vous ne pouvez plus supprimer la réservation</p>
                                )}
                            </div>
                        </li>
                    ))}
                </ul>
            </div>
        </div>
    );
}

export default Mesreservations;

