import NavbarHaut from "./Component/NavbarHaut";
import NavbarLateral from "./Component/NavbarLateral"
import { useState,useEffect } from "react";
import axios from "axios";
import { useNavigate } from "react-router-dom";
import "./Assets/CSS/Profil.css"
import React from "react";
import MyAnnonceProfil from "./Component/MyAnnonceProfil"



function MonProfil ()
{
    const navigate=useNavigate() ;
    const [EditMode,setEditMode]=useState(false)
    const [idUser,setidUser]=useState()
    const [Nom,setNom]=useState("") ;
    const [Prenom,setPrenom]=useState("")
    const [Telephone,setTelephone]=useState("")
    const [Pseudo,setPseudo]=useState("")
    const [Photodeprofil,setPhotodeprofil]=useState("")
    const [Mail,setMail]=useState("")
    const [IdAdresse,setIdAdresse]=useState()
    const [Voie,setVoie]=useState("")
    const [idRole,setIdRole]=useState("")
    const [Rue,setRue]=useState("")
    const [Ville,setVille]=useState("")
    const [CP,setCP]=useState("")
    const [Switcher,setSwitcher]=useState(false)
    const [PlanteGarder,setPlanteGarder]=useState([])
    const [AnnoncePoste,setAnnoncePoste]=useState([])
    const [NiveauExpertise,setNiveauExpertise]=useState("")

    useEffect(() => {
        axios({
            headers : {"authorization" : 'Bearer '+localStorage["Token"]},
            method : 'GET',
            url : process.env.REACT_APP_server + "GetUserByMail/" + localStorage["UserMail"]
        }).then((resp) => {
            setidUser(resp.data[0].idUser)
            setNom(resp.data[0].nom)
            setPrenom(resp.data[0].prenom)
            setPseudo(resp.data[0].pseudo)
            setMail(resp.data[0].mail)
            setTelephone(resp.data[0].telephone)
            setPhotodeprofil(resp.data[0].photodeprofil)
            setIdRole(resp.data[0].idRole)
        });

        axios({
            headers : {"authorization" : 'Bearer '+localStorage["Token"]},
            method : 'GET',
            url : process.env.REACT_APP_server + "GetAdresse/" + localStorage["UserMail"]
        }).then((resp) => {
            setVoie(resp.data[0].voie)
            setRue(resp.data[0].rue)
            setVille(resp.data[0].ville)
            setCP(resp.data[0].CP)
            setIdAdresse(resp.data[0].idAdresse)
        });

        axios({
            headers : {"authorization" : 'Bearer '+localStorage["Token"]},
            method : 'GET',
            url : process.env.REACT_APP_server + "LoadKeepAnnonces/" + localStorage["UserMail"]
        }).then((resp) => {
            setPlanteGarder(resp.data)
        });

        axios({
            headers : {"authorization" : 'Bearer '+localStorage["Token"]},
            method : 'GET',
            url : process.env.REACT_APP_server + "LoadPostAnnonces/" + localStorage["UserMail"]
        }).then((resp) => {
            setAnnoncePoste(resp.data)
        });

        

}, [])

    function GetNiveauExpertise(idNiveauExpertiseRequis){
        if(idNiveauExpertiseRequis===1)
        {
            setNiveauExpertise("Débutant")
        }
        else if(idNiveauExpertiseRequis===2)
        {
            setNiveauExpertise("Connaisseur")
        }
        else
        {
            setNiveauExpertise("Expert")
        }
    }


    function GoToEditMode()
    {
        // Pour rendre les input actif et pouvoir modifier ce qui à l'interieur
        setEditMode(true)
        var input=document.getElementsByClassName("EditableInput")
        for(var i = 0; i < input.length; i++) {
            input[i].disabled = false;
            input[i].style.border="2px solid grey"
        }
    }

    function ExitEditMode()
    {
        //Désactiver les inputs et interdire du coup la saisie
        setEditMode(false)
        var input=document.getElementsByClassName("EditableInput")
        for(var i = 0; i < input.length; i++) {
            input[i].disabled = true;
            input[i].style.border="none"
        }

        // On updates les nouvelles info  du profil
        UpdateInfoProfil()
    }

    function UpdateAdresse() {
        axios({
            headers : {"authorization" : 'Bearer '+localStorage["Token"]},
            method : 'POST',
            url : process.env.REACT_APP_server + "UpdateAdresse/" + Voie+"/"+Rue+"/"+Ville+"/"+CP+"/"+IdAdresse
        })
    }

    function UpdateInfoProfil()
    {
        axios({
            headers : {"authorization" : 'Bearer '+localStorage["Token"]},
            method :"POST" ,
            url :process.env.REACT_APP_server+"UpdateInfoProfil/"+Nom+"/"+Prenom+"/"+Telephone+"/"+Pseudo+"/"+idUser
        })
    }


    return(
        <div style={{ width: '100%', height: '100%' }}>
            <div id="Profil">
                <NavbarLateral />
            </div>
            <NavbarHaut title=" Mon profil" />

            <div id="Main_Panel_Profil">
                <div class="card-entiere-profil">
                    <div id="Profil-card">
                        <div class="information-perso-titre">
                            <p class="titre-gauche-info-perso">Informations personnelles</p>
                            <p><i className="titre-droite-icone fa-solid fa-pen-to-square" onClick={() => {navigate('/EditProfil',
                            {state: {
                                Pseudo : {Pseudo},
                                Nom : {Nom},
                                Prenom : {Prenom},
                                Telephone : {Telephone},
                                Mail : {Mail},
                                idUser : {idUser},
                                Voie : {Voie},
                                Rue : {Rue},
                                Ville : {Ville},
                                CP : {CP},
                                idAdresse : {IdAdresse},
                                idRole : {idRole},
                            }})}}></i></p>
                        </div>
                        <div class="information-perso-section">
                            <img src={Photodeprofil} class="information-perso-pdp"></img>
                            <div class="information-perso-rigth">
                                <div class="information-perso-cadre">
                                    <p class="information-perso-label-title"> Pseudo </p>
                                    <p className="information-perso-label"> {Pseudo} </p>
                                </div>
                                <div className="information-perso-nom-prenom">
                                    <div className="information-perso-cadre">
                                        <p className="information-perso-label-title"> Nom </p>
                                        <p className="information-perso-label"> {Nom} </p>
                                    </div>
                                    <div className="information-perso-cadre">
                                        <p className="information-perso-label-title"> Prénom </p>
                                        <p className="information-perso-label"> {Prenom} </p>
                                    </div>
                                </div>
                                <div className="information-perso-mail-tel">
                                    <div className="information-perso-cadre">
                                        <p className="information-perso-label-title"> Mail </p>
                                        <p className="information-perso-label"> {Mail} </p>
                                    </div>
                                    <div className="information-perso-cadre">
                                        <p className="information-perso-label-title"> Téléphone </p>
                                        <p className="information-perso-label"> {Telephone} </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="div-de-separation-profil"></div>
                        <div className="information-perso-titre">
                            <p className="titre-gauche-info-perso">Adresse</p>
                        </div>
                        <div class="adresse-profil-section">
                            <div class="adresse-long-cadre">
                                <p className="information-perso-label-title"> Rue </p>
                                <p className="information-perso-label"> {Rue} </p>
                            </div>
                            <div class="triple-short-cadre-adresse">
                                <div className="adresse-short-cadre">
                                    <p className="information-perso-label-title"> Numéro de voie </p>
                                    <p className="information-perso-label"> {Voie} </p>
                                </div>
                                <div className="adresse-short-cadre">
                                    <p className="information-perso-label-title"> Ville </p>
                                    <p className="information-perso-label"> {Ville} </p>
                                </div>
                                <div className="adresse-short-cadre">
                                    <p className="information-perso-label-title"> Code postal </p>
                                    <p className="information-perso-label"> {CP} </p>
                                </div>
                            </div>
                            <div className="adresse-long-cadre">
                                <p className="information-perso-label-title"> Complément d'adresse </p>
                                <p className="information-perso-label"> / </p>
                            </div>
                        </div>
                        <div className="div-de-separation-profil"></div>
                        <div className="information-perso-titre">
                            <p className="titre-gauche-info-perso">Réinitialisation du mot de passe</p>
                        </div>
                        <div class="section-modifier-mdp">
                            <button class="bouton-modifier-mdp" onClick={() => {navigate("/ResetPassword", {state: {_iduser : idUser}})}}>
                                <p class="bouton-texte-modifier-mdp">Modifier mon mot de passe</p>
                            </button>
                        </div>
                    </div>
                    <div class="section-menu-choix-profil">
                        <button onClick={() => {setSwitcher(false)}} class="bouton-choix-annonces-profil">
                            Mes annonces
                        </button>
                        <button onClick={() => {setSwitcher(true)}} class="bouton-choix-annonces-profil">
                            Plantes que j'ai gardés
                        </button>
                    </div>
                    {Switcher ? (
                        <div class="section-card-mes-annonces">
                            {PlanteGarder.map((item) => {
                                let niveauExpertise = "";
                                if (item.idNiveauExpertiseRequis === 1) {
                                    niveauExpertise = "Débutant";
                                } else if (item.idNiveauExpertiseRequis === 2) {
                                    niveauExpertise = "Intermédiaire";
                                } else if (item.idNiveauExpertiseRequis === 3) {
                                    niveauExpertise = "Expert";
                                }
                                return (
                                    <>
                                        <div className="card-mon-annonce">
                                            <div className="title-card-mon-annonce">
                                                <p className="title-date-card-mon-annonce">
                                                    Annonce gardée<br/>
                                                    <p className="subtitle-date-card-mon-annonce">du {item.dateDebut} au {item.dateFin} </p>

                                                </p>
                                                <p className="title-ref-card-mon-annonce">ref : {item.reference} </p>
                                            </div>
                                            <div className="div-de-separation-mon-annonce"></div>
                                            <div className="description-mon-annonce">
                                                <div className="description-label-mon-annonce">
                                                    Description
                                                </div>
                                                <div className="description-text-mon-annonce">
                                                    {item.description}
                                                </div>
                                            </div>
                                            <div className="bottom-section-mon-annonce">
                                                <div className="niveau-expertise-tag">
                                                    <p className="niveau-expertise-text">{niveauExpertise}</p>
                                                </div>
                                            </div>
                                        </div>
                                    </>
                                );
                            })}
                        </div>
                    ) : (
                        <div class="section-card-mes-annonces">
                        {AnnoncePoste.map((item) => {
                                return (
                                    <MyAnnonceProfil
                                        mailTest={item.mailTest}
                                        Date_debut={item.dateDebut}
                                        Date_fin={item.dateFin}
                                        Reference={item.reference}
                                        Description={item.description}
                                        pseudoTest={item.pseudoTest}
                                        Expertise={item.idNiveauExpertiseRequis}
                                    />
                                );
                            })}
                        </div>
                    )}
                </div>
            </div>
        </div>
    )
}


export default MonProfil;