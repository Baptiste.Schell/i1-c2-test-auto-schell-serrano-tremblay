import React from "react";
import NavbarHaut from "./Component/NavbarHaut";
import NavbarLateral from "./Component/NavbarLateral";
import axios from "axios";
import { useState, useEffect } from "react";
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import Plante from "./Component/PlanteAnnonce";
import "./Assets/CSS/CreateAnnonce.css";
import {useNavigate} from "react-router-dom";

function MesAnnonces() {

    const navigate = useNavigate();

    const [dateDebut, setdateDebut] = useState(new Date());
    const [dateFin, setdateFin] = useState(new Date());
    const [description, setdescription] = useState("");
    const [NiveauExpertiseRequis, setNiveauExpertiseRequis] = useState("");
    const [CycleCompteRendu, setCycleCompteRendu] = useState([]);
    const [NiveauxExpertiseRequis, setNiveauxExpertiseRequis] = useState([]);
    const [CyclesCompteRendu, setCyclesCompteRendu] = useState([]);
    const formatDateDebut = dateDebut.toISOString().substring(0, 10);
    const formatDateFin = dateFin.toISOString().substring(0, 10);
    const [PlantesList, setPlantes] = useState([]);

    useEffect(() => {
        axios({
            headers : {"authorization" : 'Bearer '+localStorage["Token"]},
            method: 'GET',
            url: process.env.REACT_APP_server + "NiveauExpertiseRequis"
        }).then((resp) => {
            setNiveauxExpertiseRequis(resp.data);
        });

        axios({
            headers : {"authorization" : 'Bearer '+localStorage["Token"]},
            method: 'GET',
            url: process.env.REACT_APP_server + "CycleCompteRendu"
        }).then((resp) => {
            setCyclesCompteRendu(resp.data);
        });

        axios({
            headers : {"authorization" : 'Bearer '+localStorage["Token"]},
            method: "GET",
            url: process.env.REACT_APP_server + "GetPlantByUser/" + localStorage["UserMail"],
        })
            .then((resp) => {
                setPlantes(resp.data);
            });

    }, []);
    function SubmitAnnonce() {
        axios({
            headers : {"authorization" : 'Bearer '+localStorage["Token"]},
            method: 'POST',
            url: process.env.REACT_APP_server + "CreateAnnonceByUser/"+localStorage["UserMail"]+"/"+formatDateDebut+"/"+formatDateFin+"/"+description+"/"+NiveauExpertiseRequis+"/"+CycleCompteRendu

        })
        .then((resp) =>{
            navigate("/Dashboard")
        });
    }


    return (
        <div style={{ width: '100%', height: '100%' }}>
            <div id="MesAnnonces">
                <NavbarLateral />
            </div>
            <NavbarHaut title="Poster une annonce" />
            <div id="Main_Panel">
                    <div class="card-principale-poster-annonce">
                        <div class="card-poster-mon-annonce">
                            <div class="title-potser-annonce">
                                <p class="titre-p-poster-annonce">Poster une annonce</p>
                            </div>
                            <div class="date-section-poster-annonce">
                                <div class="label-input-poster-annonce">
                                    <p class="texte-input-poster-annonce">Date de début :</p>
                                    <DatePicker class="input-poster-annonce" selected={dateDebut} onChange={(date) => setdateDebut(date)} name="dateDebut"
                                                dateFormat="dd/MM/yyyy"
                                    />
                                </div>
                                <div class="label-input-poster-annonce">
                                    <p class="texte-input-poster-annonce">Date de fin : </p>
                                    <DatePicker class="input-poster-annonce" selected={dateFin} onChange={(date) => setdateFin(date)} name="dateFin"
                                                dateFormat="dd/MM/yyyy"
                                    />
                                </div>
                            </div>
                            <div class="date-section-poster-annonce">
                                <div class="label-input-poster-annonce">
                                    <p className="texte-input-poster-annonce">Cycle de jours </p>
                                    <select class="input-poster-annonce" value={CycleCompteRendu} onChange={(e) => setCycleCompteRendu(e.target.value)}>
                                        <option value="">--Choisir un cylce (j)--</option>
                                        {CyclesCompteRendu.map((cycle) => (
                                            <option key={cycle.idCycleCompteRendu} value={cycle.idCycleCompteRendu}>{cycle.nombre}</option>
                                        ))}
                                    </select>
                                </div>
                                <div class="label-input-poster-annonce">
                                    <p className="texte-input-poster-annonce">Niveau d'expertise : </p>
                                    <select class="input-poster-annonce" value={NiveauExpertiseRequis} onChange={(e) => setNiveauExpertiseRequis(e.target.value)}>
                                        <option value="">--Choisir un niveau--</option>
                                        {NiveauxExpertiseRequis.map((niveau) => (
                                            <option key={niveau.idNiveauExpertiseRequis} value={niveau.idNiveauExpertiseRequis}>{niveau.libelle}</option>
                                        ))}
                                    </select>
                                </div>
                            </div>
                            <div class="nimporte-quoi">
                                <div className="section-description-poster-annonce">
                                    <p className="label-description-poster-annonce">Description de l'annonce : </p>
                                    <input type="text" className="input-description-poster-annonce" value={description}
                                           onChange={(e) => setdescription(e.target.value)}
                                           name="description"></input>
                                </div>
                            </div>
                            <div class="section-plante-poster-annonce">
                                {PlantesList.map((item) => {
                                    return(
                                        <>
                                            <div class="card-plante-poster-annonce">
                                                <img src={item.urlPhoto} class="photo-plante-poster-annonce"></img>
                                                <div class="trois-labels-photo-poster-annonce">
                                                    <p class="nom-plante-poster-annonce">{item.nom}</p>
                                                    <p class="description-plante-poster-annonce">{item.description}</p>
                                                    <button onClick={() => {navigate('/AllConseilsForTypePlante',{state:{idTypePlante : item.idTypePlante}})}} className="boutton-type-plante">
                                                        {item.libelle}
                                                    </button>
                                                </div>

                                            </div>
                                        </>
                                    )
                                })}
                            </div>
                            <div className="section-bas-poster-annonce">
                                <button className="boutton-annuler-profil" onClick={() => {
                                    navigate("/Dashboard")
                                }}>
                                    <p className="bouton-texte-enregistrer-profil">Retour à l'accueil</p>
                                </button>
                                <button className="boutton-enregistrer-profil" onClick={() => { SubmitAnnonce()}}>
                                    <p className="bouton-texte-enregistrer-profil">Poster l'annonce !</p>
                                </button>
                            </div>
                        </div>
                    </div>
            </div>
        </div>
    )
}
export default MesAnnonces;