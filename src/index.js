import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import Dashboard from './Dashboard';
import Mesplantes from "./Mesplantes";
import MonProfil from "./MonProfil";
import EditProfil from './EditProfil';
import FindPlant from "./FindPlant";
import Authentification from './Authentification';
import ConfirmMail from './ConfirmMail';
import MonAnnonce from './MonAnnonce';
import AnnonceSuivis from './AnnonceSuivis';
import MesReservation from "./MesReservation"
import VisitProfil from "./VisitProfil"
import ModifyPlante from "./ModifyPlante"
import AddPlante from "./AddPlante"
import ResetPassword from "./ResetPassword"
import VisitProfilBotaniste from "./VisitProfilBotaniste"
import ProfilBotaniste from "./ProfilBotaniste"
import AddAnnonce from "./CreateAnnonce"
import AnnonceSuivieBotaniste from './AnnonceSuivieBotaniste';
import GestionDemandes from "./GestionDemandes"
import AllConseilsForTypePlante from "./AllConseilsForTypePlante";
import PosterConseil from "./PosterConseil";
import MesConseils from "./MesConseils";
import ModifierConseil from "./ModifierConseil";
import VisitAnnonce from "./VisitAnnonce";
import Messagerie from './Messagerie';


import { BrowserRouter as Router, Routes, Route } from 'react-router-dom';

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(

  <React.StrictMode>

    <Router>
      <Routes>
        <Route path="/Auth" index element={<Authentification />} />
        <Route path="/Dashboard" element={<Dashboard />} />
        <Route path="/" element={<Authentification />} />
        <Route path="/Profil" element={<MonProfil />} />
        <Route path="/Mesplantes" element={<Mesplantes />} />
        <Route path="/FindPlant" element={<FindPlant />} />
        <Route path="/EditProfil" element={<EditProfil />} />
        <Route path="/Auth/ConfirmMail" element={<ConfirmMail />} />
        <Route path="/MonAnnonce" element={<MonAnnonce />} />
        <Route path="/VisitProfil" element={<VisitProfil />} />
        <Route path="/MesReservation" element={<MesReservation />} />
        <Route path="/ModifyPlante" element={<ModifyPlante />} />
        <Route path="/AddPlante" element={<AddPlante />} />
        <Route path="/ResetPassword" element={<ResetPassword />} />
        <Route path="/VisitProfilBotaniste" element={<VisitProfilBotaniste/>}/>
        <Route path="/ProfilBotaniste" element={<ProfilBotaniste/>}/>
        <Route path="/AddAnnonce" element={<AddAnnonce/>}/>
        <Route path="/AnnonceSuivis" element={<AnnonceSuivis/>}/>
        <Route path="/AnnonceSuivieBotaniste" element={<AnnonceSuivieBotaniste/>}/>
        <Route path="/GestionDemandes" element={<GestionDemandes/>}/>
        <Route path="/AllConseilsForTypePlante" element={<AllConseilsForTypePlante/>}/>
        <Route path="/PosterConseil" element={<PosterConseil/>}/>
        <Route path="/MesConseils" element={<MesConseils/>}/>
        <Route path="/ModifierConseil" element={<ModifierConseil/>}/>
        <Route path="/VisitAnnonce" element={<VisitAnnonce/>}/>
        <Route path="/Messagerie" element={<Messagerie/>}/>
      </Routes>
    </Router>
  </React.StrictMode>,
);
