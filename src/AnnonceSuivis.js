import React from "react";
import NavbarHaut from "./Component/NavbarHaut";
import NavbarLateral from "./Component/NavbarLateral"
import "./Assets/CSS/Annonces.css"
import {useState, useEffect} from "react";
import axios from "axios";
import "./Assets/CSS/AnnonceSuivis.css"
import Temp_img from "./Assets/image/no-image.jpg"
import {useNavigate} from "react-router";



function AnnonceSuivis() {

    const navigate=useNavigate() ;
    const [AnnonceReserve, setAnnonceReserve] = useState("")
    const [Photos, setPhotos] = useState([])
    const [Proprio, setProprio] = useState("")
    const [_url, setUrl] = useState("")
    const currentDate = new Date(); // pour controle lors de l'ajout de la photo
    const [isOpen, setIsOpen] = useState(false);
    const [Image,setImage]=useState([])


    const handleToggle = () => {
        setIsOpen(!isOpen);
    };

    // comportement
    useEffect(() => {
        axios({
                headers : {"authorization" : 'Bearer '+localStorage["Token"]},
            method: 'GET',
            url: process.env.REACT_APP_server + "GetAnnoncesReserved/" + localStorage['UserMail']
        }).then((resp) => {
            setAnnonceReserve(resp.data[0])
        });
    }, [])

    useEffect(() => {
        if (AnnonceReserve.idAnnonce) {
            axios({
                headers : {"authorization" : 'Bearer '+localStorage["Token"]},
                method: 'GET',
                url: process.env.REACT_APP_server + "GetPhotosByAnnoncesWithComments/" + AnnonceReserve.idAnnonce
            }).then((resp) => {
                setPhotos(resp.data);
            });
        }
    }, [AnnonceReserve.idAnnonce])

    useEffect(() => {
        if (AnnonceReserve.idAnnonce) {
            axios({
                headers : {"authorization" : 'Bearer '+localStorage["Token"]},
                method: 'GET',
                url: process.env.REACT_APP_server + "GetProprioByAnnonce/" + AnnonceReserve.idAnnonce
            }).then((resp) => {
                setProprio(resp.data[0]);
            });
        }
    }, [AnnonceReserve.idAnnonce]);


    function UpdatePicture(idPhoto) {

        const formData = new FormData()
        formData.append("image", Image[idPhoto])
        
        axios.post(process.env.REACT_APP_server+"UpdatePhotoForFollow/"+idPhoto+"/"+Image[idPhoto].type.split("/")[1]+"/"+Proprio.mail, formData, { headers: {'Content-Type': 'multipart/form-data',"authorization" : 'Bearer '+localStorage["Token"]}})
        .then((resp) => {
            if (resp.data === 'OK') {
                alert("OK")
            }
        })
        
      
    }

    function HandleImage(e,id) 
    {
        const selectedImage = e.target.files[0];

        // Mettre à jour l'état de l'image en utilisant l'id et l'image sélectionnée
        setImage((prevImage) => ({
          ...prevImage,
          [id]: selectedImage
        }));
      
        // Modifier l'attribut "id" de l'élément "img" pour correspondre à l'id de l'élément
        const imgElement = e.target.parentNode.querySelector("img");
        imgElement.setAttribute("id", id);
    
    }

    return (
        <div style={{width: '100%', height: '100%'}}>
            <div id="AnnonceSuivis">
                <NavbarLateral/>
            </div>
            <NavbarHaut title="Journal de gardiennage des plantes"/>
            <div id="Main_Panel">
                <div class="page-suivis-annonce">
                    <div class="card-principale-suivis-annonce">
                        <div class="title-suivis-annonce">
                            <div class="title-left-suivis-annonce">
                                <p class="title-left-gras-suivis-annonce">Annonce
                                    du {AnnonceReserve.dateDebut} au {AnnonceReserve.dateFin}</p>
                                <p class="title-left-ligth-suivis-annonce">Réf : {AnnonceReserve.numero}</p>
                            </div>
                            <div class="title-right-suivis-annonce">
                                <div class="title-right-left-suivis-annonce">
                                    <p class="title-right-left-">Propriétaire</p>
                                    <p class="title-right-left-nomprenom-suivis-annonce">{Proprio.nom} {Proprio.prenom}</p>
                                </div>
                                <img src={Proprio.photodeprofil} class="title-right-center-suivis-annonce"
                                     onClick={() => {navigate("/VisitProfil", {state: {mail: Proprio.mail}})}}></img>
                            </div>
                        </div>
                        <div class="cycle-nbetape-section-suivis-annonce">
                            <div className="cycle-section-suivis-annonce">
                                <p className="label-cycle-nbetape-suivis-annonce">Cycle :</p>
                                <p className="light-label-cycle-nbetape-suivis-annonce">Tous les {AnnonceReserve.idCycleCompteRendu} jours</p>
                            </div>
                            <div className="cycle-section-suivis-annonce">
                                <p className="label-cycle-nbetape-suivis-annonce">Nombre d'étapes :</p>
                                <p className="light-label-cycle-nbetape-suivis-annonce">{AnnonceReserve.nbEtape}</p>
                            </div>
                            <div className="cycle-section-suivis-annonce">
                                <p className="label-cycle-nbetape-suivis-annonce">Expertise :</p>
                                <p className="light-label-cycle-nbetape-suivis-annonce">{
                                    AnnonceReserve.idNiveauExpertiseRequis === 1 ? "Débutant" :
                                        AnnonceReserve.idNiveauExpertiseRequis === 2 ? "Intermédiaire" :
                                            AnnonceReserve.idNiveauExpertiseRequis === 3 ? "Expert" : ""}</p>
                            </div>
                        </div>
                        <div class="section-description-suivis-annonce">
                            <p class="label-cycle-nbetape-suivis-annonce">Description :</p>
                            <p class="light-label-cycle-nbetape-suivis-annonce">{AnnonceReserve.description}</p>
                        </div>
                        {Photos.map((item) => (
                            <div key={item.id}>
                                <div className="divider-perso"></div>
                                <div className="section-photo-suivis-conseil">
                                    {item.urlPhoto ? (
                                        <img src={item.urlPhoto} className="photo-suivis-annonce"/>
                                    ) : (
                                        Image[item.idPhoto] ? (
                                            <img src={URL.createObjectURL(Image[item.idPhoto])} id={item.idPhoto} className="photo-suivis-annonce"/>
                                        ) : (
                                            <img src={Temp_img} className="photo-suivis-annonce"/>
                                        )
                                    )}
                                    <div className="right-photo-suivis-annonce">
                                        <p className="date-de-poste-label">Date de poste</p>
                                        <p className="date-de-poste">{item.datePoste}</p>
                                        {item.description || item.urlPhoto ? (
                                            <>
                                                <p className="date-de-poste-label">Commentaire :</p>
                                                <p className="date-de-poste">{item.description || "Aucun commentaire sur cette photo pour le moment"}</p>
                                            </>
                                        ) : (
                                            <div className="div-de-merde"> {/* utiliser className au lieu de class */}
                                                {new Date(item.datePoste).toDateString() === currentDate.toDateString() ? (
                                                    <div className="div-de-merde"> {/* utiliser className au lieu de class */}
                                                        <p className="date-de-poste-label">Ajouter une photo :</p>
                                                        <div> {/* fermer la balise div */}
                                                            <input placeholder="Url de la photo" type="file" onChange={(e) => {HandleImage(e, item.idPhoto)}}/>
                                                            <button class="boutton-enregistrer-photos" onClick={() => {UpdatePicture(item.idPhoto)}}>Valider</button>
                                                        </div>
                                                    </div>
                                                ) : (
                                                    <div className="div-de-merde"> {/* utiliser className au lieu de class */}
                                                        {new Date(item.datePoste) > currentDate ? (
                                                            <p>Revenez le {item.datePoste} pour ajouter une photo.</p>
                                                        ) : (
                                                            <p>Date limite pour poster la photo dépassée !</p>
                                                        )}
                                                    </div>
                                                )}
                                            </div>
                                        )}
                                    </div>
                                </div>
                            </div>
                        ))}

                        <div class="boutton-de-merde">
                            <p class="contacter-proprio">Contacter le propriétaire</p>
                            <p class="numero-telephone-suivis-annonce">{Proprio.telephone}</p>
                        </div>
                    </div>
        </div>
    </div>
</div>
)
}

export default AnnonceSuivis;
