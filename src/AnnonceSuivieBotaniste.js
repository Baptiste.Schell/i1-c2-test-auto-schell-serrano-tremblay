import React from "react";
import NavbarHaut from "./Component/NavbarHaut";
import NavbarLateral from "./Component/NavbarLateral"
import { useState,useEffect} from "react";
import axios from "axios";
import { useNavigate } from "react-router";
import Temp_img from "./Assets/image/no-image.jpg";


function AnnonceSuivieBotaniste () {

    const navigate=useNavigate() ; 
    const [AnnonceFollow, setAnnonceFollow] = useState("")
    const [Photos,setPhotos] = useState([])
    const [Proprio,setProprio]= useState("")
    const [_description,setDescription]=useState("")
    
    // comportement
    useEffect(() => {
    axios({
        headers : {"authorization" : 'Bearer '+localStorage["Token"]},
        method : 'GET',
        url : process.env.REACT_APP_server + "GetFollowAnnonce/" + localStorage['UserMail']
      }).then((resp) => {
        setAnnonceFollow(resp.data[0])
      });
    },[])

    useEffect(() => {
        if (AnnonceFollow.idAnnonce) {
            axios({
                headers : {"authorization" : 'Bearer '+localStorage["Token"]},
                method: 'GET',
                url: process.env.REACT_APP_server + "GetPhotosByAnnoncesWithComments/" + AnnonceFollow.idAnnonce
            }).then((resp) => {
                setPhotos(resp.data);
            });
        }
    }, [AnnonceFollow.idAnnonce])

    useEffect(() => {
        if (AnnonceFollow.idAnnonce) {
            axios({
                headers : {"authorization" : 'Bearer '+localStorage["Token"]},
                method: 'GET',
                url: process.env.REACT_APP_server + "GetProprioByAnnonce/" + AnnonceFollow.idAnnonce
            }).then((resp) => {
                setProprio(resp.data[0]);
            });
        }
    }, [AnnonceFollow.idAnnonce]);

    function AddComments(idPhoto){
        axios({
            headers : {"authorization" : 'Bearer '+localStorage["Token"]},
            method : "POST",
            url : process.env.REACT_APP_server+"AddCommentsForFollow/"+localStorage["UserMail"]+"/"+AnnonceFollow.idAnnonce+"/"+idPhoto,
            data: {
                description:_description,
            }
        }).then((resp) => {
            if(resp.data==='OK')
            {
                window.location.reload();
            }
        });
    }
  

    return (
    <div style={{ width: '100%', height: '100%' }}>
      <div id="AnnonceSuivis">
        <NavbarLateral />
      </div>
      <NavbarHaut title="Annonce Suivie" />

        <div id="Main_Panel">
            <div className="page-suivis-annonce">
                <div className="card-principale-suivis-annonce">
                    <div className="title-suivis-annonce">
                        <div className="title-left-suivis-annonce">
                            <p className="title-left-gras-suivis-annonce">Annonce
                                du {AnnonceFollow.dateDebut} au {AnnonceFollow.dateFin}</p>
                            <p className="title-left-ligth-suivis-annonce">Réf : {AnnonceFollow.numero}</p>
                        </div>
                        <div className="title-right-suivis-annonce">
                            <div className="title-right-left-suivis-annonce">
                                <p className="title-right-left-">Propriétaire</p>
                                <p className="title-right-left-nomprenom-suivis-annonce">{Proprio.nom} {Proprio.prenom}</p>
                            </div>
                            <img src={Proprio.photodeprofil} className="title-right-center-suivis-annonce"
                                 onClick={() => {navigate("/VisitProfil", {state: {mail: Proprio.mail}})}}></img>
                        </div>
                    </div>
                    <div className="cycle-nbetape-section-suivis-annonce">
                        <div className="cycle-section-suivis-annonce">
                            <p className="label-cycle-nbetape-suivis-annonce">Cycle :</p>
                            <p className="light-label-cycle-nbetape-suivis-annonce">Tous
                                les {AnnonceFollow.idCycleCompteRendu} jours</p>
                        </div>
                        <div className="cycle-section-suivis-annonce">
                            <p className="label-cycle-nbetape-suivis-annonce">Nombre d'étapes :</p>
                            <p className="light-label-cycle-nbetape-suivis-annonce">{AnnonceFollow.nbEtape}</p>
                        </div>
                        <div className="cycle-section-suivis-annonce">
                            <p className="label-cycle-nbetape-suivis-annonce">Expertise :</p>
                            <p className="light-label-cycle-nbetape-suivis-annonce">{
                                AnnonceFollow.idNiveauExpertiseRequis === 1 ? "Débutant" :
                                    AnnonceFollow.idNiveauExpertiseRequis === 2 ? "Intermédiaire" :
                                        AnnonceFollow.idNiveauExpertiseRequis === 3 ? "Expert" : ""}</p>
                        </div>
                    </div>
                    <div className="section-description-suivis-annonce">
                        <p className="label-cycle-nbetape-suivis-annonce">Description :</p>
                        <p className="light-label-cycle-nbetape-suivis-annonce">{AnnonceFollow.description}</p>
                    </div>
                    {Photos.map((item) => (
                        <>
                            <div className="divider"></div>
                            <div className="section-photo-suivis-conseil">
                                {item.urlPhoto ? (
                                    <img src={item.urlPhoto} className="photo-suivis-annonce"/>
                                ) : (
                                    <img src={Temp_img} className="photo-suivis-annonce"/>
                                )}
                                <div className="right-photo-suivis-annonce">
                                    <p className="date-de-poste-label">Date de poste</p>
                                    <p className="date-de-poste">{item.datePoste}</p>

                                    {item.description && item.urlPhoto ? (
                                        <>
                                            <p className="date-de-poste-label">Commentaire :</p>
                                            <p className="date-de-poste">{item.description}</p>
                                        </>
                                    ) : (
                                        <div>
                                            {item.urlPhoto ? (
                                                <div>
                                                    <p class="date-de-poste-label">Ajouter un commentaire :</p>
                                                    <input class="input-profil-normal" value={_description} onChange={(e) => {setDescription(e.target.value)}}/>
                                                    <button class="boutton-enregistrer-photos" onClick={() => {AddComments(item.idPhoto)}}>Ajouter</button>
                                                </div>
                                            ) : (
                                                <p className="date-de-poste"> Il faut une photo pour ajouter un commentaire</p>
                                            )}
                                        </div>
                                    )}
                                </div>
                            </div>
                        </>
                    ))}
                </div>
            </div>
        </div>
    </div>
)}
export default AnnonceSuivieBotaniste ; 
