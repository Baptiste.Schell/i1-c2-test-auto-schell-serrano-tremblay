import React from "react";
import NavbarHaut from "./Component/NavbarHaut";
import NavbarLateral from "./Component/NavbarLateral"
import "./Assets/CSS/EditProfil.css"
import {useState, useEffect} from "react";
import axios from "axios";
import {useNavigate, useLocation} from "react-router";
import {Link} from "react-router-dom";


function EditProfil() {

    // state (état, données)
    const [User, setUser] = useState("")
    const [Adresse, setAdresse] = useState("")
    const location = useLocation();
    const navigate = useNavigate()

    const [Pseudo, setPseudo] = useState("");
    const [Nom, setNom] = useState("");
    const [Prenom, setPrenom] = useState("")
    const [Telephone, setTelephone] = useState("")
    const [Mail, setMail] = useState("")
    const [idUser, setIdUser] = useState("")
    const [Voie, setVoie] = useState("")
    const [Rue, setRue] = useState("")
    const [Ville, setVille] = useState("")
    const [CP, setCP] = useState("")
    const [idAdresse, setIdAdresse] = useState("")
    const [Photodeprofil, setPhotodeprofil] = useState("")
    const [description, setDescription] = useState(location.state.description)
    const [_url, setUrl] = useState(location.state.url)
    const [TypePlante, setTypePlante] = useState(location.state._libelle)
    const [idPlante, setIdPlante] = useState(location.state.idPlante)
    const [idRole, setIdRole] = useState(location.state.idRole)

    useEffect(() => {
        axios({
            headers : {"authorization" : 'Bearer '+localStorage["Token"]},
            method: 'GET',
            url: process.env.REACT_APP_server + "GetUserByMail/" + localStorage["UserMail"]
        }).then((resp) => {
            setIdUser(resp.data[0].idUser)
            setNom(resp.data[0].nom)
            setPrenom(resp.data[0].prenom)
            setPseudo(resp.data[0].pseudo)
            setMail(resp.data[0].mail)
            setTelephone(resp.data[0].telephone)
            setPhotodeprofil(resp.data[0].photodeprofil)
            setIdRole(resp.data[0].idRole)
        });

        axios({
            headers : {"authorization" : 'Bearer '+localStorage["Token"]},
            method: 'GET',
            url: process.env.REACT_APP_server + "GetAdresse/" + localStorage["UserMail"]
        }).then((resp) => {
            setVoie(resp.data[0].voie)
            setRue(resp.data[0].rue)
            setVille(resp.data[0].ville)
            setCP(resp.data[0].CP)
            setIdAdresse(resp.data[0].idAdresse)
        });
    }, [])

    function UpdateProfil() {
        axios({
            headers : {"authorization" : 'Bearer '+localStorage["Token"]},
            method: 'POST',
            url: process.env.REACT_APP_server + "UpdateInfoProfil/" + Nom + "/" + Prenom + "/" + Telephone + "/" + Pseudo + "/" + idUser
        }).then((resp) => {
            setUser(resp.data[0])
        }).then((resp) => {
            axios({
                headers : {"authorization" : 'Bearer '+localStorage["Token"]},
                method: 'POST',
                url: process.env.REACT_APP_server + "UpdateAdresse/" + Voie + "/" + Rue + "/" + Ville + "/" + CP + "/" + idAdresse
            }).then((resp) => {
                if (resp.data === 'OK') {
                    navigate('/Profil')
                }
            })
        })
    }

    function UpdateProfilBotanist() {
        axios({
            headers : {"authorization" : 'Bearer '+localStorage["Token"]},
            method: 'POST',
            url: process.env.REACT_APP_server + "UpdateInfoProfil/" + Nom + "/" + Prenom + "/" + Telephone + "/" + Pseudo + "/" + idUser
        }).then((resp) => {
            if (resp.data === 'OK') {
                navigate('/ProfilBotaniste')
            }
        })
    }

// affichage (render)

    return (
        <div style={{width: '100%', height: '100%'}}>
            <div id="EditProfil">
                <NavbarLateral/>
            </div>
            <NavbarHaut/>


            <div id="Main_Panel_Profil">
                <div className="card-entiere-profil">
                    <div id="Profil-card">
                        <div className="information-perso-titre">
                            <p className="titre-gauche-info-perso">Informations personnelles</p>
                        </div>
                        <div className="information-perso-section">
                            <img src={Photodeprofil} className="information-perso-pdp"></img>
                            <div className="information-perso-rigth">
                                <div className="information-perso-cadre">
                                    <p className="information-perso-label-title"> Pseudo </p>
                                    <input value={Pseudo} className="input-profil-normal" onChange={(e) => {
                                        setPseudo(e.target.value)
                                    }}></input>
                                </div>
                                <div className="information-perso-nom-prenom">
                                    <div className="information-perso-cadre">
                                        <p className="information-perso-label-title"> Nom </p>
                                        <input value={Nom} className="input-profil-normal" onChange={(e) => {
                                            setNom(e.target.value)
                                        }}></input>
                                    </div>
                                    <div className="information-perso-cadre">
                                        <p className="information-perso-label-title"> Prénom </p>
                                        <input value={Prenom} className="input-profil-normal" onChange={(e) => {
                                            setPrenom(e.target.value)
                                        }}></input>
                                    </div>
                                </div>
                                <div className="information-perso-mail-tel">
                                    <div className="information-perso-cadre">
                                        <p className="information-perso-label-title"> Mail </p>
                                        <p className="information-perso-label"> {Mail} </p>
                                    </div>
                                    <div className="information-perso-cadre">
                                        <p className="information-perso-label-title"> Téléphone </p>
                                        <input value={Telephone} className="input-profil-normal" onChange={(e) => {
                                            setTelephone(e.target.value)
                                        }}></input>
                                    </div>
                                </div>
                            </div>
                        </div>
                        {idRole === 1 && (<>
                            <div className="div-de-separation-profil"></div>
                            <div className="information-perso-titre">
                                <p className="titre-gauche-info-perso">Adresse</p>
                            </div>
                            <div className="adresse-profil-section">
                                <div className="adresse-long-cadre">
                                    <p className="information-perso-label-title"> Rue </p>
                                    <input value={Rue} className="input-profil-large" onChange={(e) => {
                                        setRue(e.target.value)
                                    }}></input>
                                </div>
                                <div className="triple-short-cadre-adresse">
                                    <div className="adresse-short-cadre">
                                        <p className="information-perso-label-title"> Numéro de voie </p>
                                        <input value={Voie} className="input-profil-normal" onChange={(e) => {
                                            setVoie(e.target.value)
                                        }}></input>
                                    </div>
                                    <div className="adresse-short-cadre">
                                        <p className="information-perso-label-title"> Ville </p>
                                        <input value={Ville} className="input-profil-normal" onChange={(e) => {
                                            setVille(e.target.value)
                                        }}></input>
                                    </div>
                                    <div className="adresse-short-cadre">
                                        <p className="information-perso-label-title"> Code postal </p>
                                        <input value={CP} className="input-profil-normal" onChange={(e) => {
                                            setCP(e.target.value)
                                        }}></input>
                                    </div>
                                </div>
                                <div className="adresse-long-cadre">
                                    <p className="information-perso-label-title"> Complément d'adresse </p>
                                    <input className="input-profil-large"></input>
                                </div>
                            </div>
                        </>)}

                        <div className="boutton-bottom-modify-profil">
                            {idRole === 1 && (<>
                                    <button className="boutton-annuler-profil" onClick={() => {
                                        navigate("/Profil")
                                    }}>
                                        <p className="bouton-texte-enregistrer-profil">Annuler</p>
                                    </button>
                                    <button className="boutton-enregistrer-profil" onClick={() => {
                                        UpdateProfil()
                                    }}>
                                        <p className="bouton-texte-enregistrer-profil">Enregistrer</p>
                                    </button>
                                </>
                            )}
                            {idRole === 2 && (
                                <>
                                    <button className="boutton-annuler-profil" onClick={() => {
                                        navigate("/ProfilBotaniste")
                                    }}>
                                        <p className="bouton-texte-enregistrer-profil">Annuler</p>
                                    </button>
                                    <button className="boutton-enregistrer-profil" onClick={() => {
                                        UpdateProfilBotanist()
                                    }}>
                                        <p className="bouton-texte-enregistrer-profil">Enregistrer</p>
                                    </button>
                                </>
                            )}

                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default EditProfil;