import React from "react";
import NavbarHaut from "./Component/NavbarHaut";
import NavbarLateral from "./Component/NavbarLateral";
import axios from "axios";
import { useState, useEffect } from "react";
import { useParams } from "react-router-dom";

function Mesplantes() {

    const { idPlante } = useParams();
    const [Nom, setNom] = useState("");
    const [Description, setDescription] = useState("");
    const [urlPhoto, setUrlPhoto] = useState("");
    const [TypePlante, setTypePlante] = useState("");
    const [TypesPlante, setTypesPlante] = useState([]);
    const [imageUrl, setImageUrl] = useState("");

    useEffect(() => {
        axios({
            headers : {"authorization" : 'Bearer '+localStorage["Token"]},
            method: 'GET',
            url: process.env.REACT_APP_server + "TypePlantes"
        }).then((resp) => {
            setTypesPlante(resp.data);
        });
    }, []);

    useEffect(() => {
        axios({
            headers : {"authorization" : 'Bearer '+localStorage["Token"]},
            method :"GET",
            url :process.env.REACT_APP_server + "GetPlanteById/" + idPlante
        })
            .then((resp) => {
                setNom(resp.data[0].nom);
                setDescription(resp.data[0].description);
                setUrlPhoto(resp.data[0].urlPhoto);
                setTypePlante(resp.data[0].idTypePlante);
            })
            .catch((err) => {
                console.log(err);
            })
    }, [idPlante]);

    function SubmitPlante() {
        const encodedUrl = encodeURIComponent(urlPhoto);
        axios({
            headers : {"authorization" : 'Bearer '+localStorage["Token"]},
            method: 'POST',
            url: process.env.REACT_APP_server + "CreatePlanteByUser/"+localStorage["UserMail"]+"/"+Nom+"/"+Description+"/"+encodedUrl+"/"+TypePlante
        }).then((resp) => {
          //  setPlantes(resp.data)
        })
    }

    const updatePlante = (e) => {
        e.preventDefault();
        const encodedUrl = encodeURIComponent(urlPhoto);
        axios({
            headers : {"authorization" : 'Bearer '+localStorage["Token"]},
            method: 'PUT',
            url: process.env.REACT_APP_server + "UpdatePlante/" + idPlante,
            data: {
                nom: Nom,
                urlPhoto: encodedUrl,
                description: Description,
                idTypePlante: TypePlante,
            }
        }).then((resp) => {
            setNom("");
            setDescription("");
            setUrlPhoto("");
            setTypePlante("");
            setImageUrl("");
        })
    }

    const handleImagePreview = () => {
        setImageUrl(urlPhoto);
    }

    return (
        <div style={{ width: '100%', height: '100%' }}>
            <div id="Mesplantes">
                <NavbarLateral />
            </div>
            <NavbarHaut title="Mes plantes" />
            <div id="Main_Panel">
                <div>
                    <form>
                        <label htmlFor="Nom">Nom</label><br/>
                        <input type="text" id="Nom" value={Nom} onChange={(e) => setNom(e.target.value)} name="Nom"></input><br/>

                        <label htmlFor="Description">Description</label><br/>
                        <input type="text" id="Description" value={Description} onChange={(e) => setDescription(e.target.value)} name="Description"></input><br/>

                        <label htmlFor="UrlPhoto">UrlPhoto</label><br/>
                        <input type="text" id="UrlPhoto" value={urlPhoto} onChange={(e) => setUrlPhoto(e.target.value)} name="UrlPhoto"></input>
                        <button type="button" onClick={handleImagePreview}>Aperçu</button><br/>

                        {imageUrl !== "" &&
                            <img src={imageUrl} alt="" style={{maxWidth: '300px', maxHeight: '300px'}} />
                        }
                        <br/>
                        <label htmlFor="TypePlante">TypePlante</label><br/>
                        <select id="TypePlante" value={TypePlante} onChange={(e) => setTypePlante(e.target.value)}>
                            <option value="">--Choisir un type de plante--</option>
                            {TypesPlante.map((typePlante) => (
                                <option key={typePlante.idTypePlante} value={typePlante.idTypePlante}>{typePlante.libelle}</option>
                            ))}
                        </select><br/>

                        <input type="submit" onClick={() => { SubmitPlante() }} value="Enregistrer"></input><br/>
                        <input type="submit" onClick={()=>{updatePlante()}} value="Modifier"></input><br/>
                    </form>
                    
                    <form action="http://localhost:3000/MesPlantes">
                        <button type="submit">Retour</button>
                    </form>
                </div>
            </div>
        </div>
    )

}
export default Mesplantes;