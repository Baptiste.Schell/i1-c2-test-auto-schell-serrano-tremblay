import React, { useState, useEffect } from "react";
import axios from "axios";
import NavbarHaut from "./Component/NavbarHaut";
import NavbarLateral from "./Component/NavbarLateral";
import "./Assets/CSS/MesConseils.css";
import Conseil from "./Component/Conseil"
import { useNavigate } from "react-router-dom";



function MesConseils() {
    const [Conseils, setConseils] = useState([]);
    const navigate=useNavigate() ; 

    useEffect(() => {
        axios({
          headers : {"authorization" : 'Bearer '+localStorage["Token"]},
            method : 'GET',
            url : process.env.REACT_APP_server + "GetConseilsByBotaniste/" + localStorage["UserMail"]
          }).then((resp) => {
            setConseils(resp.data)
          });
    }, []);

    return (
        <div style={{ width: "100%", height: "100%" }}>
            <div id="MesConseils">
                <NavbarLateral />
            </div>
            <NavbarHaut title="Mes conseils" />

            <div id="Main_Panel_conseil">
                <div>
                    <button class="boutton-ajouter" onClick={() => {navigate("/PosterConseil")}}>Poster un conseil</button>
                </div>
                <div id="Panel_of_card">
                  {Conseils.map((item) => {
                    return(<Conseil titre={item.titre} description={item.description} libelle={item.libelle} url={item.urlPhoto} idConseil={item.idConseil} idTypePlante={item.idTypePlante} />)
                  })}
                </div>

            </div>
        </div>
    );
}

export default MesConseils;
