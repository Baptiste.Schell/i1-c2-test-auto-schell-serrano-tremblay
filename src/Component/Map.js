import React, { useEffect, useState } from "react";
import "../Assets/CSS/Map.css"
import { useNavigate } from "react-router-dom";
import { MapContainer, TileLayer, useMap, Marker, Popup } from 'react-leaflet'
import L from "leaflet";
import axios from "axios";

function Map(props) {
const [Center,setCenter]=useState([])
const navigate=useNavigate() ;

useEffect(() => {
  axios({
    headers :{"authorization" : 'Bearer '+localStorage["Token"] },
    methode : 'get',
    url :process.env.REACT_APP_server+"GetCoordCenter/"+props.ville
 }).then((response) => {
     setCenter(response.data)
     console.log(Center)
 })
},[])

  const MarkerIcon = new L.Icon({
    iconUrl : require("../Assets/image/favicon-32x32.png"),
    iconSize :[48,48]

  })


  return (
    <MapContainer center={[43.5667378,3.8070518]} zoom={12} scrollWheelZoom={true}>
      <TileLayer url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png" attribution='&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'/>

      {props._Annonce.map((item) => {
        const coord=[item.lat,item.lgt]
        const idUser=[item.idUser]
        return (
          <Marker position={coord}  icon={MarkerIcon}>
            <Popup>
            <button onClick={() => navigate("/VisitAnnonce", {state: {idUser: idUser}})}>Voir les plantes à garder</button>
            </Popup>
          </Marker>
        )
      })}
    </MapContainer> )

    }

export default Map; 