import React, { useState } from "react";
import "../Assets/CSS/Plante.css"
import axios from "axios"
import { useNavigate } from "react-router";
import { useEffect } from "react";

function VisitPlante (props) {

    const navigate=useNavigate() ; 
    const [libelle,setLibelle]=useState("libelle")

    useEffect(() => {
        axios({
            headers :{"authorization" : 'Bearer '+localStorage["Token"] },
            method :'GET',
            url : process.env.REACT_APP_server+"GetTypePlantById/"+props.idtype
        }).then((response) => {
            setLibelle(response.data[0].libelle)
        })
    },[])

    return (
        <div id="card-plante">
            <div id="Photo-Card"> 
                <img src={props.url}></img>
            </div>

            <div id="label-plante">
                <h2> {props.nom}</h2>
            </div>
            <div id="label-plante">
                <button className="boutton-type-plante" onClick={() => {navigate('/AllConseilsForTypePlante',{state:{idTypePlante : props.idtype}})}}> {libelle}</button>
                <p><strong>Description :</strong><br/> {props.description} </p>
            </div>
        </div>
    ) 

}

export default VisitPlante ;