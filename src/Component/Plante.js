import React, { useState } from "react";
import "../Assets/CSS/Plante.css"
import axios from "axios"
import { useNavigate } from "react-router";
import { useEffect } from "react";
import { confirmAlert } from "react-confirm-alert";
import "react-confirm-alert/src/react-confirm-alert.css";

function Plante (props) {

    const navigate=useNavigate() ; 
    const [libelle,setLibelle]=useState("libelle")

    useEffect(() => {
        axios({
          headers :{"authorization" : 'Bearer '+localStorage["Token"] },
            method :'GET',
            url : process.env.REACT_APP_server+"GetTypePlantById/"+props.idtype
        }).then((response) => {
            setLibelle(response.data[0].libelle)
        })
    },[])


    function deletePlante()
    {
        axios({
          headers :{"authorization" : 'Bearer '+localStorage["Token"] },
            method : 'DELETE',
            url : process.env.REACT_APP_server+"DeletePlanteById/"+props.idPlante
          }).then(()=> {
            window.location.reload() ;
          })

    }
    
    function SupprimerPlante()   {
        confirmAlert({
          title: "Confirmation",
          message: "Etes-vous sûre de supprimer cette plante ?",
          buttons: [
            {
              label: "Oui",
              onClick: () => deletePlante()
            },
            {
              label: "Non"
             
            }
          ]
        });
      };

    return (
        <div id="card-plante">
            <div id="Photo-Card"> 
                <img src={props.url}></img>
            </div>

            <div id="label-plante">
                <h2> {props.nom}</h2>
            </div>
            <div id="label-plante">
                <button className="boutton-type-plante" onClick={() => {navigate('/AllConseilsForTypePlante',{state:{idTypePlante : props.idtype}})}}> {libelle}</button>
                <p><strong>Description :</strong><br/> {props.description} </p>
            </div>
            <div id="option-card">
                <button class="boutton-modifier" onClick={() => {navigate('/ModifyPlante', {state: {nom :props.nom,description : props.description, url :props.url,_libelle:libelle,idPlante:props.idPlante}})}}> <i className="fa-solid fa-pen-to-square"/>  </button>
                <button class="boutton-supprimer" style={{color :'Red'}} onClick={() => {SupprimerPlante()}}> <i className="fa-solid fa-x"></i></button>
            </div>
        </div>
    ) 

}

export default Plante ;