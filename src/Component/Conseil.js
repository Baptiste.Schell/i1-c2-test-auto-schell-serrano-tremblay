import React, { useState } from "react";
import "../Assets/CSS/Conseil.css"
import axios from "axios"
import { useNavigate } from "react-router";
import { useEffect } from "react";
import { confirmAlert } from "react-confirm-alert";
import "react-confirm-alert/src/react-confirm-alert.css";

function Conseil (props) {

    const [Conseils, setConseils] = useState([]);
    const navigate=useNavigate() ; 

    useEffect(() => {
        axios({
          headers :{"authorization" : 'Bearer '+localStorage["Token"] },
            method : 'GET',
            url : process.env.REACT_APP_server + "GetConseilsByBotaniste/" + localStorage["UserMail"]
          }).then((resp) => {
            setConseils(resp.data)
          });
    }, []);

    
    function deleteConseil(idConseil)
    {
        axios({
          header :{"authorization" : 'Bearer '+localStorage["Token"] },
            method : 'DELETE',
            url : process.env.REACT_APP_server+"DeleteConseilById/"+idConseil
          }).then(()=> {
            window.location.reload() ;
          })

    }

    function SupprimerConseil(idConseil)   {
        confirmAlert({
          title: "Confirmation",
          message: "Etes-vous sûre de supprimer ce conseil ?",
          buttons: [
            {
              label: "Oui",
              onClick: () => deleteConseil(idConseil)
            },
            {
              label: "Non"
             
            }
          ]
        });
      };

    return (
        <div id="card-conseil">
            <div id="Photo-Card"> 
                <img src={props.url}></img>
            </div>

            <div id="label-conseil">
                <h2> {props.titre}</h2>
            </div>
            <div id="label-conseil">
                <button className="boutton-type-plante" onClick={() => {navigate('/AllConseilsForTypePlante',{state:{idTypePlante : props.idTypePlante}})}}> {props.libelle}</button>
                <p><strong>Conseil :</strong><br/> {props.description} </p>
            </div>
            <div id="option-card">
                <button class="boutton-modifier" onClick={() => {navigate('/ModifierConseil', {state: {titre :props.titre,description : props.description, libelle : props.libelle,idConseil : props.idConseil, urlPhoto : props.url}})}}> <i className="fa-solid fa-pen-to-square"/> </button>
                <button class="boutton-supprimer" style={{color :'Red'}} onClick={() => {SupprimerConseil(props.idConseil)}}>  <i className="fa-solid fa-x"></i> </button>
            </div>
        </div>
    ) 

}

export default Conseil ;