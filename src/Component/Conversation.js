import React, { useEffect, useState } from "react";
import axios from 'axios' ; 
import "../Assets/CSS/Conversation.css"

function Conversation(props) 
{
    const [PDP,setPDP]=useState("")
    const [Expe,setExpe]=useState("")

    useEffect(() =>{
        
        if(props.From===localStorage["Pseudo"])
        {
            setExpe(props.To)
        }
        else
        {
            setExpe(props.From)
        }
        axios({
            headers :{"authorization" : 'Bearer '+localStorage["Token"] },
            method :'get',
            url : process.env.REACT_APP_server+"GetPDPByPseudo/"+Expe
        }).then((resp) => {
            setPDP(resp.data[0].photodeprofil)
            
        })
    })

    return (
        <div className="Conv">
            <div id="PDP">
                <img src={PDP}/>
            </div>
            <div id='Content'>
                <p style={{fontFamily :'Poppins',fontSize :'1.2em',fontWeight :'bold',color: '#0F956A'}}> {props.From===localStorage["Pseudo"] ? props.To : props.From} </p>
                <p style={{fontFamily :'Poppins',fontSize :'1.2em',fontWeight :'bold',color: 'grey',width :'80%',height :'20px'}}> {props.Message} </p>
            </div>
            <div>
            <i class="fa-solid fa-xmark"></i>
            </div>
        </div>
    )
}

export default Conversation ;