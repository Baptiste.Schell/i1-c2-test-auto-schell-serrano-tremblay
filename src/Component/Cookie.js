import React, { useState, useEffect } from "react";
import "../Assets/CSS/Cookie.css";
import Cookies from "js-cookie";
import { confirmAlert } from "react-confirm-alert";
import "react-confirm-alert/src/react-confirm-alert.css";
import PolitiqueConfidentialite from "./PolitiqueConfidentialite";

function Cookie() {
    const [showPopup, setShowPopup] = useState(false);
    const [accepted, setAccepted] = useState(false);
    const [ShowPolitiqueConf,setShowPolitique]=useState(false)



    function ShowPolitique() 
    {   
        confirmAlert({
            title: "Politique de confidentialité",
            message: "Etes-vous sûre de supprimer cette plante ?",
            buttons: [
              {
                label: "Oui",
              },
              {
                label: "Non"
               
              }
            ]
          });
    }


    const handleAccept = () => {
        Cookies.set("cookieConsent", "true", { expires: 365 });
        setAccepted(true);
        setShowPopup(false);
    };

    const handleDecline = () => {
        setAccepted(false);
        setShowPopup(false);
    };

    useEffect(() => {
        const consentCookie = Cookies.get("cookieConsent");
        if (consentCookie === undefined) {
            setShowPopup(true);
        } else {
            setAccepted(consentCookie === "true");
        }
    }, []);

    if (!showPopup && (accepted || Cookies.get("cookieConsent") === "false")) {
        return null; // Ne pas afficher le composant si les cookies ont été acceptés ou refusés
    }

    return (
        <div className="cookie-consent-dialog">
            {ShowPolitiqueConf && <PolitiqueConfidentialite/>}
            {showPopup && !accepted && (
                <div className="cookie-popup">
                    <p>Acceptez-vous l'utilisation des cookies sur ce site ?</p>

                    <p style={{textDecoration :'underline',color: "blue",cursor :"pointer"}} onClick={() => {setShowPolitique(true)}}> Politique de confidentialité </p>
                    <div className="button-container">
                        <button onClick={handleAccept}>Accepter</button>
                        <button onClick={handleDecline}>Refuser</button>
                    </div>
                </div>
            )}
        </div>
    );
}

export default Cookie;