import "../Assets/CSS/MessageRecu.css"


function MessageRecu(props) 
{  

    return(
        <div id="MessageRecu">
            <img src={props.PDP}/>
            <div id="MessageRecu-Content"><p> {props.Content} </p></div>
        </div>
    )
}

export default MessageRecu ; 