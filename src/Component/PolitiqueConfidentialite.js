import React, { useState, useEffect } from "react";
import "../Assets/CSS/PolitiqueConfidentialite.css";

function PolitiqueConfidentialite () 
{
    return(
        <div id="Main-div-politique"> 
        
            <h2> Politique de confidentialité </h2>
            <br/>
            <p>
                Cette politique de confidentialité décrit comment nous collectons, utilisons, stockons et protégeons les informations personnelles des utilisateurs de notre application web. 
                En utilisant notre application, vous consentez à la collecte et à l'utilisation de vos informations personnelles conformément à cette politique.
            </p>
            <br/>
        
            <p>
                    1. Collecte d'informations             <br/>

                Nous collectons les informations personnelles suivantes lorsque vous utilisez notre application web :

                Nom et prénom : Nous collectons votre nom et prénom afin de personnaliser votre expérience et de vous identifier.
                Adresse : Nous collectons votre adresse afin de fournir des fonctionnalités basées sur la localisation, telles que la recherche de plantes à proximité.
                Numéro de téléphone : Nous collectons votre numéro de téléphone pour faciliter la communication entre vous et les autres utilisateurs de l'application.
                Adresse e-mail : Nous collectons votre adresse e-mail pour vous envoyer des informations, des mises à jour et des nos applications mais aussi pour confirmer le changement de votre mot de passe.
            </p>
            <br/>

            <p>
                    2. Utilisation des informations            <br/>

                Nous utilisons les informations personnelles collectées pour les finalités suivantes :

                Fournir et améliorer notre application web, y compris le contenu et les fonctionnalités.
                Envoyer des notifications et des mises à jour concernant notre application.
                Analyser l'utilisation de l'application, surveiller les tendances et améliorer nos services.
                Protéger la sécurité des utilisateurs et prévenir les activités frauduleuses ou illégales.
            </p>
            <br/>


            <p>
                3. Partage des informations            <br/>

                Nous ne vendons, ne louons ni ne partageons vos informations personnelles avec des tiers, sauf dans les cas suivants :

                Lorsque vous nous avez donné votre consentement explicite pour le partage des informations.
                Lorsque le partage des informations est nécessaire pour fournir les services demandés par vous.
                Lorsque nous sommes légalement tenus de divulguer des informations conformément à la loi en vigueur, à une procédure judiciaire ou à une demande gouvernementale.
                <p/>

                <br/>
                <p>
                4. Sécurité des informations         <br/>           


                Nous mettons en place des mesures de sécurité appropriées pour protéger vos informations personnelles contre tout accès non autorisé, toute divulgation, toute altération ou toute destruction. Cependant, veuillez noter qu'aucune méthode de transmission sur Internet ou de stockage électronique n'est totalement sécurisée, et nous ne pouvons garantir la sécurité absolue de vos informations.
                </p><br/>
                <p> 
                5. Vos droits <br/>
                Vous avez le droit d'accéder, de corriger, de mettre à jour ou de supprimer vos informations personnelles. Vous pouvez exercer ces droits en nous contactant à l'adresse fournie en bas de cette politique de confidentialité.
                </p>
              <br/>
                <p>
                6. Modifications de la politique de confidentialité<br/>
                Nous nous réservons le droit de modifier cette politique de confidentialité à tout moment. Les modifications seront effectives dès leur publication sur notre application web. Nous vous encourageons à consulter régulièrement cette politique de confidentialité pour prendre connaissance des éventuelles mises à jour.
                </p>
                <br/>
               

                7. Consentement<br/>
                En utilisant notre application web, vous consentez à la collecte, à l'utilisation et à la divulgation de vos informations personnelles conformément à cette politique de confidentialité.

                8. Contactez-nous
                Si vous avez des questions, des préoccupations ou des demandes concernant cette politique de confidentialité ou nos pratiques en matière de confidentialité, veuillez nous contacter à l'adresse suivante :
                Adresse e-mail : support@arosaje.fr
                Adresse postale : 475 avenue des apothicaires Montpellelier 34000

                Nous nous efforcerons de répondre à vos demandes dans les meilleurs délais.

                Cette politique de confidentialité est en vigueur à partir du 05/06/2023.
            </p>

            <button id="Fermer-politique" onClick={() => {document.getElementById("Main-div-politique").style.visibility="hidden"}}> Fermer </button>
        </div>
    )
}


export default PolitiqueConfidentialite ; 