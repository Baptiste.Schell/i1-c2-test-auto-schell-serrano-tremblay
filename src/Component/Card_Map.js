import React, { useEffect,useState} from "react";
import "../Assets/CSS/Card_Map.css"
import axios from "axios"
import { useNavigate } from "react-router";

function Card_Map(props)
{
    const navigate=useNavigate() ;
    const [Plante, setPlante]=useState([]) 
    const [planteCount, setPlanteCount]= useState("")
    const [Role,setRole]=useState("")
    const [User,setUser]=useState([])
    
    useEffect(() => {
      axios({
        headers :{"authorization" : 'Bearer '+localStorage["Token"] },
          method : 'GET',
          url : process.env.REACT_APP_server + "GetUserByMail/" + props.mail
        }).then((resp) => {
            setUser(resp.data)
        });


    axios({
      headers :{"authorization" : 'Bearer '+localStorage["Token"] },
        method : 'GET',
        url : process.env.REACT_APP_server + "GetPlantByUser/" + props.mail
      }).then((resp) => {
        setPlante(resp.data)
        if (resp.data !== undefined) { // vérification que resp.data est défini
          setPlanteCount(resp.data.length); // ajout de cette ligne pour compter le nombre de plantes de l'utilisateur 
        } else {
          setPlanteCount(0); // si resp.data est indéfini, on initialise la valeur de count à 0
        }
      });

      axios({
        headers :{"authorization" : 'Bearer '+localStorage["Token"] },
        method : 'GET',
        url : process.env.REACT_APP_server + "GetRole/" + localStorage["UserMail"]
      }).then((resp) => {
        setRole(resp.data[0])
      });
    },[])

    function RequestReservation(idUser,idAnnonce) {
      axios({
        headers :{"authorization" : 'Bearer '+localStorage["Token"] },
          method: 'POST',
          url: process.env.REACT_APP_server + "SendRequestReservation/"+idUser+"/"+idAnnonce
      })
  }

  function AbonnementByBotaniste(idUser,idAnnonce) {
    axios({
      headers :{"authorization" : 'Bearer '+localStorage["Token"] },
        method: 'POST',
        url: process.env.REACT_APP_server + "SendAbonnementByBotaniste/"+idUser+"/"+idAnnonce
    })
}

  function SendMessage()
  {
     axios({
      headers :{"authorization" : 'Bearer '+localStorage["Token"] },
      method: 'POST',
      url: process.env.REACT_APP_server + "SendMessage/"+User.mail
     })
  }

    return (
        <div id="DFFGT334"> 
            <div id="DFFGT334-Nom-Prenom-pdp">
                <img src={props.url_pdp}/>
                <p style={{textDecoration : 'underline', cursor :'pointer'}} onClick={() => {navigate('/VisitProfil',{state: {mail: props.mail}})}}> {props.prenom}  {props.nom} </p>

            </div>

            <div id="DFFGT334-ville-reference">
                <p> <span style={{textDecoration :'underline'}}> Réference :</span> {props.reference}</p>
                <p> <span style={{textDecoration :'underline'}}> Ville :</span> {props.ville}</p>
                <p> <span style={{textDecoration :'underline'}}> Nombre de plante à gardé :</span> {planteCount}</p>
                <p> <span style={{textDecoration :'underline'}}> Niveau d'expertise requis :</span> {
                              props.expertise === 1 ? "Débutant" :
                              props.expertise === 2 ? "Connaisseur" :
                              props.expertise === 3 ? "Professionnel" : ""
                          }</p>
            </div>

            <div id="DFFGT334-date"> 
                <p> Du <span style={{backgroundColor :'#56C774',color :'white',paddingInline :"8%",borderRadius :'5px'}}> {props.Date_Debut} </span></p>
                <p> Du <span  style={{backgroundColor :'#56C774',color :'white',paddingInline :"8%",borderRadius :'5px'}}> {props.Date_Fin} </span></p>
            </div>
            {Role.idRole === 2 ? (
                <button onClick={() => {
                  const confirmation = window.confirm("Êtes-vous sûr de vouloir vous abonnez à cette annonce ?");
                  if (confirmation) {
                    AbonnementByBotaniste(props.idUser,props.idAnnonce);
                  }
                  }}> Abonnement à cette annonce</button>
            ) : (
                <button onClick={() => {
                    const confirmation = window.confirm("Êtes-vous sûr de vouloir faire une demande de réservation ?");
                    if (confirmation) {
                        RequestReservation(props.idUser, props.idAnnonce);
                    }
                }}> Demande de réservation</button>
            )}

           
              <button id="ContactMessage" 
                style={{marginTop :'10px',marginBottom :'10px',backgroundColor :"#0F956A",color :"white"}}
                onClick={() => {navigate('/Messagerie',{state: {CreateNewConv: true,Pseudo : props.Pseudo, PDP:props.url_pdp }})}}> 
                Contacter par message
              </button>
            


        </div>
    )
}
export default Card_Map ; 

