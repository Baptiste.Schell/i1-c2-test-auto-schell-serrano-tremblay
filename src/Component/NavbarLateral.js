
import { Link } from 'react-router-dom';
import "../Assets/CSS/NavbarLateral.css"
import Logo from "../Assets/image/Logo.png"
import React from "react";
import { useNavigate } from 'react-router-dom';
import { useState,useEffect} from "react";
import axios from "axios";



function Navbar()
{
  const navigate=useNavigate() ; 
  const [Annonce, setAnnonce] = useState("")
  const [AnnonceNonActive, setAnnonceNonActive] = useState("")
  const [AnnonceReserve, setAnnonceReserve] = useState("")
  const [User, setUser]=useState("")
  const [AnnonceBotaniste, setAnnonceBotaniste]=useState("")
  const [Reservation,setReservation] = useState([])
  const [Plante,setPlante]=useState([])

  function Logout()
  {
    localStorage.clear();
    navigate("/Auth") ; 
  
  }

  useEffect(() => {
    axios({
      headers :{"authorization" : 'Bearer '+localStorage["Token"] },
        method : 'GET',
        url : process.env.REACT_APP_server + "GetAnnoncesActive/" + localStorage['UserMail']
      }).then((resp) => {
        setAnnonce(resp.data[0])
      });

      axios({
        headers :{"authorization" : 'Bearer '+localStorage["Token"] },
        method : 'GET',
        url : process.env.REACT_APP_server + "GetAnnonces/" + localStorage['UserMail']
      }).then((resp) => {
        setAnnonceNonActive(resp.data[0])
      });

    axios({
      headers :{"authorization" : 'Bearer '+localStorage["Token"] },
        method : 'GET',
        url : process.env.REACT_APP_server + "GetAnnoncesReserved/" + localStorage['UserMail']
      }).then((resp) => {
        setAnnonceReserve(resp.data[0])
      });

    axios({
      headers :{"authorization" : 'Bearer '+localStorage["Token"] },
        method : 'GET',
        url : process.env.REACT_APP_server + "GetUserByMail/" + localStorage["UserMail"]
    }).then((resp) => {
        setUser(resp.data[0])
    });

    axios({
      headers :{"authorization" : 'Bearer '+localStorage["Token"] },
      method : 'GET',
      url : process.env.REACT_APP_server + "GetFollowAnnonce/" + localStorage["UserMail"]
    }).then((resp) => {
      setAnnonceBotaniste(resp.data[0])
    });

    axios({
      headers :{"authorization" : 'Bearer '+localStorage["Token"] },
      method : 'GET',
      url : process.env.REACT_APP_server + "GetPlantByUser/" + localStorage["UserMail"]
    }).then((resp) => {
      setPlante(resp.data)
    });

    axios({
      headers :{"authorization" : 'Bearer '+localStorage["Token"] },
      method: "GET",
      url: process.env.REACT_APP_server + "GetReservation/" + localStorage["UserMail"],
    }).then((resp) => {
        setReservation(resp.data);
    });
    },[])


    return(
        <div id="NavbarMain" >
           <div id="LogoCerculaire" style={{width :'150px' ,height : '150px'}}> 
              <img src={Logo}/>
           </div>
           
           <hr></hr>
           <div id="Mylink"> 
                <Link to="/Dashboard" className="alink" > <i class="fa-solid fa-layer-group"></i>Acceuil </Link>

                {User.idRole !== 2 && (
                  <Link to="/Mesplantes" className="alink">
                    <i className="fa-solid fa-seedling"></i> Mes plantes
                  </Link>
                )}

                <Link to="/FindPlant" className="alink" > <i class="fa-solid fa-magnifying-glass"></i>Rechercher une annonce </Link>

                {User.idRole !== 2 && Reservation.length !==0 && (
                  <Link className="alink" to="/MesReservation">
                    <i class="fa-solid fa-envelope"></i> Mes demandes de gardiennage 
                  </Link>
                )}

                {User.idRole == 1 && AnnonceNonActive && (
                  <Link to="/GestionDemandes" className='alink'>
                      <i className="fa-solid fa-list-ul"></i> Gestion des demandes
                  </Link>
                )}

                {Annonce && Annonce.length !== 0 && (
                  <Link to="/MonAnnonce" className="alink">
                    <i class="fa-sharp fa-solid fa-scroll"></i> Mon annonce
                  </Link>
                )}

                {AnnonceReserve && AnnonceReserve.length !==0 && (
                  <Link to="/AnnonceSuivis" className="alink" >  
                  <i class="fa-solid fa-calendar-week"></i> Annonce suivie 
                  </Link>
                )}

                {User.idRole !== 2 && (!Annonce || Annonce.active !== 1) && Plante.length !==0 && (
                  <Link to="/AddAnnonce" className='alink'>
                    <i class="fa-sharp fa-solid fa-scroll"></i> Poster une annonce
                  </Link>
                )}

                {User.idRole == 2 && (
                  <Link to="/MesConseils" className='alink'>
                      <i class="fa-sharp fa-solid fa-scroll"></i> Mes conseils
                  </Link>
                )}

                {User.idRole == 2 && AnnonceBotaniste && AnnonceBotaniste.active == 1 && (
                  <Link to="/AnnonceSuivieBotaniste" className='alink'>
                      <i class="fa-sharp fa-solid fa-scroll"></i> Annonce suivie
                  </Link>
                )}

                  <Link to="/Messagerie" className="alink">
                  <i class="fa-solid fa-message"></i> Messagerie
                  </Link>

           </div>
           <div id="Logout">
             <hr></hr>
             <br></br>
             <div id="X233222"><i class="fa-solid fa-right-from-bracket fa-2xl"></i><button onClick={() => {Logout() }}> Se déconnecter </button></div>
           </div>
        </div>
    )
}


export default Navbar ; 