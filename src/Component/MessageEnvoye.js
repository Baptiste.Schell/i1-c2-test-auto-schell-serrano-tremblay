import "../Assets/CSS/MessageEnvoye.css"


function MessageEnvoye(props) 
{
    return(
        <div id="MessageEnvoye">    
            <div id="MessageEnvoye-Content"><p> {props.Content} </p></div>
            <img src={props.PDP}/>
        </div>
    )
}

export default MessageEnvoye ; 