
import "../Assets/CSS/NavbarHaut.css"
import { useNavigate } from "react-router-dom";
import React, { useEffect,useState } from "react";
import axios from "axios";

function NavbarHaut(props)
{
    const [User, setUser]=useState("")
    const navigate=useNavigate()

    useEffect(() => {
        axios({
            headers :{"authorization" : 'Bearer '+localStorage["Token"] },
          method : 'GET',
          url : process.env.REACT_APP_server + "GetUserByMail/"+localStorage["UserMail"]
        }).then((resp) => {
          setUser(resp.data[0])
        })
    },[])
    
    return (
        <div id="Main">
            <div id="NamePage"> 
                <p> {props.title} </p>
            </div>

            <div id="Profil">
                <div id="Notif-Nom"> 
                    <p>{User.nom + " "+ User.prenom} </p>     
                </div> 
                <div id="PDP-Profil">
                <img alt="PDP" src={User.photodeprofil} onClick={() => {navigate(User.idRole === 1 ? "/Profil" : "/ProfilBotaniste")}}/>
                </div>
            </div>
        </div>
    )
}


export default NavbarHaut ; 