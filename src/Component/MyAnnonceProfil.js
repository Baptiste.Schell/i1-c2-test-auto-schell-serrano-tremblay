import React, { useEffect, useState } from "react";
import "../Assets/CSS/MyAnnonceProfil.css"
import { useNavigate } from "react-router-dom";
import axios from "axios";
import Temp_img from "../Assets/image/no-image.jpg"



function MyAnnonceProfil(props)
{
    const navigate=useNavigate() ;
    const [NiveauExpertise,setNiveauExpertise]=useState("")
    const [Photodeprofil,setPhotodeprofil]=useState("")

    useEffect(() => {
        axios({
            headers :{"authorization" : 'Bearer '+localStorage["Token"] },
            method : 'GET',
            url : process.env.REACT_APP_server + "GetProfilePhotoByPseudo/" + props.pseudoTest
        }).then((resp) => {
            if(resp.data[0].photodeprofil != undefined){
                setPhotodeprofil(resp.data[0].photodeprofil)
            }
        });

        if(props.Expertise===1)
        {
            setNiveauExpertise("Débutant")
        }
        else if(props.Expertise===2)
        {
            setNiveauExpertise("Connaisseur")
        }
        else
        {
            setNiveauExpertise("Expert")
        }
    }, [])

    function GetProfilePhotoByPseudo(Pseudo)
    {
        axios({
            headers :{"authorization" : 'Bearer '+localStorage["Token"] },
            method :"GET" ,
            url :process.env.REACT_APP_server+"GetProfilePhotoByPseudo/"+Pseudo
        })
    }

    return(
        <div class="card-mon-annonce">
            <div class="title-card-mon-annonce">
                <p class="title-date-card-mon-annonce">Annonce postée
                    <br/><p class="subtitle-date-card-mon-annonce">du {props.Date_debut} au {props.Date_fin}</p>
                </p>
                <p class="title-ref-card-mon-annonce">ref : {props.Reference} </p>
            </div>
            <div className="div-de-separation-mon-annonce"></div>
            <div class="description-mon-annonce">
                <div class="description-label-mon-annonce">
                    Description
                </div>
                <div class="description-text-mon-annonce">
                    {props.Description}
                </div>
            </div>
            <div class="bottom-section-mon-annonce">
                <div class="pdp-nom-prenom-mon-annonce">
                    {Photodeprofil ? (
                       <a href="/VisitProfil" onClick={() => {navigate("/VisitProfil", {state: {mail: props.mailTest}})}}>
                        <img src={Photodeprofil} alt="Photo de profil" />
                       </a>
                    ):(
                        <img src={Temp_img}></img>
                    )
                    }
                    <div class="nom-prenom-label-mon-annonce">
                        <p class="garde-par-label">Gardé par</p>
                        {props.pseudoTest ? (
                            <p className="nom-prenom-text-mon-annonce">{props.pseudoTest}</p>
                        ) : (
                            <p className="nom-prenom-text-mon-annonce">Aucun gardien trouvé..</p>
                        )}
                    </div>
                </div>
                <div class="niveau-expertise-tag">
                    <p class="niveau-expertise-text">{NiveauExpertise}</p>
                </div>
            </div>
        </div>


       /*
        <div id="MyAnnonceProfil-description">

        </div>

        <div id="MyAnnonceProfil-Garder-par">
            <p> Gardé par :

            </p>
        </div>


        <div id="MyAnnonceProfil-EnSavoirPlus">
            <button onClick={() => {
                if (props.pseudoTest) {
                    navigate('/VisitProfil', {state: {mailTest: props.mailTest}});
                } else {
                    navigate('/Dashboard');
                }
            }}> En savoir + </button>
        </div>*/
    )


}



export default MyAnnonceProfil ; 