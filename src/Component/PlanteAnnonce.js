import React, { useState } from "react";
import "../Assets/CSS/Plante.css"
import axios from "axios"
import { useNavigate } from "react-router";
import { useEffect } from "react";
import "react-confirm-alert/src/react-confirm-alert.css";

function Plante (props) {

    const navigate=useNavigate() ;
    const [libelle,setLibelle]=useState("libelle")

    useEffect(() => {
        axios({
            headers :{"authorization" : 'Bearer '+localStorage["Token"] },
            method :'GET',
            url : process.env.REACT_APP_server+"GetTypePlantById/"+props.idtype
        }).then((response) => {
            setLibelle(response.data[0].libelle)
        })
    },[])


    return (
        <div id="MAin-55588992">
            <div id="Photo-Card">
                <img src={props.url}></img>
            </div>

            <div id="info-card">
                <p style={{fontSize : '2.5em'}}> {props.nom}</p>
                <p> {libelle}</p>
                <p> {props.description} </p>
            </div>
        </div>
    )

}

export default Plante ;