import NavbarLateral from "./Component/NavbarLateral"
import NavbarHaut from "./Component/NavbarHaut"
import { useNavigate } from "react-router-dom";
import "./Assets/CSS/Dashboard.css"
import React, { useEffect, useState } from "react";
import axios from "axios";
import Cookie from "./Component/Cookie"


function App() {
    const [User, setUser] = useState("")
    const navigate = useNavigate()
    const [showDialog, setShowDialog] = useState(false);



    useEffect(() => {
        axios({
            headers : {"authorization" : 'Bearer '+localStorage["Token"]},
            method: 'GET',
            url: process.env.REACT_APP_server + "GetUserByMail/" + localStorage["UserMail"]
        }).then((resp) => {
            setUser(resp.data[0])
        })
    }, [])

    useEffect(() => {
        const consentCookie = localStorage.getItem("cookieConsent");
        if (consentCookie !== "true") {
            setShowDialog(true);
        }
    }, []);

    const handleShowDialog = () => {
        setShowDialog(true);
    };

    return (
        <div style={{ width: '100%', height: '100%' }}>
            <div id="Navbar">
                <NavbarLateral />
            </div>
            <NavbarHaut title="Acceuil" />
            <div id="Main_Panel">
                <div class="conteneur">
                    <div class="left-div">
                        <Cookie />
                        <p class="title">Les plantes d’intérieur ont-elles des effets bénéfiques sur la santé ?</p>
                        <p class="sommaire">
                            <span class="sommaire-title"> EN BREF</span>
                            <p class="sommaire-subtitle">Avoir des plantes d’intérieur et les entretenir peut :</p>
                            <ul>
                                <li>Réduire le stress psychologique et physiologique.</li>
                                <li>Améliorer la convalescence après une opération chirurgicale.</li>
                                <li>Augmenter l’attention et la concentration.</li>
                                <li>Augmenter la créativité et la productivité.</li>
                            </ul>
                        </p>
                        <p class="description">
                            Dans nos sociétés modernes où tout semble aller de plus en plus vite, plusieurs ressentent
                            les effets néfastes du stress et de l’anxiété ; or cela semble s’être accentué depuis le
                            début de la pandémie de COVID-19. Pendant le printemps et l’été 2020, de nombreux Québécois
                            ont profité
                            de la belle saison pour se ressourcer dans la nature, soit en visitant un parc, en faisant
                            du
                            camping, de la marche en forêt ou en louant un chalet à la campagne. À l’approche de
                            l’hiver, les
                            contacts avec la verdure se raréfient et les voyages dans des contrées aux climats plus
                            chauds sont
                            risqués et fortement déconseillés par la Santé publique.
                            <br />
                            À part les randonnées dans nos belles forêts de conifères, un des seuls contacts possibles
                            avec la verdure durant ce long hiver sera nos plantes vertes dont nous prenons soin dans nos
                            logements.
                            <br />
                            Les plantes d’intérieur décorent et amènent une touche naturelle dans nos foyers,
                            mais ont-elles des effets bénéfiques avérés sur notre santé physique et mentale ?
                        </p>
                        <p class="description">
                            <p class="subtitle">Réduction du stress</p>
                            Une revue systématique réalisée en 2019 a répertorié quelques 50 études sur les bienfaits
                            psychologiques des plantes d’intérieur, la plupart de ces études étant de qualité moyenne.
                            Les effets positifs les plus notables des plantes d’intérieur sur les participants sont une
                            augmentation des émotions positives et une diminution des émotions négatives, suivi d’une
                            réduction de l’inconfort physique.
                            <br />
                            Dans une étude randomisée contrôlée à plan croisé auprès de jeunes adultes, les participants
                            ont
                            vu leur humeur s’améliorer davantage après avoir transplanté une plante d’intérieur qu’après
                            avoir
                            exécuté une tâche à l’ordinateur. De plus, la pression artérielle diastolique et l’activité
                            du
                            système nerveux sympathique (réponse physiologique au stress) des participants étaient
                            significativement moins élevées après avoir transplanté une plante qu’après avoir exécuté
                            une
                            tâche à l’ordinateur. Ces résultats indiquent que l’interaction avec des plantes d’intérieur
                            peut
                            réduire le stress psychologique et physiologique par comparaison à un travail mental.
                        </p>
                        <p className="description">
                            <p className="subtitle">Les plantes au bureau</p>
                            Une équipe japonaise a réalisé en 2020 une étude sur les effets des plantes en milieu de
                            travail sur le niveau de stress psychologique et physiologique des travailleurs. Dans la
                            première phase de l’étude (1 semaine), les travailleurs travaillaient à leur bureau en
                            absence de plante, alors que durant la phase d’intervention (4 semaines) les participants
                            pouvaient voir et entretenir une plante d’intérieur qu’ils ont pu choisir parmi 6 différents
                            types (bonsaï, tillandsia, echeveria, cactus, plante à feuillage, kokedama).
                            <br />
                            Les
                            participants ont reçu l’instruction de prendre une pause de trois minutes lorsqu’ils
                            ressentaient de la fatigue et de prendre leur pouls avant et après la pause. Durant ces
                            pauses de 3 minutes, les travailleurs devaient regarder leur bureau (avec ou sans plante
                            d’intérieur). Les chercheurs ont mesuré le stress psychologique avec le questionnaire sur
                            l’anxiété chronique et réactionnelle (STAI ; State-Trait Anxiety Inventory). L’implication
                            des participants était donc à la fois passive (regarder la plante) et active (arroser et
                            entretenir la plante).
                            <br />
                            Le stress psychologique évalué par le STAI était significativement, quoique modérément,
                            moins élevé durant l’intervention en présence d’une plante d’intérieur que durant la période
                            sans plante. La fréquence cardiaque de la majorité des patients (89 %) n’était pas
                            significativement différente avant et après l’intervention, alors qu’elle a diminué chez 4,8
                            % des participants et augmenté chez 6,3 % des patients. On doit conclure que l’intervention
                            n’a pas eu d’effet sur le rythme cardiaque qui est un indicateur du stress physiologique,
                            même si elle a réduit légèrement le stress psychologique.
                            <br />
                            Une étude réalisée auprès de 444 employés de l’Inde et des États-Unis indique que les
                            environnements de bureau incluant des éléments naturels telles les plantes d’intérieur et
                            l’exposition à la lumière naturelle influencent positivement la satisfaction et
                            l’implication au travail. Ces éléments naturels semblent agir comme des « tampons » contre
                            les effets du stress et de l’anxiété générés par le travail.
                        </p>
                        <p className="description">
                            <p className="subtitle">Convalescence après une opération chirurgicale</p>
                            Il semble que les plantes favorisent la convalescence de patients après une opération
                            chirurgicale selon une étude réalisée dans un hôpital en Corée. Quatre-vingts femmes en
                            convalescence après une thyroïdectomie ont été assignées au hasard à une salle sans plantes
                            ou à une salle avec des plantes d’intérieur (à feuillage et à fleurs).
                            <br /> Les données
                            recueillies pour chaque patiente incluaient la durée de l’hospitalisation, l’utilisation
                            d’analgésiques pour contrôler la douleur, les signes vitaux, l’intensité de la douleur
                            perçue, l’anxiété et la fatigue, l’index STAI (stress psychologique) et d’autres
                            questionnaires.<br /> Les patientes qui ont été hospitalisées dans des chambres avec des plantes
                            d’intérieur et des fleurs ont eu une durée d’hospitalisation plus courte, pris moins
                            d’analgésiques, ressenti moins de douleur, d’anxiété et de fatigue, et elles ont eu
                            davantage d’émotions positives et une plus grande satisfaction à propos de leur chambre que
                            les patientes qui ont récupéré de leur opération dans une chambre sans plantes. Les mêmes
                            chercheurs ont réalisé une étude similaire auprès de patients qui récupéraient après une
                            appendicectomie. Ici encore les patients qui avaient des plantes et des fleurs dans leur
                            chambre ont mieux récupéré de leur opération chirurgicale que ceux qui n’avaient pas de
                            plantes dans leur chambre.
                        </p>
                        <p className="description">
                            <p className="subtitle">Amélioration de l’attention et de la concentration</p>
                            23 élèves à l’école élémentaire (âgés de 11 à 13 ans) ont participé à une étude où ils ont
                            été mis dans une pièce où se trouvait soit une plante artificielle, une vraie plante, une
                            photographie d’une plante ou pas de plante du tout. Les participants portaient un appareil
                            d’électroencéphalographie sans fil durant les 3 minutes d’exposition aux différents stimuli.<br />
                            Les enfants qui ont été mis en présence d’une vraie plante étaient plus attentifs, plus à
                            même de se concentrer que ceux des autres groupes. De plus, la présence d’une vraie plante
                            était associée à une meilleure humeur en général.
                        </p>
                        <p className="description">
                            <p className="subtitle">Productivité</p>
                            Une étude transversale auprès de 385 travailleurs de bureau en Norvège a trouvé une
                            association significative, quoique très modeste, entre le nombre de plantes présentes dans
                            leur bureau et le nombre de jours de congé de maladie et la productivité. En effet, les
                            travailleurs qui avaient davantage de plantes dans leur bureau ont pris un peu moins de
                            journées de congé de maladie et ont été un peu plus productifs au travail.<br /> Dans une autre
                            étude, des étudiants américains devaient accomplir des tâches à l’ordinateur, en présence ou
                            en absence de plantes d’intérieur dans des pièces sans fenêtre. En présence de plantes, les
                            participants ont été plus productifs (12 % plus rapide dans l’exécution des tâches) et moins
                            stressé puisque leur pression artérielle était moins élevée qu’en absence de plantes
                            d’intérieur.
                        </p>
                        <p className="description">
                            <p className="subtitle">Et la qualité de l’air ?</p>
                            Les plantes d’intérieur peuvent procurer des bienfaits pour la santé en diminuant le stress
                            psychologique et physiologique.<br /> Posséder et entretenir des plantes peut améliorer l’humeur
                            et augmenter l’attention et la concentration. De nouvelles études, plus puissantes et mieux
                            contrôlées seront nécessaires pour mieux cerner et comprendre les effets des plantes sur la
                            santé humaine.
                        </p>
                        <p className="description">
                            <p className="subtitle">Conclusion :</p>
                            Les plantes d’intérieur peuvent procurer des bienfaits pour la santé en diminuant le stress
                            psychologique et physiologique. Posséder et entretenir des plantes peut améliorer l’humeur
                            et augmenter l’attention et la concentration. De nouvelles études, plus puissantes et mieux
                            contrôlées seront nécessaires pour mieux cerner et comprendre les effets des plantes sur la
                            santé humaine.
                        </p>
                    </div>
                    <div class="right-divs">
                        <div class="right-div-1">
                            <p className="subtitle">Article écrit par</p>
                            <div class="profile">
                                <div className="image-wrapper">
                                    <img src={User.photodeprofil} onClick={() => { navigate("/VisitProfilBotaniste", { state: { mail: User.mail } }) }} alt="Photo de profil" />
                                </div>
                                <div className="text-wrapper">
                                    <h2>{User.nom}<br />
                                        {User.prenom}<br />
                                    </h2>
                                    <p className="profession">Botaniste à Paris</p>
                                </div>
                            </div>
                        </div>
                        <div class="right-div-2">
                            <img src="https://maison.20minutes.fr/wp-content/uploads/2020/05/palmier-areca-istock.jpg"
                                alt="Description de l'image" />
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default App;
