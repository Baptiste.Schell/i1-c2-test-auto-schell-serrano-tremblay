import axios from 'axios';
import MockAdapter from 'axios-mock-adapter';
import { SendMessage, LoadMessageFromConversation, ReloadConversation } from './Messagerie';

// Mocking localStorage
const localStorageMock = (function () {
    let store = {};
    return {
        getItem(key) {
            return store[key] || null;
        },
        setItem(key, value) {
            store[key] = value.toString();
        },
        clear() {
            store = {};
        }
    };
})();
Object.defineProperty(window, 'localStorage', { value: localStorageMock });

// Setting up axios mock adapter
const mock = new MockAdapter(axios);

describe('Messagerie functions', () => {
    beforeEach(() => {
        localStorage.clear();
        mock.reset();
    });

    it('should send a message successfully', async () => {
        localStorage.setItem('Token', 'testToken');
        localStorage.setItem('Pseudo', 'testUser');

        const respData = 'OK';
        mock.onPost(`${process.env.REACT_APP_server}SendMessage/testUser/testConv/testMessage`)
            .reply(200, respData);

        const setNewMessage = jest.fn();
        const setMessageFromConversation = jest.fn();
        const setConversationList = jest.fn();

        const result = await SendMessage('testConv', 'testMessage', setNewMessage, setMessageFromConversation, setConversationList);

        expect(result).toBe(respData);
        expect(setNewMessage).toHaveBeenCalledWith('');
    });

    it('should load messages from a conversation successfully', async () => {
        localStorage.setItem('Token', 'testToken');
        localStorage.setItem('Pseudo', 'testUser');

        const messages = [{ FromPseudo: 'testUser', Message: 'Hello' }];
        const pdp = [{ photodeprofil: 'testProfilePic' }];

        mock.onGet(`${process.env.REACT_APP_server}LoadMessageByConversation/testUser/testConv`)
            .reply(200, messages);
        mock.onGet(`${process.env.REACT_APP_server}GetPDPByPseudo/testConv`)
            .reply(200, pdp);

        const setConvHeadTitle = jest.fn();
        const setPDP_Destinataire = jest.fn();
        const setConvPDP = jest.fn();
        const setMessageFromConversation = jest.fn();

        const result = await LoadMessageFromConversation('testUser', 'testConv', setConvHeadTitle, setPDP_Destinataire, setConvPDP, setMessageFromConversation);

        expect(result).toEqual(messages);
        expect(setConvHeadTitle).toHaveBeenCalledWith('testConv');
        expect(setPDP_Destinataire).toHaveBeenCalledWith('testProfilePic');
        expect(setConvPDP).toHaveBeenCalledWith('testProfilePic');
        expect(setMessageFromConversation).toHaveBeenCalledWith(messages);
    });

    it('should reload a conversation successfully', async () => {
        localStorage.setItem('Token', 'testToken');
        localStorage.setItem('Pseudo', 'testUser');

        const messages = [{ FromPseudo: 'testUser', Message: 'Hello' }];
        mock.onGet(`${process.env.REACT_APP_server}LoadMessageByConversation/testUser/testConv`)
            .reply(200, messages);

        const setMessageFromConversation = jest.fn();

        const result = await ReloadConversation('testConv', setMessageFromConversation);

        expect(result).toEqual(messages);
        expect(setMessageFromConversation).toHaveBeenCalledWith(messages);
    });
});
