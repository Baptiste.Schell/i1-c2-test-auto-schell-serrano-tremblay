import React from "react";
import NavbarHaut from "./Component/NavbarHaut";
import NavbarLateral from "./Component/NavbarLateral"
import { useState,useEffect} from "react";
import axios from "axios";
import { useLocation, useNavigate } from "react-router-dom";
import "./Assets/CSS/AllConseilsForTypePlante.css";


function AllConseilsForTypePlante () {

    const navigate=useNavigate()
    const location=useLocation()
    const [idTypePlante, setidTypePlante] = useState(location.state.idTypePlante);
    const [TypePlante,setTypePlante]=useState("")
    const [AllConseils,setAllConseils]=useState([])
    const [Botaniste, setBotaniste]=useState("")
    // comportement
    useEffect(() => {

    axios({
     headers : {"authorization" : 'Bearer '+localStorage["Token"]},
      method : 'GET',
      url : process.env.REACT_APP_server + "GetTypePlantById/" + idTypePlante
    }).then((resp) => {
      setTypePlante(resp.data[0])
    });

    axios({
        headers : {"authorization" : 'Bearer '+localStorage["Token"]},
        method : 'GET',
        url : process.env.REACT_APP_server + "GetConseilAndUserByTypePlant/" + idTypePlante
      }).then((resp) => {
        setAllConseils(resp.data)
      });
},[])

    return (
    <div style={{ width: '100%', height: '100%' }}>
      <div id="AllConseilsForTypePlante">
        <NavbarLateral />
      </div>
      <NavbarHaut title={TypePlante.libelle} />

        <div id="Main_Panel_Profil">
            <div className="card-entiere-typeplant-page">
                <div class="card-description-type-plante">
                    <img class="photo-type-plante-page" src={TypePlante.urlPhoto}/>
                    <div class="section-right-typelante-page">
                        <p class="title-right-typeplant-page">{TypePlante.libelle}</p>
                        <p class="desc-right-typeplant-page">{TypePlante.description}</p>
                    </div>
                </div>
            </div>

            {AllConseils.length === 0 ? (
              <p>Aucun conseils pour ce type de plante pour le moment !</p>
            ) : (

            <div class="section-card-mes-annonces">
            {AllConseils.map((item) => {
                return (
                    <>
                        <div class="card-mon-conseil-page-typeplant">
                            <div class="section-title-top-conseil">
                                <p class="text-title-top-conseil">{item.titre}</p>
                            </div>
                            <div className="div-de-separation-mon-conseil"></div>
                            <div class="section-bottom-conseil">
                                <div class="right-img-conseil-card-type-plante-page">
                                    <div class="description-card-conseil">
                                        <p class="title-desc-conseil">Conseil :</p>
                                        <p class="paragraph-desc-conseil">{item.description}</p>
                                    </div>
                                    <div className="button-type-plant-conseil" onClick={() => {
                                        navigate('/VisitProfilBotaniste', {state: {mail: item.mail}})
                                    }}>
                                        <p className="text-type-plant-conseil">{item.pseudo}</p>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </>
                )
            })
            }
            </div>
                
            )}

        </div>
    </div>
    )
}
export default AllConseilsForTypePlante ; 
