import React, { useState, useEffect } from "react";
import axios from "axios";
import NavbarHaut from "./Component/NavbarHaut";
import NavbarLateral from "./Component/NavbarLateral";
import "./Assets/CSS/Mesplantes.css";
import Plante from "./Component/Plante"
import { useNavigate } from "react-router-dom";



function Mesplantes() {
    const [PlantesList, setPlantes] = useState([]);
    const navigate=useNavigate() ; 

    useEffect(() => {
        axios({
            headers : {"authorization" : 'Bearer '+localStorage["Token"]},
            method: "GET",
            url: process.env.REACT_APP_server + "GetPlantByUser/" + localStorage["UserMail"],
        })
        .then((resp) => {
            setPlantes(resp.data);
        })
    }, []);



    return (
        <div style={{ width: "100%", height: "100%" }}>
            <div id="Mesplantes">
                <NavbarLateral />
            </div>
            <NavbarHaut title="Mes plantes" />

            <div id="Main_Panel_plante">
                <div>
                    <button class="boutton-ajouter" onClick={() => { navigate("/AddPlante")}}> Ajouter une plante </button>
                </div>

                <div id="Panel_of_card">
                  {PlantesList.map((item) => {
                    return(<Plante idPlante={item.idPlante} url={item.urlPhoto} nom={item.nom} description={item.description} idtype={item.idTypePlante} />)
                  })}
                </div>

            </div>
        </div>
    );
}

export default Mesplantes;
