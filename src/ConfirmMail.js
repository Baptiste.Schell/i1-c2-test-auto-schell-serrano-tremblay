import React, { useEffect, useState } from "react";
import axios from "axios";
import "./Assets/CSS/ConfirmMail.css"
import { useNavigate, useLocation } from "react-router-dom";
import ErrorCode from './Component/ErrorCode'




function ConfirmMail() {
    const location = useLocation();
    const navigate = useNavigate();
    const [token,setToken]=useState(location.state.Token)
    const [Code, setCode] = useState(location.state.Confirmation_code)
    const [Login,setLogin]=useState(location.state.Mail)
    const [InsertedCode, setInsertedCode] = useState("")
    const [Error, setErrorCode] = useState(false)


   

    function ValidateCode()
    {
  
        if(Code!=InsertedCode)
        {
            setErrorCode(true)
        }
        else
        {
            axios({
                headers : {"authorization" : 'Bearer '+token},
                method: "post",
                url: process.env.REACT_APP_server + "ConfirmAccount/" + Login
              }).then((response) => {
                if (response.data.Status==="OK") {
                  localStorage["UserMail"] = Login;
                  localStorage["Token"]=response.data.token
                  
                  navigate("/Dashboard");
                }
              });
              
           
        }
    }

    return (
        <div id="Confirm-Mail">

            <p> Nous avons besoins de confirmer votre adresse mail, pour ça, veuilez saisir le code à six chiffres envoyé par mail </p>
            <div id="Code">
                <input value={InsertedCode} type="number" onChange={(e) => { setInsertedCode(e.target.value) }} placeholder="VOTRE CODE" />
            </div>
            {Error && <ErrorCode/>}

            <button onClick={() => ValidateCode()}> Confirmer </button>
        </div>)
}

export default ConfirmMail; 
