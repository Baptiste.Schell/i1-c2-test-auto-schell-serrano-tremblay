import React, { useState, useEffect } from "react";
import { useNavigate,useLocation } from "react-router";
import axios from "axios";
import NavbarHaut from "./Component/NavbarHaut";
import NavbarLateral from "./Component/NavbarLateral";
import "./Assets/CSS/ModifyPlante.css"
import Temp_img from "./Assets/image/no-image.jpg"

function AddPlante()
{

    const navigate=useNavigate() ; 
    const [nom,setNom]=useState("")
    const [description,setDescription]=useState("")
    const [Image,setImage]=useState()
    const [ListTypePlant,setListTypePlant]=useState([])
    const [SelectedTypePlant,setSelectedTypePlant]=useState()



    function HandleImage(e) 
    {
        setImage(e.target.files[0])
    }

    useEffect(() => {
        axios({
            headers :{"authorization" : 'Bearer '+localStorage["Token"] },
            method : 'get',
            url :process.env.REACT_APP_server+"TypePlantes/"
        }).then((resp) => {
            setListTypePlant(resp.data)
        })
    },[])

    function AddPlante()
    {
    

        axios({
            headers :{"authorization" : 'Bearer '+localStorage["Token"] },
            method :'get',
            url :process.env.REACT_APP_server+"GetTypesPlantsByName/"+SelectedTypePlant
        }).then((resp) => {

            const formData = new FormData()
            formData.append("image", Image)
            

            axios.post(process.env.REACT_APP_server+"CreatePlanteByUser/"+localStorage["UserMail"]+"/"+nom+"/"+description+"/"+resp.data[0].idTypePlante+"/"+Image.type.split("/")[1], formData, { headerss: {'Content-Type': 'multipart/form-data',"authorization" : 'Bearer '+localStorage["Token"]}})
            .then((resp) => {
                if(resp.data==='OK')
                {
                    navigate('/Mesplantes')
                }
            })
                })    
    }

    return(
        <div style={{ width: "100%", height: "100%" }}>
        <div id="Mesplantes">
            <NavbarLateral />
        </div>
        <NavbarHaut title="Ajouter ma plante" />
    
        <div id="Main_Panel_plante">
            <div id="xcd587415">
                 {Image ? (
                    <img src={URL.createObjectURL(Image)} ></img>
                ) : (
                    <img src={Temp_img}></img>
                )}

            

                <fieldset>
                    <input placeholder="Nom" value={nom} onChange={(e) => {setNom(e.target.value)}}/>
                </fieldset>

                <fieldset class="input-description">
                    <input placeholder="Description" value={description} onChange={(e) => {setDescription(e.target.value)}}/>
                </fieldset>

                <fieldset>
                    <select id="SelectPlant"  onChange={(e) => {setSelectedTypePlant(e.target.value)}}>
                        <option> Type de plante</option>
                        {ListTypePlant.map((item) => {
                                return( <option> {item.libelle} </option>)
                        })}
                    </select>
                </fieldset>
                <fieldset>
                    <input placeholder="Url de la photo" type="file" onChange={(e) => {HandleImage(e)}}/>
                </fieldset>

                <div id="button-plante">
                        <button class="boutton-ajouter-plante" onClick={() => {AddPlante()}}> Ajouter </button>
                        <button class="boutton-annuler-add-plante" onClick={() => {navigate("/Mesplantes")}}> Annuler </button>
                </div>
            </div>
        </div>
        </div>
    )
}

export default AddPlante ;