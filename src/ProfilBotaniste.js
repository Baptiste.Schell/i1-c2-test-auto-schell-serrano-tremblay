import NavbarHaut from "./Component/NavbarHaut";
import NavbarLateral from "./Component/NavbarLateral"
import {useState, useEffect} from "react";
import axios from "axios";
import {useNavigate} from "react-router-dom";
import "./Assets/CSS/BotanisteCSS/ProfilBotaniste.css"
import React from "react";
import MyAnnonceProfil from "./Component/MyAnnonceProfil";


function MonProfilBotaniste() {
    const navigate = useNavigate();
    const [EditMode, setEditMode] = useState(false)
    const [idUser, setidUser] = useState()
    const [Nom, setNom] = useState("");
    const [Prenom, setPrenom] = useState("")
    const [Telephone, setTelephone] = useState("")
    const [Pseudo, setPseudo] = useState("")
    const [Photodeprofil, setPhotodeprofil] = useState("")
    const [IdAdresse, setIdAdresse] = useState()
    const [Voie, setVoie] = useState("")
    const [Rue, setRue] = useState("")
    const [Role, setRole] = useState("")
    const [Ville, setVille] = useState("")
    const [CP, setCP] = useState("")
    const [Switcher, setSwitcher] = useState(false)
    const [conseils, setConseils] = useState([])
    const [conseilsCount, setConseilsCount] = useState("")

    useEffect(() => {
        axios({
            headers : {"authorization" : 'Bearer '+localStorage["Token"]},
            method: 'GET',
            url: process.env.REACT_APP_server + "GetUserByMail/" + localStorage["UserMail"]
        }).then((resp) => {
            setidUser(resp.data[0].idUser)
            setNom(resp.data[0].nom)
            setPrenom(resp.data[0].prenom)
            setPseudo(resp.data[0].pseudo)
            setTelephone(resp.data[0].telephone)
            setPhotodeprofil(resp.data[0].photodeprofil)
        });

        axios({
            headers : {"authorization" : 'Bearer '+localStorage["Token"]},
            method: 'GET',
            url: process.env.REACT_APP_server + "GetRole/" + localStorage["UserMail"]
        }).then((resp) => {
            setRole(resp.data[0])
        });

        axios({
            headers : {"authorization" : 'Bearer '+localStorage["Token"]},
            method: 'GET',
            url: process.env.REACT_APP_server + "GetConseilsByBotaniste/" + localStorage["UserMail"]
        }).then((resp) => {
            setConseils(resp.data)
            setConseilsCount(resp.data.length)
        });
    }, [])


    function GoToEditMode() {
        // Pour rendre les input actif et pouvoir modifier ce qui à l'interieur
        setEditMode(true)
        var input = document.getElementsByClassName("EditableInput")
        for (var i = 0; i < input.length; i++) {
            input[i].disabled = false;
            input[i].style.border = "2px solid grey"
        }

    }

    function ExitEditMode() {
        //Désactiver les inputs et interdire du coup la saisie
        setEditMode(false)
        var input = document.getElementsByClassName("EditableInput")
        for (var i = 0; i < input.length; i++) {
            input[i].disabled = true;
            input[i].style.border = "none"
        }

        // On updates les nouvelles info  du profil
        UpdateInfoProfil()
    }

    function UpdateInfoProfil() {
        axios({
            method: "POST",
            url: process.env.REACT_APP_server + "UpdateInfoProfil/" + Nom + "/" + Prenom + "/" + Telephone + "/" + Pseudo + "/" + idUser
        })
    }


    return (
        <div style={{width: '100%', height: '100%'}}>
            <div id="Profil">
                <NavbarLateral/>
            </div>
            <NavbarHaut title="Profil"/>

            <div id="Main_Panel_Profil">
                <div className="card-entiere-profil">
                    <div id="Profil-card">
                        <div className="information-perso-titre">
                            <p className="titre-gauche-info-perso">Informations personnelles</p>
                            <p><i className="titre-droite-icone fa-solid fa-pen-to-square" onClick={() => {
                                navigate('/EditProfil',
                                    {
                                        state: {
                                            Pseudo: {Pseudo},
                                            Nom: {Nom},
                                            Prenom: {Prenom},
                                            Telephone: {Telephone},
                                            idRole: Role.idRole,
                                            UserMail: localStorage["UserMail"],
                                            idUser: {idUser},
                                            Voie: {Voie},
                                            Rue: {Rue},
                                            Ville: {Ville},
                                            CP: {CP},
                                            idAdresse: {IdAdresse},
                                        }
                                    })
                            }}></i></p>
                        </div>
                        <div className="information-perso-section">
                            <img src={Photodeprofil} className="information-perso-pdp"></img>
                            <div className="information-perso-rigth">
                                <div className="information-perso-cadre">
                                    <p className="information-perso-label-title"> Pseudo </p>
                                    <p className="information-perso-label"> {Pseudo} </p>
                                </div>
                                <div className="information-perso-nom-prenom">
                                    <div className="information-perso-cadre">
                                        <p className="information-perso-label-title"> Nom </p>
                                        <p className="information-perso-label"> {Nom} </p>
                                    </div>
                                    <div className="information-perso-cadre">
                                        <p className="information-perso-label-title"> Prénom </p>
                                        <p className="information-perso-label"> {Prenom} </p>
                                    </div>
                                </div>
                                <div className="information-perso-mail-tel">
                                    <div className="information-perso-cadre">
                                        <p className="information-perso-label-title"> Mail </p>
                                        <p className="information-perso-label"> {localStorage["UserMail"]} </p>
                                    </div>
                                    <div className="information-perso-cadre">
                                        <p className="information-perso-label-title"> Téléphone </p>
                                        <p className="information-perso-label"> {Telephone} </p>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div className="div-de-separation-profil"></div>
                        <div className="information-perso-titre">
                            <p className="titre-gauche-info-perso">Réinitialisation du mot de passe</p>
                        </div>
                        <div className="section-modifier-mdp">
                            <button className="bouton-modifier-mdp" onClick={() => {
                                navigate("/ResetPassword", {state: {_iduser: idUser}})
                            }}>
                                <p className="bouton-texte-modifier-mdp">Modifier mon mot de passe</p>
                            </button>
                        </div>
                    </div>
                    <div className="section-menu-choix-profil">
                        <button onClick={() => {
                            setSwitcher(false)
                        }} className="bouton-choix-annonces-profil">
                            Mes conseils
                        </button>
                    </div>

                    <div class="section-card-mes-annonces">
                        {conseils.map((item) => {
                            return (
                                <>
                                    <div class="card-mon-conseil">
                                        <div class="section-title-top-conseil">
                                            <p class="text-title-top-conseil">{item.titre}</p>
                                        </div>
                                        <div className="div-de-separation-mon-conseil"></div>
                                        <div class="section-bottom-conseil">
                                            <img class="img-conseil-card" src={item.urlPhoto}/>
                                            <div class="right-img-conseil-card">
                                                <div class="button-type-plant-conseil">
                                                    <p class="text-type-plant-conseil" onClick={() => {navigate('/AllConseilsForTypePlante',{state:{idTypePlante : item.idTypePlante}})}}>{item.libelle}</p>
                                                </div>
                                                <div class="description-card-conseil">
                                                    <p class="title-desc-conseil">Description</p>
                                                    <p class="paragraph-desc-conseil">{item.description}</p>
                                                </div>

                                            </div>
                                        </div>
                                    </div>

                                </>
                            )
                        })
                        }
                    </div>
                </div>
            </div>
        </div>
    )
}

export default MonProfilBotaniste;