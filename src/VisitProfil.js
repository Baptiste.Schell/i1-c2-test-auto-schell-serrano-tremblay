import React from "react";
import NavbarHaut from "./Component/NavbarHaut";
import NavbarLateral from "./Component/NavbarLateral"
import {useState, useEffect} from "react";
import axios from "axios";
import "./Assets/CSS/VisitProfil.css"
import {useLocation} from "react-router-dom";
import { useNavigate } from "react-router-dom";
import MyAnnonceProfil from "./Component/MyAnnonceProfil";


function VisitProfil() {

    const navigate=useNavigate() ;
    const location = useLocation()
    const [Mail, setMail] = useState(location.state.mailTest ? location.state.mailTest : location.state.mail);
    const [User, setUser] = useState("")
    const [Role, setRole] = useState("")
    const [Plante, setPlante] = useState([])
    const [postAnnonces, setPostAnnonces] = useState([]);
    const [keepAnnonces, setKeepAnnonces] = useState([]);
    const [switchFilters, setSwitchFilters] = useState(true)
    const [keepAnnoncesCount, setKeepAnnoncesCount] = useState("")
    const [postAnnoncesCount, setPostAnnoncesCount] = useState("")
    const [planteCount, setPlanteCount] = useState("")
    // comportement
    useEffect(() => {

        axios({
            headers : {"authorization" : 'Bearer '+localStorage["Token"]},
            method: 'GET',
            url: process.env.REACT_APP_server + "GetUserByMail/" + Mail
        }).then((resp) => {
            setUser(resp.data[0])
        });

        axios({
            headers : {"authorization" : 'Bearer '+localStorage["Token"]},
            method: 'GET',
            url: process.env.REACT_APP_server + "GetRole/" + Mail
        }).then((resp) => {
            setRole(resp.data[0])
        });

        axios({
            headers : {"authorization" : 'Bearer '+localStorage["Token"]},
            method: 'GET',
            url: process.env.REACT_APP_server + "GetPlantByUser/" + Mail
        }).then((resp) => {
            setPlante(resp.data)
            if (resp.data !== undefined) { // vérification que resp.data est défini
                setPlanteCount(resp.data.length); // ajout de cette ligne pour compter le nombre de plantes de l'utilisateur
            } else {
                setPlanteCount(0); // si resp.data est indéfini, on initialise la valeur de count à 0
            }
        });
        axios({
            method: 'GET',
            url: process.env.REACT_APP_server + "LoadPostAnnonces/" + Mail
        }).then((resp) => {
            setPostAnnonces(resp.data)
            if (resp.data !== undefined) { // vérification que resp.data est défini
                setPostAnnoncesCount(resp.data.length); // ajout de cette ligne pour compter le nombre d'annonces postée
            } else {
                setPostAnnoncesCount(0); // si resp.data est indéfini, on initialise la valeur de count à 0
            }
        });

        axios({
            headers : {"authorization" : 'Bearer '+localStorage["Token"]},
            method: 'GET',
            url: process.env.REACT_APP_server + "LoadKeepAnnonces/" + Mail
        }).then((resp) => {
            setKeepAnnonces(resp.data)
            if (resp.data !== undefined) { // vérification que resp.data est défini
                setKeepAnnoncesCount(resp.data.length); // ajout de cette ligne pour compter le nombre d'annonces postée
            } else {
                setKeepAnnoncesCount(0); // si resp.data est indéfini, on initialise la valeur de count à 0
            }
        });
    }, [])

    return (
        <div style={{width: '100%', height: '100%'}}>
            <div id="VisitProfil">
                <NavbarLateral/>
            </div>
            <NavbarHaut title={"Profil de " + User.pseudo}/>

            <div id="Main_Panel_Profil">
                <div class="card-entiere-profil">
                    <div id="Profil-card">
                        <div class="information-perso-titre">
                            <p class="titre-gauche-info-perso">Informations personnelles</p>
                        </div>
                        <div class="information-perso-section">
                            <img src={User.photodeprofil} class="information-perso-pdp"></img>
                            <div class="information-perso-rigth">
                                <div className="information-perso-nom-prenom">
                                    <div className="information-perso-cadre">
                                        <p className="information-perso-label-title"> Pseudo </p>
                                        <p className="information-perso-label"> {User.pseudo} </p>
                                    </div>
                                    <div className="information-perso-cadre">
                                        <p className="information-perso-label-title"> Status </p>
                                        <p className="information-perso-label"> {Role.label} </p>
                                    </div>
                                </div>
                                <div className="information-perso-nom-prenom">
                                    <div className="information-perso-cadre">
                                        <p className="information-perso-label-title"> Nom </p>
                                        <p className="information-perso-label"> {User.nom} </p>
                                    </div>
                                    <div className="information-perso-cadre">
                                        <p className="information-perso-label-title"> Prénom </p>
                                        <p className="information-perso-label"> {User.prenom} </p>
                                    </div>
                                </div>
                                <button class="bouton-modifier-mdp" onClick={() => navigate("/VisitAnnonce", {state: {idUser: User.idUser}})}>
                                    <p class="bouton-texte-modifier-mdp">Mes plantes</p>
                                </button>
                            </div>
                        </div>
                    </div>
                    <div class="section-menu-choix-profil">
                        <button onClick={() => {
                            setSwitchFilters(false)
                        }} class="bouton-choix-annonces-profil">
                            Mes annonces
                        </button>
                        <button onClick={() => {
                            setSwitchFilters(true)
                        }} class="bouton-choix-annonces-profil">
                            Plantes que j'ai gardés
                        </button>
                    </div>
                    {switchFilters ? (
                        <div class="section-card-mes-annonces">
                            {keepAnnonces.map((item) => {
                                let niveauExpertise = "";
                                if (item.idNiveauExpertiseRequis === 1) {
                                    niveauExpertise = "Débutant";
                                } else if (item.idNiveauExpertiseRequis === 2) {
                                    niveauExpertise = "Intermédiaire";
                                } else if (item.idNiveauExpertiseRequis === 3) {
                                    niveauExpertise = "Expert";
                                }
                                return (
                                    <>
                                        <div className="card-mon-annonce">
                                            <div className="title-card-mon-annonce">
                                                <p className="title-date-card-mon-annonce">
                                                    Annonce gardée<br/>
                                                    <p className="subtitle-date-card-mon-annonce">du {item.dateDebut} au {item.dateFin} </p>

                                                </p>
                                                <p className="title-ref-card-mon-annonce">ref : {item.reference} </p>
                                            </div>
                                            <div className="div-de-separation-mon-annonce"></div>
                                            <div className="description-mon-annonce">
                                                <div className="description-label-mon-annonce">
                                                    Description
                                                </div>
                                                <div className="description-text-mon-annonce">
                                                    {item.description}
                                                </div>
                                            </div>
                                            <div className="bottom-section-mon-annonce">
                                                <div className="niveau-expertise-tag">
                                                    <p className="niveau-expertise-text">{niveauExpertise}</p>
                                                </div>
                                            </div>
                                        </div>
                                    </>
                                );
                            })}
                        </div>
                    ) : (
                        <div class="section-card-mes-annonces">
                            {postAnnonces.map((item) => {
                                return (
                                    <MyAnnonceProfil
                                        mailTest={item.mailTest}
                                        Date_debut={item.dateDebut}
                                        Date_fin={item.dateFin}
                                        Reference={item.reference}
                                        Description={item.description}
                                        pseudoTest={item.pseudoTest}
                                        Expertise={item.idNiveauExpertiseRequis}
                                    />
                                );
                            })}
                        </div>
                    )}
                </div>
            </div>

        </div>
    )
}

export default VisitProfil;
